## Zusammenfassung

_Summarize the bug encountered concisely._

## Schritte zur Reproduktion

_Describe how one can reproduce the issue - this is very important. Please use an ordered list._ -->\*

## Aktuelles _fehlerhaftes_ Verhalten

_Describe what actually happens._

## Erwartetes _korrektes_ Verhalten

_Describe what you should see instead._

## Logs/Screenshots

_Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
as it's tough to read otherwise._

## Lösungsideen

_If you can, link to the line of code that might be responsible for the problem._

/label ~bug
