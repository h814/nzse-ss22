# Struktur von *src*

In diesem Ordner befinden sich alle Android Projekte, welche entweder im Rahmen des Praktikums oder für das Finale Ergebnis erstellt wurden und relevant sind. Mehr Infos zu den einzelnen Projekten gibt es in dem jeweiligen Abschnitt.

## upanddown

Diese App wurde im Rahmen des ersten Praktikums erstellt. Man kann lediglich einen Counter mit Buttons um entweder fünf erhöhen oder verringern.

## FirstApp

Auch diese App wurde im Rahmen des ersten Praktikums erstellt. Es ist ein einfacher Prozentrechner, welcher ein paar basic Konzepte von Android verwendet.

## BasicApp

Diese App ist teil des zweiten Praktikums. Sie implementiert weitere basic Android Konzepte wie z.B. Navigation und verschiedene Activities.

## SafeSpace

Dieser Ordner enthält das "main"-Projekt also die Finale App für die Prüfungsleistung.

## Legacy

legen Sie hier Ihr Projekt an, also
src/MensaBuddy -- o.ä. beachten Sie dabei, ein passendes Template zu wählen und als Package _de.hda.fbi.nzse.sose22.ProjektName_ zu wählen.

### Konventionen

- Beachten Sie die Hinweise für Klassen-Templates in Moodle (Tipps & Best Practices Java bzw. Kotlin).
- Führen Sie regelmäßig eine Code Inspection durch und fixen Sie wichtige Hinweise.

#### Naming Convention Ressourcen

Grundprinzip: Alle Ressourcennamen folgen einer einfachen [Konvention](https://jeroenmols.com/blog/2016/03/07/resourcenaming/).
![CheatSheat](https://jeroenmols.com/img/blog/resourcenaming/resourcenaming_cheatsheet.png)
