# SafeSpace App

The Todos are just to get a shor overview of what is required for a MVP. The ToDo items are also registered as issues and tracked in gitlab. The issues in gitlab are the counting part.

## ToDo

- [ ] Setup folder structure
- [ ] Remove default action bar
- [ ] Apply color scheme and font
- [ ] Apply app logo
- [ ] Apply Navigation ressource
- [ ] Apply screen fragments for Navigation items
- [ ] Create ViewModels for data
- [ ] Apply activities for non navigation screens
- [ ] Create DataClasses
- [ ] Create Layout for Screens
- [ ] Insert fake data

## ToDo RealData

- [ ] Setup Firebase
- [ ] Add login and register functionality
- [ ] Secure "insert home" screen
- [ ] Secure "my homes" screen
- [ ] fetch "home" data from firebase
- [ ] fetch "home" data from firebase