# Presentation folder

Here all all UI relevant files and classes like fragments and activities. You can create subpackages
to make it more clear to which part of the UI some files belong.