package de.hda.fbi.nzse.sose22.safespace.domain.use_case;

public class OfferNotFoundException extends RuntimeException {

    public OfferNotFoundException(String errorMessage, Throwable err) {
        super("The requested offer could not be found: " + errorMessage, err);
    }

    public OfferNotFoundException(String errorMessage) {
        this(errorMessage, null);
    }
}
