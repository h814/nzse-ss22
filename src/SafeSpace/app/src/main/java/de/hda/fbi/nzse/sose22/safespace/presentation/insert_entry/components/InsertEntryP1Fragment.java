package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferAcceptedGroup;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferType;

public class InsertEntryP1Fragment extends Fragment implements InsertScreen {
    private final AtomicBoolean isInited;
    private InsertOfferViewModel insertOfferViewModel;

    public InsertEntryP1Fragment() {
        isInited = new AtomicBoolean(false);
    }

    public static InsertEntryP1Fragment newInstance() {
        InsertEntryP1Fragment fragment = new InsertEntryP1Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insert_entry_p1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.isInited.set(false);
        this.insertOfferViewModel = new ViewModelProvider(requireParentFragment()).get(InsertOfferViewModel.class);

        this.insertOfferViewModel.getOffer().observe(getViewLifecycleOwner(), o -> {

            var acceptedGroups = o.getAcceptedGroup();
            ConstraintLayout s3GroupCounterWrapper = requireView().findViewById(R.id.insert_entry_p1_s3_person_count_wrapper);
            NumberPlusMinusView s3GroupCounter = requireView().findViewById(R.id.insert_entry_p1_s3_count);
            if (acceptedGroups.isEmpty()) {
                s3GroupCounterWrapper.setVisibility(View.INVISIBLE);
                s3GroupCounter.setValue(1);
            } else {
                s3GroupCounterWrapper.setVisibility(View.VISIBLE);
                var otherThanSingle = acceptedGroups.stream().anyMatch(g -> g != OfferAcceptedGroup.Single);
                if (otherThanSingle) {
                    s3GroupCounter.setIsEnabled(true);
                } else {
                    s3GroupCounter.setValue(1);
                    s3GroupCounter.setIsEnabled(false);
                }
            }

        });

        this.setupCheckboxListener();
        this.setupRadioListener();
        this.setupNumberChangeListener();

        this.insertOfferViewModel.getOffer().observe(getViewLifecycleOwner(), offer -> {
            if (this.isInited.get()) return;

            if (this.insertOfferViewModel.getOffer().getValue() == null)
                return;

            this.populateScreenData(this.insertOfferViewModel.getOffer().getValue());

            if (this.insertOfferViewModel.getOffer().getValue().getType() != OfferType.Room) {
                NumberPlusMinusView roomCounter = requireView().findViewById(R.id.insert_entry_p1_s2_count);
                roomCounter.setIsEnabled(true);
            }

            isInited.set(true);
        });

        var checkboxes = List.of(R.id.insert_entry_p1_s3_checkbox_single, R.id.insert_entry_p1_s3_checkbox_friends, R.id.insert_entry_p1_s3_checkbox_family);
        checkboxes.forEach(checkbox -> {
            CheckBox box = requireView().findViewById(checkbox);
            box.setOnClickListener(v -> {
                TextView tv = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_error);
                tv.setVisibility(View.GONE);
                tv.setText("");
            });
        });

    }

    private void setupCheckboxListener() {
        int[] checboxIds = {
                R.id.insert_entry_p1_s2_checkbox_bath,
                R.id.insert_entry_p1_s2_checkbox_kitchen,
                R.id.insert_entry_p1_s3_checkbox_family,
                R.id.insert_entry_p1_s3_checkbox_friends,
                R.id.insert_entry_p1_s3_checkbox_single
        };

        Arrays.stream(checboxIds).forEach(id -> {
            CheckBox checkBox = requireView().findViewById(id);
            if (checkBox == null) return;

            checkBox.setOnCheckedChangeListener(this::onChangeCheckBox);
        });
    }

    private void setupNumberChangeListener() {
        int[] numberPlusMinusViewIds = {
                R.id.insert_entry_p1_s2_count,
                R.id.insert_entry_p1_s3_count
        };

        Arrays.stream(numberPlusMinusViewIds).forEach(id -> {
            NumberPlusMinusView numberPlusMinusView = requireView().findViewById(id);
            numberPlusMinusView.addNumberChangedListener((view, number) -> {
                if (!isInited.get()) return;
                var offer = this.insertOfferViewModel.getOffer().getValue();
                if (offer == null) {
                    throw new RuntimeException("no offer found in viewmodel");
                }
                if (view.getId() == R.id.insert_entry_p1_s2_count) {
                    offer.setRoomCount(number);
                } else if (view.getId() == R.id.insert_entry_p1_s3_count) {
                    offer.setMaxAcceptedPersons(number);
                }
                this.insertOfferViewModel.updateOffer(offer);
            });
        });
    }


    private void setupRadioListener() {
        RadioGroup rg = requireView().findViewById(R.id.insert_entry_p1_s1_rg);
        rg.setOnCheckedChangeListener(this::onChangeRadioGroup);
    }

    private void onChangeRadioGroup(RadioGroup group, int checkedId) {
        if (!isInited.get()) return;
        var rgId = group.getId();
        var offer = this.insertOfferViewModel.getOffer().getValue();
        if (offer == null) {
            throw new RuntimeException("no offer found in viewmodel");
        }
        if (rgId == R.id.insert_entry_p1_s1_rg) {

            NumberPlusMinusView pmv = requireView().findViewById(R.id.insert_entry_p1_s2_count);

            int checkedRadioButtonId = group.getCheckedRadioButtonId();
            if (checkedRadioButtonId == R.id.insert_entry_p1_s1_rg_flat) {
                offer.setType(OfferType.Flat);
                pmv.setIsEnabled(true);
            } else if (checkedRadioButtonId == R.id.insert_entry_p1_s1_rg_house) {
                offer.setType(OfferType.House);
                pmv.setIsEnabled(true);
            } else if (checkedRadioButtonId == R.id.insert_entry_p1_s1_rg_room) {
                offer.setType(OfferType.Room);
                pmv.setIsEnabled(false);
                pmv.setValue(1);
            }
        }

        this.insertOfferViewModel.updateOffer(offer);
    }


    private void onChangeCheckBox(CompoundButton buttonView, boolean isChecked) {
        if (!isInited.get()) return;
        var id = buttonView.getId();

        var offer = this.insertOfferViewModel.getOffer().getValue();

        if (offer == null) {
            throw new RuntimeException("no offer found in viewmodel");
        }

        if (id == R.id.insert_entry_p1_s2_checkbox_bath) {
            offer.setHasBath(isChecked);
        } else if (id == R.id.insert_entry_p1_s2_checkbox_kitchen) {
            offer.setHasKitchen(isChecked);
        } else if (id == R.id.insert_entry_p1_s3_checkbox_family) {
            if (isChecked) {
                offer.addAcceptedGroup(OfferAcceptedGroup.Family);
            } else {
                offer.removeAcceptedGroup(OfferAcceptedGroup.Family);
            }
        } else if (id == R.id.insert_entry_p1_s3_checkbox_friends) {
            if (isChecked) {
                offer.addAcceptedGroup(OfferAcceptedGroup.Group);
            } else {
                offer.removeAcceptedGroup(OfferAcceptedGroup.Group);
            }
        } else if (id == R.id.insert_entry_p1_s3_checkbox_single) {
            if (isChecked) {
                offer.addAcceptedGroup(OfferAcceptedGroup.Single);
            } else {
                offer.removeAcceptedGroup(OfferAcceptedGroup.Single);
            }
        }
        this.insertOfferViewModel.updateOffer(offer);
    }

    @Override
    public void populateScreenData(Offer o) {

        RadioGroup type = requireView().findViewById(R.id.insert_entry_p1_s1_rg);
        switch (o.getType()) {
            case Flat:
                type.check(R.id.insert_entry_p1_s1_rg_flat);
                break;
            case House:
                type.check(R.id.insert_entry_p1_s1_rg_house);
                break;
            case Room:
                type.check(R.id.insert_entry_p1_s1_rg_room);
                break;
        }

        NumberPlusMinusView roomCount = requireView().findViewById(R.id.insert_entry_p1_s2_count);
        CheckBox bathBox = requireView().findViewById(R.id.insert_entry_p1_s2_checkbox_bath);
        CheckBox kitchenBox = requireView().findViewById(R.id.insert_entry_p1_s2_checkbox_kitchen);

        roomCount.setValue(o.getRoomCount());
        bathBox.setChecked(o.isHasBath());
        kitchenBox.setChecked(o.isHasKitchen());

        CheckBox singleBox = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_single);
        CheckBox groupBox = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_friends);
        CheckBox familyBox = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_family);
        NumberPlusMinusView personCount = requireView().findViewById(R.id.insert_entry_p1_s3_count);

        o.getAcceptedGroup().forEach(el -> {
            switch (el) {
                case Single:
                    singleBox.setChecked(true);
                    break;
                case Family:
                    familyBox.setChecked(true);
                    break;
                case Group:
                    groupBox.setChecked(true);
                    break;
            }
        });

        if (groupBox.isChecked() || familyBox.isChecked()) {
            personCount.setValue(o.getMaxAcceptedPersons());
        }

    }

    @Override
    public boolean isValid() {
        CheckBox single = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_single);
        CheckBox group = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_friends);
        CheckBox family = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_family);

        if (!single.isChecked() && !group.isChecked() && !family.isChecked()) {
            TextView tv = requireView().findViewById(R.id.insert_entry_p1_s3_checkbox_error);
            tv.setVisibility(View.VISIBLE);
            tv.setText(R.string.insert_entry_p1_s3_error);
            return false;
        }

        return true;
    }
}