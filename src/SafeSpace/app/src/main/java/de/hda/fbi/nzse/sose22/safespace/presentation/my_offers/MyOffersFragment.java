package de.hda.fbi.nzse.sose22.safespace.presentation.my_offers;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.OfferListAdapter;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

@AndroidEntryPoint
public class MyOffersFragment extends Fragment {

    @Inject
    @OfferRepositoryFirestore
    OfferRepository offerRepository;
    private MyOffersViewModel myOffersViewModel;
    private List<Offer> offers;

    public MyOffersFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_offers, container, false);
    }

    // Use notifyDataSetChanged because whole list can change
    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        UserViewModel userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
        userViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            if (user == null) {
                var dst = MyOffersFragmentDirections.actionGlobalProfileToMainNavigationLogin2();
                Navigation.findNavController(requireView()).navigate(dst);
            }
        });

        this.offers = new ArrayList<>();
        super.onViewCreated(view, savedInstanceState);

        MaterialToolbar toolbar = requireView().findViewById(R.id.my_offers_list_toolbar);
        toolbar.setTitle(R.string.my_offers_title);
        toolbar.setNavigationOnClickListener(v -> Navigation.findNavController(requireView()).popBackStack());

        OfferListAdapter offerListAdapter = new OfferListAdapter(offers);
        offerListAdapter.setHasStableIds(true);
        RecyclerView offerList = requireView().findViewById(R.id.my_offers_list);
        offerList.setLayoutManager(new LinearLayoutManager(requireContext()));
        offerList.setAdapter(offerListAdapter);

        var self = this;

        ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    var pos = viewHolder.getAbsoluteAdapterPosition();
                    var deleted = self.offers.get(pos);
                    self.offers.remove(pos);
                    offerListAdapter.notifyItemRemoved(pos);
                    offerRepository.removeById(deleted.getId());


                    Snackbar.make(offerList, R.string.my_offers_offer_deleted_snackbar, Snackbar.LENGTH_SHORT).setAction(R.string.my_offers_offer_deleted_snackbar_undo, v -> {
                        self.offers.add(pos, deleted);
                        offerRepository.createNew(deleted);
                        offerListAdapter.notifyItemInserted(pos);
                    }).show();
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addBackgroundColor(ContextCompat.getColor(requireContext(), R.color.delete_red))
                        .addActionIcon(R.drawable.ic_delete_rounded_48)
                        .setSwipeLeftActionIconTint(R.color.white)
                        .create()
                        .decorate();

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper helper = new ItemTouchHelper(removeCallback);
        helper.attachToRecyclerView(offerList);

        this.myOffersViewModel = new ViewModelProvider(this).get(MyOffersViewModel.class);
        this.myOffersViewModel.getOffers().observe(getViewLifecycleOwner(), o -> {
            //Handle offer change
            this.offers.clear();
            this.offers.addAll(o);
            offerListAdapter.notifyDataSetChanged();
        });

        requireView().findViewById(R.id.my_offers_add_fab).setOnClickListener(v -> {
            var direction = MyOffersFragmentDirections.actionMainNavigationMyOffersToMainNavigationInsertEntry();
            Navigation.findNavController(requireView()).navigate(direction);
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        this.myOffersViewModel.update();
    }
}