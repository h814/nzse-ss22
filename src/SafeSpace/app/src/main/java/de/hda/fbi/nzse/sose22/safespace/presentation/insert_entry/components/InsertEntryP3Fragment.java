package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;

public class InsertEntryP3Fragment extends Fragment implements InsertScreen {

    private static final int IMAGE_LIMIT = 6;
    private final AtomicBoolean isInited;
    private InsertOfferViewModel insertOfferViewModel;
    private ImageListAdapter imageListAdapter;
    //Cannot be final because is required in galleryLauncher where the field is required
    //will fail if field is final
    @SuppressWarnings("CanBeFinal")
    private List<Uri> images;
    private final ActivityResultLauncher<Intent> galleryLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() != Activity.RESULT_OK) {
            Toast.makeText(getActivity(), "Error in gallery", Toast.LENGTH_SHORT).show();
            return;
        }

        if (result.getData() != null && result.getData().getClipData() != null) {
            var clipData = result.getData().getClipData();
            for (int i = 0; i < clipData.getItemCount(); ++i) {
                var item = clipData.getItemAt(i);
                if (this.images.size() >= IMAGE_LIMIT) {
                    break;
                }
                images.add(item.getUri());
                imageListAdapter.notifyItemInserted(this.images.size() - 1);
            }
        } else if (result.getData() != null && result.getData().getData() != null) {
            var uri = result.getData().getData();
            images.add(uri);
            imageListAdapter.notifyItemInserted(this.images.size() - 1);
        }

    });

    public InsertEntryP3Fragment() {
        this.images = new ArrayList<>();
        isInited = new AtomicBoolean(false);
    }

    public static InsertEntryP3Fragment newInstance() {
        InsertEntryP3Fragment fragment = new InsertEntryP3Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insert_entry_p3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.insertOfferViewModel = new ViewModelProvider(requireParentFragment()).get(InsertOfferViewModel.class);

        this.insertOfferViewModel.getIUris().observe(getViewLifecycleOwner(), u -> ((TextView) requireView().findViewById(R.id.insert_entry_p3_s2_picture_count)).setText(getString(R.string.insert_entry_p3_s2_image_count, this.images.size(), IMAGE_LIMIT)));

        this.setupImageList();
        this.setupAddressListener();

        this.insertOfferViewModel.getOffer().observe(getViewLifecycleOwner(), offer -> {
            if (isInited.get()) return;

            if (offer != null) {
                this.populateScreenData(offer);
                this.isInited.set(true);
            }
        });
    }

    private void setupAddressListener() {
        Button infoButton = requireView().findViewById(R.id.insert_entry_p3_s1_show_address_info);
        SwitchMaterial infoSwitch = requireView().findViewById(R.id.insert_entry_p3_s1_show_address_switch);

        infoButton.setOnClickListener(view -> {
            var alertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
            alertDialogBuilder
                    .setTitle(R.string.insert_entry_p3_s1_show_address)
                    .setMessage(R.string.insert_entry_p3_s1_show_address_info)
                    .setPositiveButton(getString(R.string.insert_entry_p3_s1_show_address_alert_confirm), (dialog, which) -> dialog.dismiss()).show();
        });

        infoSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            var o = this.insertOfferViewModel.getOffer().getValue();

            if (o == null) throw new RuntimeException("The current offer value is null");

            o.setShowFullAddress(isChecked);
            this.insertOfferViewModel.updateOffer(o);
        });

        Map<Integer, Integer> layoutIdEditIdMap = Map.of(R.id.insert_entry_p3_s1_street_layout, R.id.insert_entry_p3_s1_street, R.id.insert_entry_p3_s1_number_layout, R.id.insert_entry_p3_s1_number, R.id.insert_entry_p3_s1_plz_layout, R.id.insert_entry_p3_s1_plz, R.id.insert_entry_p3_s1_town_layout, R.id.insert_entry_p3_s1_town);
        layoutIdEditIdMap.forEach((layoutId, editId) -> {
            TextInputLayout layout = requireView().findViewById(layoutId);
            if (layout == null) throw new RuntimeException("cannot find view with id: " + layoutId);
            Objects.requireNonNull(layout.getEditText()).addTextChangedListener(new AddressTextWatcher(editId, this.insertOfferViewModel, layout));
        });
    }

    private void setupImageList() {
        RecyclerView imageList = requireView().findViewById(R.id.insert_entry_p3_s2_picutre_list);

        // reset list an recreate it otherwise data will be readded on navigating
        var tmp = new ArrayList<>(this.images);
        this.images.clear();
        this.images.addAll(tmp);

        ImageView deleteImage = requireView().findViewById(R.id.insert_entry_p3_s2_picutre_delete);
        this.imageListAdapter = new ImageListAdapter(this.images, IMAGE_LIMIT, deleteImage);
        imageList.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        imageList.setAdapter(this.imageListAdapter);
        this.imageListAdapter.galleryLauncher = this.galleryLauncher;
        imageListAdapter.registerAdapterDataObserver(new OnChageImageList(imageListAdapter, images, IMAGE_LIMIT, this.insertOfferViewModel));


        deleteImage.setOnDragListener((v, event) -> {

            if (event.getAction() == DragEvent.ACTION_DRAG_ENTERED) {
                v.getBackground().setAlpha(255);
            } else if (event.getAction() == DragEvent.ACTION_DRAG_EXITED) {
                v.getBackground().setAlpha(128);
            } else if (event.getAction() == DragEvent.ACTION_DROP) {
                View view = (View) event.getLocalState();
                ViewGroup owner = (ViewGroup) view.getParent();

                if (owner.getId() == R.id.insert_entry_p3_s2_picutre_list) {
                    int pos = Objects.requireNonNull(imageList.findContainingViewHolder(view)).getAbsoluteAdapterPosition();

                    var deleted = this.images.get(pos);
                    this.images.remove(pos);
                    this.imageListAdapter.notifyItemRemoved(pos);

                    Snackbar undo = Snackbar.make(imageList, R.string.insert_entry_p3_s2_snackbar_undo_text, Snackbar.LENGTH_SHORT);
                    undo.setAction(R.string.insert_entry_p3_s2_snackbar_undo_action, ignored -> {
                        images.add(pos, deleted);
                        imageListAdapter.notifyItemInserted(pos);
                    });
                    undo.show();
                }
            }

            return true;
        });
    }

    @Override
    public void populateScreenData(Offer o) {
        TextInputEditText streetField = requireView().findViewById(R.id.insert_entry_p3_s1_street);
        TextInputEditText numberField = requireView().findViewById(R.id.insert_entry_p3_s1_number);
        TextInputEditText plzField = requireView().findViewById(R.id.insert_entry_p3_s1_plz);
        TextInputEditText townField = requireView().findViewById(R.id.insert_entry_p3_s1_town);
        SwitchMaterial infoSwitch = requireView().findViewById(R.id.insert_entry_p3_s1_show_address_switch);

        infoSwitch.setChecked(o.isShowFullAddress());
        streetField.setText(o.getAddress().getStreet());
        numberField.setText(o.getAddress().getNumber());

        if (o.getAddress().getPostalCode() <= 0) {
            plzField.setText("");
        } else {
            plzField.setText(String.valueOf(o.getAddress().getPostalCode()));
        }

        townField.setText(o.getAddress().getTown());

        List<Uri> uris = o.getImageURIs().stream().map(uri -> Uri.parse(uri.toString())).collect(Collectors.toList());
        this.images.addAll(uris);
        this.imageListAdapter.notifyItemRangeInserted(0, uris.size());
        TextView textView = requireView().findViewById(R.id.insert_entry_p3_s2_picture_count);
        textView.setText(getString(R.string.insert_entry_p3_s2_image_count, this.images.size(), IMAGE_LIMIT));
    }

    @Override
    public boolean isValid() {
        TextInputLayout street = requireView().findViewById(R.id.insert_entry_p3_s1_street_layout);
        TextInputLayout number = requireView().findViewById(R.id.insert_entry_p3_s1_number_layout);
        TextInputLayout postalCode = requireView().findViewById(R.id.insert_entry_p3_s1_plz_layout);
        TextInputLayout town = requireView().findViewById(R.id.insert_entry_p3_s1_town_layout);

        var offer = insertOfferViewModel.getOffer().getValue();

        if (offer == null) throw new RuntimeException("Offer cannot be null here");

        var address = offer.getAddress();

        boolean hasError = false;

        if (address.getStreet().isEmpty()) {
            street.setError(getString(R.string.insert_entry_p3_s1_field_required));
            hasError = true;
        }

        if (address.getNumber().isEmpty()) {
            hasError = true;
            number.setError(getString(R.string.insert_entry_p3_s1_field_required));
        }


        if (!isZIPValid(String.valueOf(address.getPostalCode()))) {
            hasError = true;
            postalCode.setError(getString(R.string.insert_entry_p3_s1_invalid_zip));
        }

        if (address.getTown().isEmpty()) {
            hasError = true;
            town.setError(getString(R.string.insert_entry_p3_s1_field_required));
        }

        return !hasError;
    }

    private boolean isZIPValid(String zipCode) {
        var germanZipRegex = Pattern.compile("(?!01000|99999)(0[1-9]\\d{3}|[1-9]\\d{4})");
        return germanZipRegex.matcher(zipCode).find();
    }

    private static class AddressTextWatcher implements TextWatcher {

        private final int id;
        private final InsertOfferViewModel viewModel;
        private final TextInputLayout layout;

        public AddressTextWatcher(int id, InsertOfferViewModel viewModel, TextInputLayout layout) {
            this.id = id;
            this.viewModel = viewModel;
            this.layout = layout;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            layout.setError("");
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            var str = s.toString();
            var o = this.viewModel.getOffer().getValue();

            if (o == null || o.getAddress() == null) return;

            if (id == R.id.insert_entry_p3_s1_street) {
                o.getAddress().setStreet(str);
            } else if (id == R.id.insert_entry_p3_s1_number) {
                o.getAddress().setNumber(str);
            } else if (id == R.id.insert_entry_p3_s1_plz) {
                if (s.toString().isEmpty()) {
                    o.getAddress().setPostalCode(0);
                } else {
                    try {
                        o.getAddress().setPostalCode(Integer.parseInt(str));
                    } catch (NumberFormatException ignored) {
                        o.getAddress().setPostalCode(0);
                    }
                }
            } else if (id == R.id.insert_entry_p3_s1_town) {
                o.getAddress().setTown(str);
            } else {
                return;
            }

            this.viewModel.updateOffer(o);
        }
    }

    public static class OnChageImageList extends RecyclerView.AdapterDataObserver {
        final ImageListAdapter adapterbind;
        final InsertOfferViewModel viewModel;
        final List<Uri> data;
        final int limit;

        public OnChageImageList(ImageListAdapter adapter, List<Uri> data, int limit, InsertOfferViewModel viewModel) {
            this.adapterbind = adapter;
            this.limit = limit;
            this.data = data;
            this.viewModel = viewModel;
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            handleChange();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            handleChange();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            handleChange();
        }

        @Override
        public void onChanged() {
            handleChange();
        }

        private void handleChange() {
            adapterbind.setHasAddButton(data.size() < limit);
            this.viewModel.updateImageUris(data);
        }
    }
}