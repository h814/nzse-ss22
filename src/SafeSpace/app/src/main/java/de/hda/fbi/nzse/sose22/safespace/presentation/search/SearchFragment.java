package de.hda.fbi.nzse.sose22.safespace.presentation.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputEditText;

import de.hda.fbi.nzse.sose22.safespace.R;

public class SearchFragment extends Fragment {

    public SearchFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        requireView().findViewById(R.id.search_search_button).setOnClickListener(v -> {
            TextInputEditText searchValue = requireView().findViewById(R.id.search_value);

            if (searchValue.getText() == null)
                throw new IllegalStateException("TextInputEditText with id search_value getText() was unexpected null");

            var val = searchValue.getText().toString();
            var dest = SearchFragmentDirections.actionMainNavigationSearchToMainNavigationSearchList(val);
            Navigation.findNavController(requireView()).navigate(dest);
        });
    }
}