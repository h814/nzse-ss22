package de.hda.fbi.nzse.sose22.safespace.data.repository;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;

public class UserServiceFirebaseImpl extends UserService {

    private final AtomicBoolean pendingLogin = new AtomicBoolean(false);

    @Inject
    public UserServiceFirebaseImpl() {
    }

    @Override
    public void setCurrentUser(User currentUser) {
        this.pendingLogin.set(false);
        super.setCurrentUser(currentUser);
    }

    @Override
    public void setUserRepository(UserRepository userRepository) {
        super.setUserRepository(userRepository);
        var auth = FirebaseAuth.getInstance();
        FirebaseUser fbUser = auth.getCurrentUser();
        if (fbUser == null) return;

        userRepository.getUserById(fbUser.getUid()).addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                logout();
            } else {
                var user = task.getResult();

                if (user == null) {
                    logout();
                    return;
                }
                setCurrentUser(user);
            }
        });


    }

    @Override
    public Task<Boolean> login(String email, String password) {
        var auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null)
            return Tasks.forException(new RuntimeException("Already loggedIn please logout first"));

        if (email.isEmpty() || password.isEmpty())
            return Tasks.forException(new IllegalArgumentException("No empty strings"));


        var authTask = auth.signInWithEmailAndPassword(email, password);

        return authTask.continueWithTask(task -> {
            if (!task.isSuccessful()) {
                setCurrentUser(null);
                if (task.getException() != null) throw task.getException();
                throw new RuntimeException("Error signing in");
            }
            FirebaseUser fbUser = auth.getCurrentUser();
            return userRepository.getUserById(fbUser.getUid());
        }).continueWith(task -> {
            var user = task.getResult();
            if (!task.isSuccessful() || user == null) {
                auth.signOut();
                setCurrentUser(null);
                if (task.getException() != null) throw task.getException();
                throw new RuntimeException("Failed to fetch user data");
            }

            setCurrentUser(user);
            return true;
        });
    }

    @Override
    public Task<Boolean> changePassword(String password, String oldPassword) {
        var auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null || auth.getCurrentUser().getEmail() == null)
            return Tasks.forException(new RuntimeException("Not logged in or unable to get email"));

        var user = auth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), oldPassword);
        return user.reauthenticate(credential).continueWithTask(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) throw task.getException();
                throw new RuntimeException("Unable to reauthenticate");
            }
            var reAuthUser = auth.getCurrentUser();
            return reAuthUser.updatePassword(password);
        }).continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) throw task.getException();
                throw new RuntimeException("Failed to updated password");
            }

            return true;
        });
    }

    @Override
    public Task<Boolean> changeEmail(String email, String oldPassword) {
        var auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null || auth.getCurrentUser().getEmail() == null)
            return Tasks.forException(new RuntimeException("Not logged in or unable to get email"));

        var user = auth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), oldPassword);
        return user.reauthenticate(credential).continueWithTask(task -> {
            if (!task.isSuccessful()) {
                throw new RuntimeException("Unable to reauthenticate");
            }
            var reAuthUser = auth.getCurrentUser();
            return reAuthUser.updateEmail(email);
        }).continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) throw task.getException();
                throw new RuntimeException("Unable to change email address");
            }

            return true;
        });
    }

    @Override
    public Task<Boolean> alterUser(User user) {
        var auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() == null)
            return Tasks.forException(new RuntimeException("Not logged in"));

        return this.userRepository.alterUser(auth.getCurrentUser().getUid(), user).continueWithTask(task -> {
            if (!task.isSuccessful()) {

                if (task.getException() != null) throw task.getException();

                throw new RuntimeException("Failed to alter user");
            }
            FirebaseUser fbUser = auth.getCurrentUser();
            return userRepository.getUserById(fbUser.getUid());
        }).continueWith(task -> {
            if (!task.isSuccessful()) {

                if (task.getException() != null) throw task.getException();

                return false;
            }

            var updatedUser = task.getResult();
            setCurrentUser(updatedUser);
            return true;
        });
    }

    @Override
    public Task<Boolean> register(User user, String password) {
        var auth = FirebaseAuth.getInstance();

        return auth.createUserWithEmailAndPassword(user.getEmail(), password).continueWithTask(task -> {
            if (task.isSuccessful()) {

                if (auth.getCurrentUser() == null)
                    throw new RuntimeException("Register task successful but user is null");

                var id = auth.getCurrentUser().getUid();
                return userRepository.createUser(id, user);
            } else {
                if (task.getException() != null) throw task.getException();

                return Tasks.forException(new RuntimeException("unknown error while register user"));
            }
        }).continueWith(task -> {
            if (task.isSuccessful() && task.getResult()) {
                setCurrentUser(user);
                return true;
            } else {
                auth.signOut();

                if (task.getException() != null) throw task.getException();

                return false;
            }
        });
    }

    @Override
    public void logout() {
        setCurrentUser(null);
        var auth = FirebaseAuth.getInstance();
        auth.signOut();
    }
}
