package de.hda.fbi.nzse.sose22.safespace.presentation.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.domain.model.ContactType;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.presentation.my_profile.MyProfileFragmentDirections;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class RegisterFragment extends Fragment {
    public static final String IS_CHANGE_PROFILE_PARAM = "isChangeProfile";
    private boolean isChangeProfile = false;

    //Chip status
    private boolean chip_phone = false;
    private boolean chip_email = false;
    private boolean chip_whatsapp = false;
    private boolean chip_other = false;

    private RegisterViewModel registerViewModel;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RegisterFragment.
     */
    public static RegisterFragment newInstance(boolean isChangeProfile) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_CHANGE_PROFILE_PARAM, isChangeProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.isChangeProfile = getArguments().getBoolean(IS_CHANGE_PROFILE_PARAM, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.registerViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);

        if (this.isChangeProfile) {
            MaterialToolbar appBar = requireView().findViewById(R.id.register_app_bar);
            appBar.setTitle(R.string.register_my_profile);
            Button registerBtn = requireView().findViewById(R.id.register_submit_btn);
            registerBtn.setText(R.string.register_change_profile);
            var user = new ViewModelProvider(requireActivity()).get(UserViewModel.class).getUser().getValue();
            if (user == null) throw new RuntimeException("User cannot  be null");

            requireView().findViewById(R.id.register_edit_sensitive_layout).setVisibility(View.VISIBLE);
            this.populateData(user);
        }

        requireView().findViewById(R.id.register_submit_btn).setOnClickListener(l -> this.clickSubmit());

        ((MaterialToolbar) requireView().findViewById(R.id.register_app_bar)).setNavigationOnClickListener(item -> this.clickBack());

        this.addChangeListener();

        //Handle chip status
        requireView().findViewById(R.id.register_chip_phone).setOnClickListener(l -> {
            this.chip_phone = !this.chip_phone;
            this.handleDisplayedInputs();
        });
        requireView().findViewById(R.id.register_chip_whatsapp).setOnClickListener(l -> {
            this.chip_whatsapp = !this.chip_whatsapp;
            this.handleDisplayedInputs();
        });
        requireView().findViewById(R.id.register_chip_email).setOnClickListener(l -> {
            this.chip_email = !this.chip_email;
            this.handleDisplayedInputs();
        });
        requireView().findViewById(R.id.register_chip_other).setOnClickListener(l -> {
            this.chip_other = !this.chip_other;
            this.handleDisplayedInputs();
        });

        int[] textViews = {
                R.id.register_input_email,
                R.id.register_input_phone,
                R.id.register_input_whatsapp,
                R.id.register_input_other,
                R.id.register_vorname,
                R.id.register_nachname,
                R.id.register_email
        };

        Arrays.stream(textViews).forEach(id -> {
            TextInputEditText tv = requireView().findViewById(id);
            tv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    var user = registerViewModel.getUser().getValue();

                    if (user == null) throw new IllegalStateException("User cannot be null");

                    var data = s.toString();

                    if (id == R.id.register_input_email) {
                        user.changeContact(ContactType.Mail, data);
                    } else if (id == R.id.register_input_phone) {
                        user.changeContact(ContactType.Phone, data);
                    } else if (id == R.id.register_input_whatsapp) {
                        user.changeContact(ContactType.WhatsApp, data);
                    } else if (id == R.id.register_input_other) {
                        user.changeContact(ContactType.Other, data);
                    } else if (id == R.id.register_vorname) {
                        user.setFirstName(data);
                    } else if (id == R.id.register_nachname) {
                        user.setLastName(data);
                    } else if (id == R.id.register_email) {
                        user.setEmail(data);
                    }

                    registerViewModel.updateUser(user);
                }
            });
        });

    }

    /**
     * Fills the form when changing settings
     *
     * @param user Object of current user
     */
    private void populateData(User user) {
        TextInputEditText firstName = requireView().findViewById(R.id.register_vorname);
        firstName.setText(user.getFirstName());
        TextInputEditText lastName = requireView().findViewById(R.id.register_nachname);
        lastName.setText(user.getLastName());

        user.getContacts().forEach(contact -> {
            TextInputLayout tv;
            Chip chip;
            switch (contact.getType()) {
                case WhatsApp:
                    this.chip_whatsapp = true;
                    chip = requireView().findViewById(R.id.register_chip_whatsapp);
                    chip.setChecked(true);
                    tv = requireView().findViewById(R.id.register_input_whatsapp_layout);
                    tv.setVisibility(View.VISIBLE);
                    Objects.requireNonNull(tv.getEditText()).setText(contact.get());
                    break;
                case Phone:
                    this.chip_phone = true;
                    chip = requireView().findViewById(R.id.register_chip_phone);
                    chip.setChecked(true);
                    tv = requireView().findViewById(R.id.register_input_phone_layout);
                    tv.setVisibility(View.VISIBLE);
                    Objects.requireNonNull(tv.getEditText()).setText(contact.get());
                    break;
                case Other:
                    this.chip_other = true;
                    chip = requireView().findViewById(R.id.register_chip_other);
                    chip.setChecked(true);
                    tv = requireView().findViewById(R.id.register_input_other_layout);
                    tv.setVisibility(View.VISIBLE);
                    Objects.requireNonNull(tv.getEditText()).setText(contact.get());
                    break;
                case Mail:
                    this.chip_email = true;
                    chip = requireView().findViewById(R.id.register_chip_email);
                    chip.setChecked(true);
                    tv = requireView().findViewById(R.id.register_input_email_layout);
                    tv.setVisibility(View.VISIBLE);
                    Objects.requireNonNull(tv.getEditText()).setText(contact.get());
                    break;
            }
        });

    }

    public void clickSubmit() {

        Button registerBtn = requireView().findViewById(R.id.register_submit_btn);

        TextInputEditText password = requireView().findViewById(R.id.register_password);
        TextInputEditText passwordOld = requireView().findViewById(R.id.register_old_password);

        if (password.getText() == null || passwordOld.getText() == null)
            throw new IllegalStateException("Password or old password getText() is null");

        //Reset form errors
        this.resetFormErrors();

        //Listener to dismiss the error dialog
        DialogInterface.OnClickListener onPositiveDialog = (dialog, which) -> {
            dialog.dismiss();
            registerBtn.setEnabled(true);
            registerBtn.setClickable(true);
            requireView().findViewById(R.id.progress_linear).setVisibility(View.GONE);
        };

        //Check if form is valid
        if (!this.validateForm()) return;

        registerBtn.setEnabled(false);
        registerBtn.setClickable(false);
        requireView().findViewById(R.id.progress_linear).setVisibility(View.VISIBLE);

        Task<Boolean> registerTask;

        //Mode change profile
        if (isChangeProfile) {
            registerTask = this.registerViewModel.register(password.getText().toString(), passwordOld.getText().toString());
        } else {
            registerTask = this.registerViewModel.register(password.getText().toString());
        }

        registerTask.addOnCompleteListener(task -> {

            if (task.isSuccessful() && task.getResult()) {
               if(!isChangeProfile) {
                   var dir = RegisterFragmentDirections.actionMainNavigationRegisterToMainNavigationProfileGraph();
                   Navigation.findNavController(requireView()).navigate(dir);
               }else {
                  Navigation.findNavController(requireView()).navigateUp();
               }

               return;
            }

            String msg = getString(R.string.unknown_error);

            if (task.getException() != null) {
                msg = task.getException().getMessage();
            } else if (!task.getResult()) {
                msg = getString(R.string.internal_error_try_again);
            }

            showErrorDialog(getString(R.string.register_failed_dialog_title), msg, onPositiveDialog);
        });

    }

    private void showErrorDialog(String title, String msg, DialogInterface.OnClickListener listener) {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, listener).show();
    }

    //Button click back
    public void clickBack() {
        Navigation.findNavController(requireView()).popBackStack();
    }

    //handle clicked chips
    public void handleDisplayedInputs() {

        var user = this.registerViewModel.getUser().getValue();

        if (user == null) throw new IllegalStateException("User cannot be null");

        if (this.chip_phone)
            requireView().findViewById(R.id.register_input_phone_layout).setVisibility(View.VISIBLE);
        else {
            resetInput(R.id.register_input_phone_layout);
            requireView().findViewById(R.id.register_input_phone_layout).setVisibility(View.GONE);
            user.removeContact(ContactType.Phone);
            ((TextInputLayout) requireView().findViewById(R.id.register_input_phone_layout)).setError(null);
        }

        if (this.chip_whatsapp)
            requireView().findViewById(R.id.register_input_whatsapp_layout).setVisibility(View.VISIBLE);
        else {
            resetInput(R.id.register_input_whatsapp_layout);
            requireView().findViewById(R.id.register_input_whatsapp_layout).setVisibility(View.GONE);
            user.removeContact(ContactType.WhatsApp);
            ((TextInputLayout) requireView().findViewById(R.id.register_input_whatsapp_layout)).setError(null);
        }
        if (this.chip_email)
            requireView().findViewById(R.id.register_input_email_layout).setVisibility(View.VISIBLE);
        else {
            resetInput(R.id.register_input_email_layout);
            requireView().findViewById(R.id.register_input_email_layout).setVisibility(View.GONE);
            user.removeContact(ContactType.Mail);
            ((TextInputLayout) requireView().findViewById(R.id.register_input_email_layout)).setError(null);
        }
        if (this.chip_other)
            requireView().findViewById(R.id.register_input_other_layout).setVisibility(View.VISIBLE);
        else {
            resetInput(R.id.register_input_other_layout);
            requireView().findViewById(R.id.register_input_other_layout).setVisibility(View.GONE);
            user.removeContact(ContactType.Other);
            ((TextInputLayout) requireView().findViewById(R.id.register_input_other_layout)).setError(null);
        }

        this.registerViewModel.updateUser(user);
    }

    /**
     * Resets an TextInputLayout
     *
     * @param id the id of the TextInputLayout
     */
    private void resetInput(int id) {
        TextInputLayout textInputLayout = requireView().findViewById(id);
        Objects.requireNonNull(textInputLayout.getEditText()).setText("");
    }

    /**
     * Resets the errors on all TextInputLayouts
     */
    private void resetFormErrors() {
        TextInputLayout forename_layout = requireView().findViewById(R.id.register_vorname_layout);
        TextInputLayout lastname_layout = requireView().findViewById(R.id.register_nachname_layout);
        TextInputLayout email_layout = requireView().findViewById(R.id.register_email_layout);
        TextInputLayout password_layout = requireView().findViewById(R.id.register_password_layout);
        TextInputLayout password_repeat_layout = requireView().findViewById(R.id.register_password_repeat_layout);
        TextView no_chip_text = requireView().findViewById(R.id.register_error_no_chip);
        TextInputLayout phone_layout = requireView().findViewById(R.id.register_input_phone_layout);
        TextInputLayout whatsapp_layout = requireView().findViewById(R.id.register_input_whatsapp_layout);
        TextInputLayout email_adr_layout = requireView().findViewById(R.id.register_input_email_layout);
        TextInputLayout other_layout = requireView().findViewById(R.id.register_input_other_layout);

        //Reset fields
        forename_layout.setError(null);
        lastname_layout.setError(null);
        email_layout.setError(null);
        password_layout.setError(null);
        password_repeat_layout.setError(null);
        no_chip_text.setVisibility(View.INVISIBLE);
        phone_layout.setError(null);
        whatsapp_layout.setError(null);
        email_adr_layout.setError(null);
        other_layout.setError(null);
    }

    /**
     * Validates the form on input correctness
     *
     * @return state if form is valid
     */
    private boolean validateForm() {
        TextInputLayout forename_layout = requireView().findViewById(R.id.register_vorname_layout);
        TextInputLayout lastname_layout = requireView().findViewById(R.id.register_nachname_layout);
        TextInputLayout email_layout = requireView().findViewById(R.id.register_email_layout);
        TextInputLayout password_layout = requireView().findViewById(R.id.register_password_layout);
        TextInputLayout password_repeat_layout = requireView().findViewById(R.id.register_password_repeat_layout);
        TextInputLayout password_old_layout = requireView().findViewById(R.id.register_old_password_layout);

        TextInputEditText forename = requireView().findViewById(R.id.register_vorname);
        TextInputEditText lastname = requireView().findViewById(R.id.register_nachname);
        TextInputEditText email = requireView().findViewById(R.id.register_email);
        TextInputEditText password = requireView().findViewById(R.id.register_password);
        TextInputEditText password_repeat = requireView().findViewById(R.id.register_password_repeat);
        TextInputEditText password_old = requireView().findViewById(R.id.register_old_password);

        TextView no_chip_text = requireView().findViewById(R.id.register_error_no_chip);
        TextInputLayout phone_layout = requireView().findViewById(R.id.register_input_phone_layout);
        TextInputLayout whatsapp_layout = requireView().findViewById(R.id.register_input_whatsapp_layout);
        TextInputLayout email_adr_layout = requireView().findViewById(R.id.register_input_email_layout);
        TextInputLayout other_layout = requireView().findViewById(R.id.register_input_other_layout);

        TextInputEditText phone = requireView().findViewById(R.id.register_input_phone);
        TextInputEditText whatsapp = requireView().findViewById(R.id.register_input_whatsapp);
        TextInputEditText email_adr = requireView().findViewById(R.id.register_input_email);
        TextInputEditText other = requireView().findViewById(R.id.register_input_other);

        //Validations Text
        if (this.isChangeProfile) {
            //Validate mail
            if (!Objects.requireNonNull(email.getText()).toString().isEmpty() && !this.isEmailValid(Objects.requireNonNull(email.getText()).toString()))
                email_layout.setError(getText(R.string.register_wrong_format));
            //Validate pws
            if (!Objects.requireNonNull(password.getText()).toString().isEmpty() || !Objects.requireNonNull(password_repeat.getText()).toString().isEmpty()) {
                if (!this.isPasswordValid(Objects.requireNonNull(password.getText()).toString()))
                    password_layout.setError(getText(R.string.register_wrong_pw_format));
                if (!password.getText().toString().equals(Objects.requireNonNull(password_repeat.getText()).toString())) {
                    password_layout.setError(getText(R.string.register_no_pw_same));
                    password_repeat_layout.setError(getText(R.string.register_no_pw_same));
                }
                if (!this.isPasswordValid(password_repeat.getText().toString()))
                    password_repeat_layout.setError(getText(R.string.register_wrong_pw_format));
            }
        } else {
            if (!this.isEmailValid(Objects.requireNonNull(email.getText()).toString()))
                email_layout.setError(getText(R.string.register_wrong_format));
            if (!this.isPasswordValid(Objects.requireNonNull(password.getText()).toString()))
                password_layout.setError(getText(R.string.register_wrong_pw_format));
            if (!password.getText().toString().equals(Objects.requireNonNull(password_repeat.getText()).toString())) {
                password_layout.setError(getText(R.string.register_no_pw_same));
                password_repeat_layout.setError(getText(R.string.register_no_pw_same));
            }
            if (!this.isPasswordValid(password_repeat.getText().toString()))
                password_repeat_layout.setError(getText(R.string.register_wrong_pw_format));
        }

        //Validate chips
        if (!chip_phone && !chip_whatsapp && !chip_email && !chip_other)
            no_chip_text.setVisibility(View.VISIBLE);
        if (chip_email && !this.isEmailValid(Objects.requireNonNull(email_adr.getText()).toString()))
            email_adr_layout.setError(getText(R.string.register_wrong_format));
        if (chip_phone && !this.isPhoneNumberValid(Objects.requireNonNull(phone.getText()).toString()))
            phone_layout.setError(getText(R.string.register_wrong_format));
        if (chip_whatsapp && !this.isPhoneNumberValid(Objects.requireNonNull(whatsapp.getText()).toString()))
            whatsapp_layout.setError(getText(R.string.register_wrong_format));

        //Check empty fields
        if (this.isChangeProfile) {
            //If email change
            if (!email.getText().toString().isEmpty() && Objects.requireNonNull(password_old.getText()).toString().isEmpty())
                password_old_layout.setError(getText(R.string.register_empty_field));
            if ((!password.getText().toString().isEmpty() || !password_repeat.getText().toString().isEmpty()) && Objects.requireNonNull(password_old.getText()).toString().isEmpty())
                password_old_layout.setError(getText(R.string.register_empty_field));

        } else {
            if (Objects.requireNonNull(forename.getText()).toString().isEmpty())
                forename_layout.setError(getText(R.string.register_empty_field));
            if (Objects.requireNonNull(lastname.getText()).toString().isEmpty())
                lastname_layout.setError(getText(R.string.register_empty_field));
            if (email.getText().toString().isEmpty())
                email_layout.setError(getText(R.string.register_empty_field));
            if (password.getText().toString().isEmpty())
                password_layout.setError(getText(R.string.register_empty_field));
            if (password_repeat.getText().toString().isEmpty())
                password_repeat_layout.setError(getText(R.string.register_empty_field));
        }

        //Validate chips
        if (!chip_phone && !chip_whatsapp && !chip_email && !chip_other)
            no_chip_text.setVisibility(View.VISIBLE);
        if (chip_phone && Objects.requireNonNull(phone.getText()).toString().isEmpty())
            phone_layout.setError(getText(R.string.register_empty_field));
        if (chip_whatsapp && Objects.requireNonNull(whatsapp.getText()).toString().isEmpty())
            whatsapp_layout.setError(getText(R.string.register_empty_field));
        if (chip_email && Objects.requireNonNull(email_adr.getText()).toString().isEmpty())
            email_adr_layout.setError(getText(R.string.register_empty_field));
        if (chip_other && Objects.requireNonNull(other.getText()).toString().isEmpty())
            other_layout.setError(getText(R.string.register_empty_field));

        //Set returns
        if (isChangeProfile) {
            if (!email.getText().toString().isEmpty() && Objects.requireNonNull(password_old.getText()).toString().isEmpty())
                return false;
            if ((!password.getText().toString().isEmpty() || !password_repeat.getText().toString().isEmpty()) && Objects.requireNonNull(password_old.getText()).toString().isEmpty())
                return false;
            if (!email.getText().toString().isEmpty() && !this.isEmailValid(Objects.requireNonNull(email.getText()).toString()))
                return false;
            if (!password.getText().toString().isEmpty() || !password_repeat.getText().toString().isEmpty()) {
                return this.isPasswordValid(Objects.requireNonNull(password.getText()).toString()) &&
                        password.getText().toString().equals(Objects.requireNonNull(password_repeat.getText()).toString()) &&
                        this.isPasswordValid(password_repeat.getText().toString());
            }
        } else {
            if (Objects.requireNonNull(forename.getText()).toString().isEmpty() ||
                    Objects.requireNonNull(lastname.getText()).toString().isEmpty() ||
                    email.getText().toString().isEmpty() ||
                    password.getText().toString().isEmpty() ||
                    password_repeat.getText().toString().isEmpty()) return false;

            if (!chip_phone && !chip_whatsapp && !chip_email && !chip_other) return false;

            if ((chip_phone && Objects.requireNonNull(phone.getText()).toString().isEmpty()) ||
                    (chip_whatsapp && Objects.requireNonNull(whatsapp.getText()).toString().isEmpty()) ||
                    (chip_email && Objects.requireNonNull(email_adr.getText()).toString().isEmpty()) ||
                    (chip_other && Objects.requireNonNull(other.getText()).toString().isEmpty()))
                return false;

            if ((chip_email && !this.isEmailValid(Objects.requireNonNull(email_adr.getText()).toString())) ||
                    (chip_phone && !this.isPhoneNumberValid(Objects.requireNonNull(phone.getText()).toString())) ||
                    (chip_whatsapp && !this.isPhoneNumberValid(Objects.requireNonNull(whatsapp.getText()).toString())))
                return false;

            //Suppress because it is more readable if not simplified because
            //it matches method pattern
            //noinspection RedundantIfStatement
            if (!this.isEmailValid(email.getText().toString()) ||
                    !this.isPasswordValid(password.getText().toString()) ||
                    !this.isPasswordValid(password_repeat.getText().toString())) return false;
        }

        return true;
    }

    /**
     * Validates the email address via regex
     *
     * @param email Email to validate
     * @return true if email is valid false otherwise
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    // Suppressed because it's cleaner to read functionality if returned this way. Negated pattern would not be intuitive
    private boolean isEmailValid(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    /**
     * Validates the password based on its length
     *
     * @param password Password to validate
     * @return true if password is valid otherwise false
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    // Suppressed because it's cleaner to read functionality if returned this way. Negated pattern would not be intuitive
    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

    /**
     * Validates phone number based on valid format
     *
     * @param phoneNumber Phone number to validate
     * @return true if phone number is valid otherwise false
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    // Suppressed because it's cleaner to read functionality if returned this way. Negated pattern would not be intuitive
    private boolean isPhoneNumberValid(String phoneNumber) {
        Pattern VALID_PHONE_NUMBER_REGEX = Pattern.compile("^[0-9]*$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_PHONE_NUMBER_REGEX.matcher(phoneNumber);
        return matcher.find();
    }

    private void addChangeListener() {
        TextInputLayout forename_layout = requireView().findViewById(R.id.register_vorname_layout);
        TextInputLayout lastname_layout = requireView().findViewById(R.id.register_nachname_layout);
        TextInputLayout email_layout = requireView().findViewById(R.id.register_email_layout);
        TextInputLayout password_layout = requireView().findViewById(R.id.register_password_layout);
        TextInputLayout password_repeat_layout = requireView().findViewById(R.id.register_password_repeat_layout);
        TextInputLayout password_old_layout = requireView().findViewById(R.id.register_old_password_layout);

        TextInputEditText forename = requireView().findViewById(R.id.register_vorname);
        TextInputEditText lastname = requireView().findViewById(R.id.register_nachname);
        TextInputEditText email = requireView().findViewById(R.id.register_email);
        TextInputEditText password = requireView().findViewById(R.id.register_password);
        TextInputEditText password_repeat = requireView().findViewById(R.id.register_password_repeat);
        TextInputEditText password_old = requireView().findViewById(R.id.register_old_password);

        TextInputLayout phone_layout = requireView().findViewById(R.id.register_input_phone_layout);
        TextInputLayout whatsapp_layout = requireView().findViewById(R.id.register_input_whatsapp_layout);
        TextInputLayout email_adr_layout = requireView().findViewById(R.id.register_input_email_layout);
        TextInputLayout other_layout = requireView().findViewById(R.id.register_input_other_layout);

        TextInputEditText phone = requireView().findViewById(R.id.register_input_phone);
        TextInputEditText whatsapp = requireView().findViewById(R.id.register_input_whatsapp);
        TextInputEditText email_adr = requireView().findViewById(R.id.register_input_email);
        TextInputEditText other = requireView().findViewById(R.id.register_input_other);

        forename.addTextChangedListener(new ErrorEditClear(forename_layout));
        lastname.addTextChangedListener(new ErrorEditClear(lastname_layout));
        email.addTextChangedListener(new ErrorEditClear(email_layout));
        password.addTextChangedListener(new ErrorEditClear(password_layout));
        password_repeat.addTextChangedListener(new ErrorEditClear(password_repeat_layout));
        password_old.addTextChangedListener(new ErrorEditClear(password_old_layout));
        phone.addTextChangedListener(new ErrorEditClear(phone_layout));
        whatsapp.addTextChangedListener(new ErrorEditClear(whatsapp_layout));
        email_adr.addTextChangedListener(new ErrorEditClear(email_adr_layout));
        other.addTextChangedListener(new ErrorEditClear(other_layout));
    }

    /**
     * Dynamic remove error text when retyping
     */
    private static class ErrorEditClear implements TextWatcher {

        private final TextInputLayout layout;

        public ErrorEditClear(TextInputLayout layout) {
            this.layout = layout;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (layout.getError() != null && !layout.getError().toString().isEmpty()) {
                layout.setError("");
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


}