package de.hda.fbi.nzse.sose22.safespace.domain.model;

/**
 * Types how a person can be contacted
 */
public enum ContactType {
    Phone,
    WhatsApp,
    Mail,
    Other,
}
