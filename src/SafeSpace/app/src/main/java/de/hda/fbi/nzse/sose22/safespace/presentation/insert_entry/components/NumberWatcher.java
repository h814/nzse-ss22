package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

public interface NumberWatcher {
    void onNumberChanged(NumberPlusMinusView view, int number);
}