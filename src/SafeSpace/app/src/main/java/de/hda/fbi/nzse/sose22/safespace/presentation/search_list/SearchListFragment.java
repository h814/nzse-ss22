package de.hda.fbi.nzse.sose22.safespace.presentation.search_list;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.FavoriteRepositorySharedPrefs;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferType;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.OfferListAdapter;
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

@AndroidEntryPoint
public class SearchListFragment extends Fragment {

    @Inject
    @OfferRepositoryFirestore
    OfferRepository offerRepository;

    @Inject
    @FavoriteRepositorySharedPrefs
    FavoriteRepository favoriteRepository;

    private BottomSheetBehavior<NestedScrollView> bottomSheetBehavior;
    private List<Offer> offers;

    private SearchListViewModel searchListViewModel;

    private boolean filterActive = false;

    public SearchListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_list, container, false);
    }

    // Use notifyDataSetChanged because whole list can change
    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.offers = new ArrayList<>();
        super.onViewCreated(view, savedInstanceState);

        MaterialToolbar toolbar = requireView().findViewById(R.id.search_offers_toolbar);
        toolbar.setTitle(R.string.search_offers_search_results);
        toolbar.setNavigationOnClickListener(v -> Navigation.findNavController(requireView()).popBackStack());

        OfferListAdapter offerListAdapter = new OfferListAdapter(offers);
        offerListAdapter.setHasStableIds(true);
        RecyclerView offerList = requireView().findViewById(R.id.search_offers_list);
        offerList.setLayoutManager(new LinearLayoutManager(requireContext()));
        offerList.setAdapter(offerListAdapter);

        //Adding offers
        this.searchListViewModel = new ViewModelProvider(this).get(SearchListViewModel.class);
        searchListViewModel.getOffers().observe(getViewLifecycleOwner(), o -> {
            //Handle offer change
            this.offers.clear();
            this.offers.addAll(this.getFilteredOffers(o));
            offerListAdapter.notifyDataSetChanged();

            //Text
            if (this.offers.isEmpty()) {
                this.setResultNumberText(getString(R.string.search_no_results_found));
            } else {
                this.setResultNumberText(getString(R.string.search_results_found, this.offers.size()));
            }

        });

        //Favourites Swipe
        var self = this;
        ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    var pos = viewHolder.getAbsoluteAdapterPosition();
                    var changedOffer = self.offers.get(pos);
                    boolean isFavourite = favoriteRepository.is(changedOffer.getId());

                    isFavourite = !isFavourite;
                    if (isFavourite) favoriteRepository.add(changedOffer.getId());
                    else favoriteRepository.remove(changedOffer.getId());

                    offerListAdapter.notifyItemChanged((pos));
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                var pos = viewHolder.getAbsoluteAdapterPosition();
                var changedOffer = self.offers.get(pos);
                boolean isFavourite = favoriteRepository.is(changedOffer.getId());

                int drawableId;
                if (isFavourite) drawableId = R.drawable.ic_favorite_rounded_filled_48;
                else drawableId = R.drawable.ic_outline_favorite_rounded_48;

                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addBackgroundColor(ContextCompat.getColor(requireContext(), R.color.orange_light_40))
                        .addActionIcon(drawableId)
                        .setSwipeLeftActionIconTint(R.color.white)
                        .create()
                        .decorate();

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper helper = new ItemTouchHelper(removeCallback);
        helper.attachToRecyclerView(offerList);

        //Filter
        NestedScrollView linearLayout = requireView().findViewById(R.id.search_offers_filter_bottom_sheet_scroll);
        this.bottomSheetBehavior = BottomSheetBehavior.from(linearLayout);

        requireView().findViewById(R.id.search_offers_filter_button).setOnClickListener(l -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED));
        requireView().findViewById(R.id.filter_close_bar).setOnClickListener(l -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED));
        requireView().findViewById(R.id.filter_submit).setOnClickListener(l -> this.btn_submit_filter());
        requireView().findViewById(R.id.filter_reset).setOnClickListener(l -> this.btn_reset_filter());

    }

    /**
     * Filters all incoming offers based on intern criteria
     *
     * @param allOffers expects list of all offers
     * @return filtered list of offers
     */
    private List<Offer> getFilteredOffers(List<Offer> allOffers) {
        if (!this.filterActive) return allOffers;

        //Get UI values
        final boolean type_room = ((Chip) requireView().findViewById(R.id.filter_offer_type_group_room)).isChecked();
        final boolean type_flat = ((Chip) requireView().findViewById(R.id.filter_offer_type_group_flat)).isChecked();
        final boolean type_house = ((Chip) requireView().findViewById(R.id.filter_offer_type_group_house)).isChecked();

        int amount_persons = -1, amount_rooms = -1;
        try {
            String string_amount_persons = ((TextView) requireView().findViewById(R.id.filter_persons_amount_input)).getText().toString();
            String string_amount_rooms = ((TextView) requireView().findViewById(R.id.filter_room_amount_input)).getText().toString();
            if (!string_amount_persons.isEmpty())
                amount_persons = Integer.parseInt(string_amount_persons);
            if (!string_amount_rooms.isEmpty())
                amount_rooms = Integer.parseInt(string_amount_rooms);
        } catch (NumberFormatException e) {
            amount_persons = 0;
            amount_rooms = 0;
        }
        final boolean equipment_bathroom = ((Chip) requireView().findViewById(R.id.filter_equpiment_bathroom)).isChecked();
        final boolean equipment_kitchen = ((Chip) requireView().findViewById(R.id.filter_equpiment_kitchen)).isChecked();

        final boolean language_german = ((Chip) requireView().findViewById(R.id.filter_language_german)).isChecked();
        final boolean language_english = ((Chip) requireView().findViewById(R.id.filter_language_english)).isChecked();
        final boolean language_ukrainian = ((Chip) requireView().findViewById(R.id.filter_language_ukrainian)).isChecked();
        final boolean language_other = ((Chip) requireView().findViewById(R.id.filter_language_other)).isChecked();


        //Check every offer
        List<Offer> filteredOffers = new ArrayList<>();
        for (Offer o : allOffers) {
            if ((type_room && o.getType().equals(OfferType.Room))
                    || (type_flat && o.getType().equals(OfferType.Flat))
                    || (type_house && o.getType().equals(OfferType.House))
                    || (equipment_bathroom && o.isHasBath())
                    || (equipment_kitchen && o.isHasKitchen())
                    || (language_german && o.getYourLanguages().contains("de"))
                    || (language_english && o.getYourLanguages().contains("en"))
                    || (language_ukrainian && o.getYourLanguages().contains("uk"))
                    || (language_other && o.getYourLanguages().contains("other"))
                    || ((amount_persons > 0) && amount_persons <= o.getMaxAcceptedPersons())
                    || ((amount_rooms > 0) && amount_rooms <= o.getRoomCount())) {
                filteredOffers.add(o);
            }
        }

        //Check if filter active but no properties set
        if (!type_room && !type_flat && !type_house
                && !equipment_bathroom && !equipment_kitchen
                && !language_german && !language_english && !language_ukrainian && !language_other
                && amount_persons <= 0 && amount_rooms <= 0) {
            return allOffers;
        }

        return filteredOffers;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.searchListViewModel.update();
    }

    //Button event when submit is pressed
    private void btn_submit_filter() {
        this.filterActive = true;
        this.searchListViewModel.update();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    //Button event when reset is pressed
    private void btn_reset_filter() {
        ((Chip) requireView().findViewById(R.id.filter_offer_type_group_room)).setChecked(false);
        ((Chip) requireView().findViewById(R.id.filter_offer_type_group_flat)).setChecked(false);
        ((Chip) requireView().findViewById(R.id.filter_offer_type_group_house)).setChecked(false);

        ((TextView) requireView().findViewById(R.id.filter_persons_amount_input)).setText("");
        ((TextView) requireView().findViewById(R.id.filter_room_amount_input)).setText("");

        ((Chip) requireView().findViewById(R.id.filter_equpiment_bathroom)).setChecked(false);
        ((Chip) requireView().findViewById(R.id.filter_equpiment_kitchen)).setChecked(false);

        ((Chip) requireView().findViewById(R.id.filter_language_german)).setChecked(false);
        ((Chip) requireView().findViewById(R.id.filter_language_english)).setChecked(false);
        ((Chip) requireView().findViewById(R.id.filter_language_ukrainian)).setChecked(false);
        ((Chip) requireView().findViewById(R.id.filter_language_other)).setChecked(false);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        this.filterActive = false;
        this.searchListViewModel.update();
    }

    /**
     * Prints text to label
     *
     * @param text The text which ought to be displayed
     */
    private void setResultNumberText(String text) {
        TextView labelAmountOffers = requireView().findViewById(R.id.search_offers_results_number);

        if (filterActive) text += " - " + getString(R.string.filter_active);

        labelAmountOffers.setText(text);
    }

}