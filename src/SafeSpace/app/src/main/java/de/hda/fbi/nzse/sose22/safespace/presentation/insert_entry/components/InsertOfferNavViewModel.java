package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InsertOfferNavViewModel extends ViewModel {

    private final MutableLiveData<Integer> page;
    private final MutableLiveData<Fragment> activeFragment;

    public InsertOfferNavViewModel() {
        this.page = new MutableLiveData<>();
        this.page.setValue(0);

        this.activeFragment = new MutableLiveData<>();
    }

    public LiveData<Integer> getPage() {
        return this.page;
    }

    public LiveData<Fragment> getActiveFragment() {
        return this.activeFragment;
    }

    public void setActiveFragment(Fragment f) {
        this.activeFragment.postValue(f);
    }

    public void previous() {

        if (this.page.getValue() == null) return;

        if (this.page.getValue() > 0) {
            this.page.postValue(this.page.getValue() - 1);
        }
    }

    public void next() {
        if (this.page.getValue() == null) return;
        this.page.postValue(this.page.getValue() + 1);
    }

}
