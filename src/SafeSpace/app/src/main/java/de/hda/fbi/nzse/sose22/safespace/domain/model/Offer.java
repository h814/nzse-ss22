package de.hda.fbi.nzse.sose22.safespace.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;

/*
 * TODO
 * Disabled unused for now. We wait until we implemented the correct way of storing the data (firebase) also with the images.
 * Then we will clean up unused methods and remove the suppress
 * */
@SuppressWarnings("unused")
public class Offer {
    private final List<OfferAcceptedGroup> acceptedGroup;
    private UUID id;
    private UUID insertedByUserId;
    private OfferType type;
    private int roomCount;
    private boolean hasBath;
    private boolean hasKitchen;
    private int maxAcceptedPersons;
    private List<String> yourLanguages;
    private Address address;
    private boolean showFullAddress;
    private String description;
    private List<String> tags;
    private List<URI> imageURIs;
    private int showcaseImageIndex;

    public Offer() {
        this.id = UUID.randomUUID();
        this.insertedByUserId = null;
        this.type = OfferType.Room;
        this.roomCount = 1;
        this.hasBath = false;
        this.hasKitchen = false;
        this.acceptedGroup = new ArrayList<>();
        this.maxAcceptedPersons = 1;
        this.yourLanguages = new ArrayList<>();
        this.address = new Address("", "", 0, "");
        this.showFullAddress = false;
        this.description = "";
        this.tags = new ArrayList<>();
        this.imageURIs = new ArrayList<>();
        this.showcaseImageIndex = 0;
    }

    public Offer(UUID id, UUID insertedByUserId, OfferType type, int roomCount, boolean hasBath, boolean hasKitchen, Set<OfferAcceptedGroup> acceptedGroup, int maxAcceptedPersons, List<String> yourLanguages, Address address, boolean showFullAddress, String description, List<String> tags, List<URI> imageURIs, int showcaseImageIndex) {
        this.id = id;
        this.insertedByUserId = insertedByUserId;
        this.type = type;
        this.roomCount = roomCount;
        this.hasBath = hasBath;
        this.hasKitchen = hasKitchen;
        this.acceptedGroup = new ArrayList<>(acceptedGroup);
        this.maxAcceptedPersons = maxAcceptedPersons;
        this.yourLanguages = yourLanguages;
        this.address = address;
        this.showFullAddress = showFullAddress;
        this.description = description;
        this.tags = tags;
        this.imageURIs = imageURIs;
        this.showcaseImageIndex = showcaseImageIndex;
    }

    //TODO remove for prod use
    public Offer(OfferType offerType, int roomCount) {
        this();
        this.type = offerType;
        this.roomCount = roomCount;
    }

    public UUID getInsertedByUserId() {
        return insertedByUserId;
    }

    public void setInsertedByUserId(UUID insertedByUserId) {
        this.insertedByUserId = insertedByUserId;
    }

    public OfferType getType() {
        return type;
    }

    public void setType(OfferType type) {
        this.type = type;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public boolean isHasBath() {
        return hasBath;
    }

    public void setHasBath(boolean hasBath) {
        this.hasBath = hasBath;
    }

    public boolean isHasKitchen() {
        return hasKitchen;
    }

    public void setHasKitchen(boolean hasKitchen) {
        this.hasKitchen = hasKitchen;
    }

    public Set<OfferAcceptedGroup> getAcceptedGroup() {
        return new HashSet<>(acceptedGroup);
    }

    public void addAcceptedGroup(OfferAcceptedGroup acceptedGroup) {
        if (!this.acceptedGroup.contains(acceptedGroup)) {
            this.acceptedGroup.add(acceptedGroup);
        }
    }

    public void removeAcceptedGroup(OfferAcceptedGroup acceptedGroup) {
        this.acceptedGroup.remove(acceptedGroup);
    }

    public int getMaxAcceptedPersons() {
        return maxAcceptedPersons;
    }

    public void setMaxAcceptedPersons(int maxAcceptedPersons) {
        this.maxAcceptedPersons = maxAcceptedPersons;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isShowFullAddress() {
        return showFullAddress;
    }

    public void setShowFullAddress(boolean showFullAddress) {
        this.showFullAddress = showFullAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getShowcaseImageIndex() {
        return showcaseImageIndex;
    }

    public void setShowcaseImageIndex(int showcaseImageIndex) {
        this.showcaseImageIndex = showcaseImageIndex;
    }

    public List<String> getYourLanguages() {
        return yourLanguages;
    }

    public void setYourLanguages(List<String> yourLanguages) {
        this.yourLanguages = yourLanguages;
    }

    public void addLanguages(String lang) {
        yourLanguages.add(lang);
    }

    public void removeLanguages(String lang) {
        yourLanguages.remove(lang);
    }

    public void removeLanguages(int i) {
        yourLanguages.remove(i);
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag) {
        tags.remove(tag);
    }

    public void removeTag(String tag) {
        tags.remove(tag);
    }

    public void removeTag(int i) {
        tags.remove(i);
    }

    public List<URI> getImageURIs() {
        return imageURIs;
    }

    public void setImageURIs(List<URI> uris) {
        imageURIs = uris;
    }

    public void addImageURI(URI image) {
        imageURIs.add(image);
    }

    public void removeImageURI(URI image) {
        imageURIs.remove(image);
    }

    public void removeImageURI(int i) {
        imageURIs.remove(i);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Offer)) return false;

        Offer offer = (Offer) o;

        return new EqualsBuilder()
                .append(roomCount, offer.roomCount)
                .append(hasBath, offer.hasBath)
                .append(hasKitchen, offer.hasKitchen)
                .append(maxAcceptedPersons, offer.maxAcceptedPersons)
                .append(showFullAddress, offer.showFullAddress)
                .append(showcaseImageIndex, offer.showcaseImageIndex)
                .append(insertedByUserId, offer.insertedByUserId)
                .append(type, offer.type)
                .append(acceptedGroup, offer.acceptedGroup)
                .append(yourLanguages, offer.yourLanguages)
                .append(address, offer.address)
                .append(description, offer.description)
                .append(tags, offer.tags)
                .append(imageURIs, offer.imageURIs)
                .append(id, offer.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(insertedByUserId).append(type)
                .append(roomCount).append(hasBath)
                .append(hasKitchen).append(acceptedGroup)
                .append(maxAcceptedPersons).append(yourLanguages)
                .append(address)
                .append(showFullAddress)
                .append(description)
                .append(tags)
                .append(imageURIs)
                .append(showcaseImageIndex)
                .append(id)
                .toHashCode();
    }

    @Nonnull
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("insertedByUserId", insertedByUserId)
                .append("type", type)
                .append("roomCount", roomCount)
                .append("hasBath", hasBath)
                .append("hasKitchen", hasKitchen)
                .append("acceptedGroup", acceptedGroup)
                .append("maxAcceptedPersons", maxAcceptedPersons)
                .append("yourLanguages", yourLanguages)
                .append("address", address)
                .append("showFullAddress", showFullAddress)
                .append("description", description)
                .append("tags", tags)
                .append("imageURLs", imageURIs)
                .append("showcaseImageIndex", showcaseImageIndex)
                .toString();
    }
}
