package de.hda.fbi.nzse.sose22.safespace.domain.repository;

import com.google.android.gms.tasks.Task;

import java.util.UUID;

import de.hda.fbi.nzse.sose22.safespace.domain.model.User;

public interface UserRepository {
    Task<User> getUserById(UUID id);

    Task<User> getUserById(String otherId);

    Task<Boolean> alterUser(String id, User user);

    Task<Boolean> createUser(String docName, User user);
}
