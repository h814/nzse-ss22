package de.hda.fbi.nzse.sose22.safespace.presentation.offer_detail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import java.util.UUID;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.BrowseOffers;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.InsertEntryFragment;

@HiltViewModel
public class OfferDetailViewModel extends ViewModel {
    private final MutableLiveData<Offer> myOffer;
    private final MutableLiveData<User> myUser;

    private final OfferRepository offerRepository;
    private final UserRepository userRepository;
    private final UUID id;

    @Inject
    public OfferDetailViewModel(@OfferRepositoryFirestore OfferRepository offerRepository, @UserRepositoryFirestore UserRepository userRepository, SavedStateHandle savedStateHandle) {
        this.id = savedStateHandle.get(InsertEntryFragment.EDIT_OFFER_ID_KEY);
        this.offerRepository = offerRepository;
        this.userRepository = userRepository;
        myOffer = new MutableLiveData<>();
        myUser = new MutableLiveData<>();
        if (id == null) myOffer.setValue(new Offer());
        else {
            BrowseOffers browseOffers = new BrowseOffers(offerRepository);
            browseOffers.getByOfferId(id).addOnSuccessListener(offer -> {
                myOffer.setValue(offer);
                userRepository.getUserById(offer.getInsertedByUserId()).addOnSuccessListener(myUser::setValue);
            });
        }
    }

    public LiveData<User> getMyUser() {
        return myUser;
    }

    public LiveData<Offer> getMyOffer() {
        return myOffer;
    }

    public void update() {
        if (id == null) myOffer.setValue(new Offer());
        else {
            BrowseOffers browseOffers = new BrowseOffers(offerRepository);
            browseOffers.getByOfferId(id).addOnSuccessListener(offer -> {
                myOffer.postValue(offer);
                userRepository.getUserById(offer.getInsertedByUserId()).addOnSuccessListener(myUser::setValue);
            });
        }
    }
}
