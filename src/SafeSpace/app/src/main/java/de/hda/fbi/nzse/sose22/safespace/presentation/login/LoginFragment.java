package de.hda.fbi.nzse.sose22.safespace.presentation.login;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.apache.commons.validator.routines.EmailValidator;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

@AndroidEntryPoint
public class LoginFragment extends Fragment {

    //TODO better error messages
    //TODO login loading screen

    private UserViewModel userViewModel;
    private final boolean waitForLoginResponse;

    public LoginFragment() {
        waitForLoginResponse = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        this.userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);

        userViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                back();
            } else if (waitForLoginResponse) {
                TextView error = requireView().findViewById(R.id.login_error_hint);
                error.setVisibility(View.VISIBLE);
                error.setText(R.string.login_error);
                requireView().findViewById(R.id.login_login_button).setEnabled(true);
            }
        });

        requireView().findViewById(R.id.login_login_button).setOnClickListener(this::navigateProfile);

        requireView().findViewById(R.id.login_register_button).setOnClickListener(v -> {
            var dst = LoginFragmentDirections.actionMainNavigationLoginToMainNavigationRegister();
            Navigation.findNavController(requireView()).navigate(dst);
        });

        ((MaterialToolbar) requireView().findViewById(R.id.login_toolbar)).setNavigationOnClickListener(v -> back());

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                //Prevent default back press event and call close
                back();
            }
        });

        TextInputEditText email = requireView().findViewById(R.id.login_email_address);
        TextInputLayout email_layout = requireView().findViewById(R.id.login_email_address_layout);
        email.addTextChangedListener(new ErrorEditClear(email_layout));

        TextInputEditText password = requireView().findViewById(R.id.login_password_input);
        TextInputLayout password_layout = requireView().findViewById(R.id.login_password_input_layout);
        password.addTextChangedListener(new ErrorEditClear(password_layout));
    }

    private void back() {

        var callbackID = LoginFragmentArgs.fromBundle(getArguments()).getCallbackID();

        var nav = Navigation.findNavController(requireView());
        if (callbackID != -1) {
            try {
                nav.navigate(callbackID);
            } catch (Exception ignored) {
                nav.navigateUp();
            }
        } else {
            nav.navigateUp();
        }
    }

    private void navigateProfile(View view) {

        Button loginBtn = requireView().findViewById(R.id.login_login_button);

        TextInputEditText email = requireView().findViewById(R.id.login_email_address);
        TextInputLayout email_layout = requireView().findViewById(R.id.login_email_address_layout);

        TextInputEditText password = requireView().findViewById(R.id.login_password_input);
        TextInputLayout password_layout = requireView().findViewById(R.id.login_password_input_layout);

        if (email.getText() == null || password.getText() == null) {
            throw new IllegalStateException("Email or password text is unexpected null");
        }

        boolean hasError = false;

        if (email.getText().toString().isEmpty()) {
            hasError = true;
            email_layout.setError(getString(R.string.login_insert_email_error));
        }

        if (!EmailValidator.getInstance().isValid(email.getText().toString())) {
            hasError = true;
            email_layout.setError(getString(R.string.login_valid_email_error));
        }

        if (password.getText().toString().isEmpty()) {
            hasError = true;
            password_layout.setError(getString(R.string.login_insert_password_error));
        }

        if (hasError) return;

        loginBtn.setEnabled(false);
        loginBtn.setClickable(false);

        requireView().findViewById(R.id.progress_linear).setVisibility(View.VISIBLE);

        var loginTask = this.userViewModel.login(email.getText().toString(), password.getText().toString());

        loginTask.addOnCompleteListener(task -> {

            requireView().findViewById(R.id.progress_linear).setVisibility(View.GONE);

            if (task.isSuccessful() && task.getResult()) return;

            String msg = getString(R.string.unknown_error);

            if (task.getException() != null) {
                msg = task.getException().getMessage();
            } else if (!task.getResult()) {
                msg = getString(R.string.internal_error_try_again);
            }

            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
            builder
                    .setTitle(R.string.login_failed_dialog_title)
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        dialog.dismiss();
                        loginBtn.setClickable(true);
                        loginBtn.setEnabled(true);
                    }).show();
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    private static class ErrorEditClear implements TextWatcher {

        final TextInputLayout layout;

        public ErrorEditClear(TextInputLayout layout) {
            this.layout = layout;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (layout.getError() != null && !layout.getError().toString().isEmpty()) {
                layout.setError("");
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}