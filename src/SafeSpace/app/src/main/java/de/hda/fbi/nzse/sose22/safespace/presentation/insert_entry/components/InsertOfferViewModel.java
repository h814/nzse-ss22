package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;

import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceFirebase;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.ImageRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.InsertEntryFragment;

@HiltViewModel
public class InsertOfferViewModel extends ViewModel {

    private final MutableLiveData<Offer> newOffer = new MutableLiveData<>();
    private final MutableLiveData<List<Uri>> imageUris = new MutableLiveData<>();
    private final OfferRepository offerRepository;
    private final ImageRepository imageRepository;
    private final UserService userService;
    private boolean isNew;

    @Inject
    public InsertOfferViewModel(
            @OfferRepositoryFirestore OfferRepository offerRepository,
            @UserServiceFirebase UserService userService,
            ImageRepository imageRepository,
            SavedStateHandle savedState
    ) {
        this.offerRepository = offerRepository;
        this.imageRepository = imageRepository;
        this.userService = userService;
        if (savedState.<UUID>get(InsertEntryFragment.EDIT_OFFER_ID_KEY) != null && savedState.get(InsertEntryFragment.EDIT_OFFER_ID_KEY) != null) {
            Log.d(getClass().getName(), "Got offer id: " + savedState.<UUID>get(InsertEntryFragment.EDIT_OFFER_ID_KEY));
            this.isNew = false;
            offerRepository.getById(savedState.get(InsertEntryFragment.EDIT_OFFER_ID_KEY)).addOnSuccessListener(newOffer::setValue);
        } else {
            this.isNew = true;
            newOffer.setValue(new Offer());
        }

        imageUris.setValue(new ArrayList<>());
    }

    public void setId(UUID id) {
        this.isNew = false;
        offerRepository.getById(id).addOnSuccessListener(newOffer::setValue);
    }

    public LiveData<Offer> getOffer() {
        return this.newOffer;
    }

    public void updateOffer(Offer o) {
        this.newOffer.postValue(o);
    }

    public void updateImageUris(List<Uri> imageList) {
        this.imageUris.postValue(imageList);
    }

    public LiveData<List<Uri>> getIUris() {
        return this.imageUris;
    }

    public void updateLanguages(List<String> languages) {
        var offer = this.newOffer.getValue();

        if (offer == null) throw new IllegalStateException("Offer cannot be null");

        offer.setYourLanguages(languages);
        updateOffer(offer);
    }

    public void updateTags(List<String> tags) {
        var offer = this.newOffer.getValue();
        if (offer == null) throw new IllegalStateException("Offer cannot be null");
        offer.setTags(tags);
        updateOffer(offer);
    }

    public Task<Boolean> save(@NonNull Context context) {
        //save images
        var offer = this.newOffer.getValue();
        if (offer == null || imageUris.getValue() == null)
            throw new IllegalStateException("Offer or image uris cannot be null");

        final List<URI> savedImageUrisForOffer = new ArrayList<>();

        //Check if image is already a URI with scheme of http or https (then is is already uploaded) else upload to firebase storage
        var imageUploadTasks = new ArrayList<Task<URI>>();

        String imageUploadPath = "offer_images/" + Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid() + "/" + offer.getId().toString();

        for (int i = 0; i < this.imageUris.getValue().size(); ++i) {
            var image = this.imageUris.getValue().get(i);

            if (!List.of("http", "https").contains(image.getScheme())) {
                String imageUploadPathWithExtension = imageUploadPath + "/imageID_" + UUID.randomUUID() + ".jpg";

                try {
                    imageUploadTasks.add(this.imageRepository.uploadImage(imageUploadPathWithExtension, context.getContentResolver().openInputStream(image)));
                } catch (FileNotFoundException e) {
                    Log.e("Upload image", "failed to open stream to uri: " + e.getMessage());
                }

                savedImageUrisForOffer.add(null);
            } else {
                savedImageUrisForOffer.add(URI.create(image.toString()));
            }
        }

        return Tasks.whenAllComplete(imageUploadTasks).continueWithTask(urisTasks -> {
            AtomicInteger startCnt = new AtomicInteger(0);

            var uris = urisTasks.getResult();

            //Set uris in correct order
            uris.forEach(uri -> {
                for (int i = startCnt.get(); i < savedImageUrisForOffer.size(); ++i) {

                    var res = (URI) uri.getResult();


                    if (savedImageUrisForOffer.get(i) == null) {
                        savedImageUrisForOffer.set(i, res);
                        startCnt.set(i);
                        break;
                    }

                }
            });

            var storage = FirebaseStorage.getInstance().getReference();
            var imageRef = storage.child(imageUploadPath);

            //Clear removed images
            imageRef.listAll().addOnSuccessListener(task -> task.getItems().forEach(item -> item.getDownloadUrl().addOnSuccessListener(uri -> {
                    if (!savedImageUrisForOffer.contains(URI.create(uri.toString()))) {
                        item.delete();
                    }
                })));

            offer.setImageURIs(savedImageUrisForOffer.stream().filter(Objects::nonNull).collect(Collectors.toList()));
            offer.setInsertedByUserId(this.userService.getUser().getUserId());
            this.updateOffer(offer);

            Task<Boolean> result;

            if (isNew) result = this.offerRepository.createNew(offer);
            else result = this.offerRepository.update(offer.getId(), offer);

            return result;
        });
    }
}
