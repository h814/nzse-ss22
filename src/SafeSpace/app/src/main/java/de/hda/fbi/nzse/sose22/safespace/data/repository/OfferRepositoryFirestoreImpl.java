package de.hda.fbi.nzse.sose22.safespace.data.repository;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.data.remote.dto.MalformedAddressDTOException;
import de.hda.fbi.nzse.sose22.safespace.data.remote.dto.MalformedOfferDTOException;
import de.hda.fbi.nzse.sose22.safespace.data.remote.dto.OfferDTO;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.OfferNotFoundException;

public class OfferRepositoryFirestoreImpl implements OfferRepository {

    @Inject
    public OfferRepositoryFirestoreImpl() {
    }

    @Override
    public Task<List<Offer>> get() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("offers").get().continueWith(task -> {
            if (!task.isSuccessful()) throw new OfferNotFoundException("Failed to fetch offers");

            List<Offer> offers = new ArrayList<>();
            for (var res : task.getResult()) {

                try {
                    OfferDTO dto = OfferDTO.fromFirebase(res);
                    offers.add(dto.toOffer());
                } catch (MalformedAddressDTOException | MalformedOfferDTOException e) {
                    Log.e(getClass().getName(), "failed to create a dto from the data", e);
                }
            }

            return offers;
        });
    }

    @Override
    public Task<Boolean> createNew(Offer o) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        var oDto = OfferDTO.fromOffer(o);
        return db.collection("offers").document(o.getId().toString()).set(oDto.toFirebaseMap()).continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) {
                    throw task.getException();
                }

                throw new RuntimeException("Failed to get offers");
            }

            return true;
        });
    }

    @Override
    public Task<Boolean> update(UUID offerId, Offer updatedOffer) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        var oDto = OfferDTO.fromOffer(updatedOffer);
        return db.collection("offers").document(offerId.toString()).set(oDto.toFirebaseMap()).continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) {
                    throw task.getException();
                }

                throw new RuntimeException("Failed to get offers");
            }

            return true;
        });
    }

    @Override
    public Task<Offer> getById(UUID id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("offers").document(id.toString()).get().continueWith(task -> {
            if (!task.isSuccessful()) throw new OfferNotFoundException("Failed to fetch offers");

            var res = task.getResult();
            try {
                OfferDTO dto = OfferDTO.fromFirebase(res);
                return dto.toOffer();
            } catch (MalformedAddressDTOException | MalformedOfferDTOException e) {
                Log.e(getClass().getName(), "failed to get a dto", e);
                throw e;
            }
        });
    }

    @Override
    public Task<Boolean> removeById(UUID id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        return db.collection("offers").document(id.toString()).delete().continueWith(task -> {
            if (!task.isSuccessful())
                throw new OfferNotFoundException("Failed to delete offer with id " + id);

            return true;
        });
    }
}
