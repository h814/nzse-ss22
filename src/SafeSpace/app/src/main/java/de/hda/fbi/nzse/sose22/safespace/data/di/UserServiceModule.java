package de.hda.fbi.nzse.sose22.safespace.data.di;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceDummy;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceFirebase;
import de.hda.fbi.nzse.sose22.safespace.data.repository.UserServiceDummyImpl;
import de.hda.fbi.nzse.sose22.safespace.data.repository.UserServiceFirebaseImpl;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;

/**
 * Binds the {@link UserService} to concrete implementations for dependency injection.
 * <p>
 * Specifies {@link UserServiceDummyImpl} as {@link UserServiceDummy} and
 * {@link UserServiceFirebaseImpl} as {@link UserServiceFirebase}
 */
//Suppressed because only used by Hilt for Di.
//IDE thinks it is unused
@SuppressWarnings("unused")
@Module
@InstallIn(SingletonComponent.class)
public abstract class UserServiceModule {

    @Binds
    @Singleton
    @UserServiceDummy
    public abstract UserService bindUserRepositoryDummyImpl(
            UserServiceDummyImpl userRepositoryDummy
    );

    @Binds
    @Singleton
    @UserServiceFirebase
    public abstract UserService bindUserServiceFirebase(
            UserServiceFirebaseImpl userServiceFirebase
    );
}