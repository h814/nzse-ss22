package de.hda.fbi.nzse.sose22.safespace.data.di;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryDummy;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.repository.OfferRepositoryDummyImpl;
import de.hda.fbi.nzse.sose22.safespace.data.repository.OfferRepositoryFirestoreImpl;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;

/**
 * Binds the {@link OfferRepository} to concrete implementations for dependency injection.
 * <p>
 * Specifies {@link OfferRepositoryDummyImpl} as {@link OfferRepositoryDummy}
 */
//Suppressed because only used by Hilt for Di.
//IDE thinks it is unused
@SuppressWarnings("unused")
@Module
@InstallIn(SingletonComponent.class)
public abstract class OfferRepositoryModule {

    @Binds
    @Singleton
    @OfferRepositoryDummy
    public abstract OfferRepository bindOfferListRepositoryDummyImpl(
            OfferRepositoryDummyImpl offerListDummyImpl
    );

    @Binds
    @Singleton
    @OfferRepositoryFirestore
    public abstract OfferRepository bindOfferListRepositoryFirestoreImpl(
            OfferRepositoryFirestoreImpl offerRepositoryFirestore
    );
}