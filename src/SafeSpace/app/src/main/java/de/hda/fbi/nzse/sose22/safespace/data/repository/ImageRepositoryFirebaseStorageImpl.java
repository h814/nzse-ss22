package de.hda.fbi.nzse.sose22.safespace.data.repository;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;

import java.io.InputStream;
import java.net.URI;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.repository.ImageRepository;

public class ImageRepositoryFirebaseStorageImpl implements ImageRepository {

    @Inject
    ImageRepositoryFirebaseStorageImpl() {
    }

    @Override
    public Task<URI> uploadImage(String path, InputStream imageStream) {
        var storage = FirebaseStorage.getInstance().getReference();
        var imageRef = storage.child(path);

        return imageRef.putStream(imageStream).continueWithTask(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) {
                    throw task.getException();
                }

                throw new RuntimeException("Failed to upload image on path: " + path);
            }

            return imageRef.getDownloadUrl();
        }).continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) {
                    throw task.getException();
                }

                throw new RuntimeException("Failed to get image URI");
            }

            return URI.create(task.getResult().toString());
        });
    }

    @Override
    public Task<Boolean> deleteImage(String path) {
        var storage = FirebaseStorage.getInstance().getReference();
        var imageRef = storage.child(path);

        return imageRef.delete().continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) {
                    throw task.getException();
                }

                throw new RuntimeException("Failed to delete images on path: " + path);
            }

            return true;
        });
    }
}
