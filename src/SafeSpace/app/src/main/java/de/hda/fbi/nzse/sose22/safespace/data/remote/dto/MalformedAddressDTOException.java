package de.hda.fbi.nzse.sose22.safespace.data.remote.dto;

public class MalformedAddressDTOException extends RuntimeException {
    public MalformedAddressDTOException(String message) {
        super(message);
    }
}