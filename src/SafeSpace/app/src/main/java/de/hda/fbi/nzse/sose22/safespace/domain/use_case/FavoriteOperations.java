package de.hda.fbi.nzse.sose22.safespace.domain.use_case;

import java.util.ArrayList;
import java.util.stream.Collectors;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;

public class FavoriteOperations {

    private final FavoriteRepository favoriteRepository;
    private final OfferRepository offerRepository;

    public FavoriteOperations(FavoriteRepository favoriteRepository, OfferRepository offerRepository) {
        this.favoriteRepository = favoriteRepository;
        this.offerRepository = offerRepository;
    }

    public void cleanNotPresentFavs() {
        var favs = favoriteRepository.get();
        var browseOffers = new BrowseOffers(this.offerRepository);

        browseOffers.getForFavorite(new ArrayList<>(favs)).addOnSuccessListener(favIds -> {
            var offersFoundByFavsIds = favIds.stream().map(Offer::getId).collect(Collectors.toSet());

            favs.forEach(favUUID -> {
                if (!offersFoundByFavsIds.contains(favUUID)) {
                    this.favoriteRepository.remove(favUUID);
                }
            });
        });
    }

}
