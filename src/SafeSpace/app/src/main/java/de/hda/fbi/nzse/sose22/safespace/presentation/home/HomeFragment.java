package de.hda.fbi.nzse.sose22.safespace.presentation.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import de.hda.fbi.nzse.sose22.safespace.R;

public class HomeFragment extends Fragment {

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        var navigation = Navigation.findNavController(view);

        requireView().findViewById(R.id.home_b_offer).setOnClickListener(ignored -> {
            var res = HomeFragmentDirections.actionMainNavigationHomeToMainNavigationInsertEntry().setCallbackID(R.id.main_navigation_home);
            navigation.navigate(res);
        });

        requireView().findViewById(R.id.home_b_search).setOnClickListener(ignored -> {
            var homeFragmentDirections = HomeFragmentDirections.actionMainNavigationHomeToMainNavigationSearchGraph2();
            navigation.navigate(homeFragmentDirections);
        });
    }

}