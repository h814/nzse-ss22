package de.hda.fbi.nzse.sose22.safespace.presentation.my_profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.presentation.register.RegisterFragment;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

public class MyProfileFragment extends Fragment {

    public MyProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        var userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
        userViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            if (user == null) {
                var dir = MyProfileFragmentDirections.actionGlobalProfileToMainNavigationLogin2();
                Navigation.findNavController(requireView()).navigate(dir);
            }
        });

        Fragment my_profile_register = RegisterFragment.newInstance(true);

        getChildFragmentManager().beginTransaction().add(R.id.my_profile_fragment_view, my_profile_register).commit();
    }
}