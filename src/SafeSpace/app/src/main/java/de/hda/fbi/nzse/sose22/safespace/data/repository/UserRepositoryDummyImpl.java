package de.hda.fbi.nzse.sose22.safespace.data.repository;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Contact;
import de.hda.fbi.nzse.sose22.safespace.domain.model.ContactType;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserRepository;

public class UserRepositoryDummyImpl implements UserRepository {

    private static final User dummyUser = new User(UUID.fromString("9ff9ea23-c29d-4f96-ab7e-c93db7c94bea"), "Max", "Muster", "max@muster.de");
    private static final User dummyUser2 = new User(UUID.randomUUID(), "Test", "User", "user@muster.com");


    private final List<User> users;

    @Inject
    public UserRepositoryDummyImpl() {
        dummyUser.addContact(new Contact(ContactType.WhatsApp, "+49123423232"));
        this.users = Stream.of(dummyUser, dummyUser2).collect(Collectors.toList());
    }

    @Override
    public Task<User> getUserById(UUID id) {
        var userOpt = users.stream().filter(user -> user.getUserId().equals(id)).findAny();
        return Tasks.forResult(userOpt.orElse(null));
    }

    @Override
    public Task<User> getUserById(String otherId) {
        return Tasks.forResult(null);
    }

    @Override
    public Task<Boolean> alterUser(String docName, User user) {

        if (!user.getUserId().equals(UUID.fromString(docName))) {
            throw new IllegalArgumentException("id and user->id not matching");
        }

        for (int i = 0; i < this.users.size(); ++i) {
            if (this.users.get(i).getUserId().equals(UUID.fromString(docName))) {
                this.users.set(i, user);
                return Tasks.forResult(true);
            }
        }

        return Tasks.forResult(false);
    }

    @Override
    public Task<Boolean> createUser(String docName, User user) {
        var hasEmail = this.users.stream().anyMatch(user1 -> user1.getEmail().equals(user.getEmail()));
        if (hasEmail) {
            throw new IllegalArgumentException("User with this email already exiting");
        }
        this.users.add(user);

        return Tasks.forResult(true);
    }
}
