package de.hda.fbi.nzse.sose22.safespace.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;

public class Favorites {

    private final Set<UUID> favSet;

    public Favorites(Set<UUID> favSet) {
        this.favSet = favSet;
    }

    public Set<UUID> getFavSet() {
        return favSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Favorites)) return false;

        Favorites favorites = (Favorites) o;

        return new EqualsBuilder().append(favSet, favorites.favSet).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(favSet).toHashCode();
    }

    @Nonnull
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("favSet", favSet)
                .toString();
    }
}
