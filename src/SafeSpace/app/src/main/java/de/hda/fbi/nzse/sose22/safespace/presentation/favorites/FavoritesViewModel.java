package de.hda.fbi.nzse.sose22.safespace.presentation.favorites;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.FavoriteRepositorySharedPrefs;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.BrowseOffers;

@HiltViewModel
public class FavoritesViewModel extends ViewModel {

    private final BrowseOffers browseOffers;
    private final FavoriteRepository favoriteRepository;
    private final MutableLiveData<List<Offer>> offers;


    @Inject
    public FavoritesViewModel(@OfferRepositoryFirestore OfferRepository offerRepository, @FavoriteRepositorySharedPrefs FavoriteRepository favoriteRepository) {
        this.browseOffers = new BrowseOffers(offerRepository);
        this.offers = new MutableLiveData<>();
        this.favoriteRepository = favoriteRepository;

        this.browseOffers.getForFavorite(new ArrayList<>(favoriteRepository.get())).addOnSuccessListener(this.offers::setValue);
    }

    public void updatedFavList() {
        this.browseOffers.getForFavorite(new ArrayList<>(favoriteRepository.get())).addOnSuccessListener(this.offers::postValue);
    }

    public LiveData<List<Offer>> getOffers() {
        return this.offers;
    }

}
