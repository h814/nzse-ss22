package de.hda.fbi.nzse.sose22.safespace.data.repository;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Contact;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserRepository;

public class UserRepositoryFirestoreImpl implements UserRepository {

    @Inject
    public UserRepositoryFirestoreImpl() {
    }

    @Override
    public Task<User> getUserById(UUID id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        var resultsTask = db.collection("users").whereEqualTo("userId", id.toString()).get();

        return resultsTask.continueWith(task -> {
            if (task.isSuccessful() && !task.getResult().isEmpty()) {
                var res = task.getResult().iterator().next();
                //cast is required to get the firebase data and parse it to a list op key, value pairs
                //noinspection unchecked
                var contactsRaw = (List<Map<String, String>>) res.get("contacts");

                if (contactsRaw == null)
                    throw new RuntimeException("Cannot parse contacts field of Firestore database");

                var contactList = contactsRaw.stream().map(entry -> new Contact(Contact.typeFromString(Objects.requireNonNull(entry.get("type"))), entry.get("data"))).collect(Collectors.toList());

                return new User(UUID.fromString((String) res.get("userId")), (String) res.get("firstName"), (String) res.get("lastName"), (String) res.get("email"), contactList);

            } else {
                return null;
            }
        });
    }

    @Override
    public Task<User> getUserById(String otherId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        var resultsTask = db.collection("users").document(otherId).get();

        return resultsTask.continueWith(task -> {
            if (task.isSuccessful()) {
                var res = task.getResult();

                //cast is required to get the firebase data and parse it to a list op key, value pairs
                //noinspection unchecked
                var contactsRaw = (List<Map<String, String>>) res.get("contacts");

                if (contactsRaw == null)
                    throw new RuntimeException("Cannot parse contacts field of Firestore database");

                var contactList = contactsRaw.stream().map(entry -> new Contact(Contact.typeFromString(Objects.requireNonNull(entry.get("type"))), entry.get("data"))).collect(Collectors.toList());

                return new User(UUID.fromString((String) res.get("userId")), (String) res.get("firstName"), (String) res.get("lastName"), (String) res.get("email"), contactList);
            }
            return null;
        });
    }

    @Override
    public Task<Boolean> alterUser(String docName, User user) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> userData = new HashMap<>();
        userData.put("firstName", user.getFirstName());
        userData.put("lastName", user.getLastName());
        userData.put("email", user.getEmail());
        userData.put("contacts", user.getContacts().stream().map(contact -> Map.of("data", contact.get(), "type", contact.getTypeString())).collect(Collectors.toList()));

        return db.collection("users").document(docName).update(userData).continueWith(Task::isSuccessful);
    }

    @Override
    public Task<Boolean> createUser(String docName, User user) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> userData = new HashMap<>();
        userData.put("email", user.getEmail());
        userData.put("firstName", user.getFirstName());
        userData.put("lastName", user.getLastName());
        userData.put("userId", user.getUserId().toString());
        userData.put("firebaseUserId", docName);
        userData.put("contacts", user.getContacts().stream().map(contact -> Map.of("data", contact.get(), "type", contact.getTypeString())).collect(Collectors.toList()));

        return db.collection("users").document(docName).set(userData).continueWith(Task::isSuccessful);
    }
}
