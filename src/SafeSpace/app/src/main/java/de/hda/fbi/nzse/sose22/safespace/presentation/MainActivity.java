package de.hda.fbi.nzse.sose22.safespace.presentation;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.hilt.work.HiltWorker;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.assisted.Assisted;
import dagger.assisted.AssistedInject;
import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.FavoriteRepositorySharedPrefs;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceFirebase;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.FavoriteOperations;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {


    @Inject
    @UserServiceFirebase
    UserService userService;

    @Inject
    @UserRepositoryFirestore
    UserRepository userRepository;

    private String initialLocale;
    private BottomNavigationView bottomNavigationView;
    private NavController navController;

    public static void setThemeInternal(Context context) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        String appearance = sharedPreferences.getString(context.getString(R.string.preference_general_color_scheme_key), "system");
        switch (appearance) {
            case "system":
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                break;
            case "light":
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
            case "dark":
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserViewModel userViewModel = new ViewModelProvider(this).get(UserViewModel.class);

        userViewModel.getUser().observe(this, user -> {
            var bottomMenuItem = this.bottomNavigationView.getMenu().findItem(R.id.main_navigation_profile_graph);
            if (bottomMenuItem == null) return;
            if (user != null) {
                bottomMenuItem.setTitle(R.string.main_navigation_profile);
            } else {
                bottomMenuItem.setTitle(R.string.main_navigation_login);
            }
        });

        this.userService.setUserRepository(this.userRepository);
        initialLocale = LocaleHelper.getPersistedLocale(this);
        setContentView(R.layout.activity_main);

        this.bottomNavigationView = findViewById(R.id.main_navigation_view);
        this.bottomNavigationView.setOnItemSelectedListener(this::onBottomNavItemSelected);


        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.main_nav_host_fragment);
        if (navHostFragment == null) {
            throw new RuntimeException("Cannot find the NavHostFragment");
        }
        this.navController = navHostFragment.getNavController();
        navController.addOnDestinationChangedListener(this::onDestinationChanged);

        NavigationUI.setupWithNavController(this.bottomNavigationView, navController);

        MainActivity.setThemeInternal(this);

        /*
         * Clean favorite list to stop from uncontrolled growing
         * */
        var periodicWorkRequest = new PeriodicWorkRequest.Builder(CleanFavsWorker.class, 3, TimeUnit.HOURS, 5, TimeUnit.MINUTES).build();
        WorkManager.getInstance(getApplicationContext()).enqueue(periodicWorkRequest);
    }

    private boolean onBottomNavItemSelected(MenuItem menuItem) {
        NavController navController = Navigation.findNavController(this, R.id.main_nav_host_fragment);
        return NavigationUI.onNavDestinationSelected(menuItem, navController);
    }

    private void onDestinationChanged(@NonNull NavController controller,
                                      @NonNull NavDestination destination, @Nullable Bundle arguments) {
        var destId = destination.getId();

        Log.d("dest", destination.getDisplayName());

        if (destId == R.id.main_navigation_insert_offer
                || destId == R.id.main_navigation_register
                || destId == R.id.main_navigation_login
                || destId == R.id.main_navigation_settings
                || destId == R.id.main_navigation_offer_detail) {
            this.bottomNavigationView.setVisibility(View.GONE);
        } else {
            this.bottomNavigationView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return this.navController.navigateUp();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.preference_general_color_scheme_key))) {
            MainActivity.setThemeInternal(this);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (initialLocale != null && !initialLocale.equals(LocaleHelper.getPersistedLocale(this))) {
            recreate();
        }
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    @HiltWorker
    public static class CleanFavsWorker extends Worker {
        private final FavoriteRepository favoriteRepository;
        private final OfferRepository offerRepository;

        @AssistedInject
        public CleanFavsWorker(@Assisted @NonNull Context context, @Assisted @NonNull WorkerParameters workerParams, @FavoriteRepositorySharedPrefs FavoriteRepository favoriteRepository, @OfferRepositoryFirestore OfferRepository offerRepository) {
            super(context, workerParams);

            this.favoriteRepository = favoriteRepository;
            this.offerRepository = offerRepository;
        }

        @NonNull
        @Override
        public Result doWork() {
            new FavoriteOperations(favoriteRepository, offerRepository).cleanNotPresentFavs();
            return Result.success();
        }
    }
}