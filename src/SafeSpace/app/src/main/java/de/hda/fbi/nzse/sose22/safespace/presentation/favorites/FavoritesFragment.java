package de.hda.fbi.nzse.sose22.safespace.presentation.favorites;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.OfferListAdapter;

@AndroidEntryPoint
public class FavoritesFragment extends Fragment {

    @Inject
    @OfferRepositoryFirestore
    OfferRepository offerRepository;
    private FavoritesViewModel favoritesViewModel;
    private List<Offer> offers;

    public FavoritesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    // Use notifyDataSetChanged because whole list can change
    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.offers = new ArrayList<>();
        super.onViewCreated(view, savedInstanceState);

        OfferListAdapter offerListAdapter = new OfferListAdapter(offers);
        offerListAdapter.setHasStableIds(true);
        RecyclerView offerList = requireView().findViewById(R.id.favorites_list);
        offerList.setLayoutManager(new LinearLayoutManager(requireContext()));
        offerList.setAdapter(offerListAdapter);


        this.favoritesViewModel = new ViewModelProvider(this).get(FavoritesViewModel.class);
        this.favoritesViewModel.getOffers().observe(getViewLifecycleOwner(), o -> {
            //Handle offer change
            this.offers.clear();
            this.offers.addAll(o);
            offerListAdapter.notifyDataSetChanged();

            //Text
            if (this.offers.isEmpty()) {
                this.setResultNumberText(getString(R.string.favorite_no_favorites));
            } else {
                this.setResultNumberText(getString(R.string.favorites_count_favorites, this.offers.size()));
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        this.favoritesViewModel.updatedFavList();
    }

    private void setResultNumberText(String text) {
        TextView label = requireView().findViewById(R.id.favorites_result_number);
        label.setText(text);
    }
}