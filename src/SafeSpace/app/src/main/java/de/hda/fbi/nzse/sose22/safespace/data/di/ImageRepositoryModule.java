package de.hda.fbi.nzse.sose22.safespace.data.di;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import de.hda.fbi.nzse.sose22.safespace.data.repository.ImageRepositoryFirebaseStorageImpl;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.ImageRepository;

/**
 * Binds the {@link ImageRepository} to concrete implementations for dependency injection.
 * <p>
 * Specifies {@link ImageRepositoryFirebaseStorageImpl}
 */
//Suppressed because only used by Hilt for Di.
//IDE thinks it is unused
@SuppressWarnings("unused")
@Module
@InstallIn(SingletonComponent.class)
public abstract class ImageRepositoryModule {

    @Binds
    @Singleton
    public abstract ImageRepository bindImageRepositoryFirebaseStorageImpl(ImageRepositoryFirebaseStorageImpl imageRepositoryFirebaseStorage);

}
