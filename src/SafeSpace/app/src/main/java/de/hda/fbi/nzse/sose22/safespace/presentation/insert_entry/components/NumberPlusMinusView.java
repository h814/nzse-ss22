package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import de.hda.fbi.nzse.sose22.safespace.R;

public class NumberPlusMinusView extends LinearLayout {

    private boolean isUnsigned;
    private boolean hasMin;
    private int min;
    private boolean hasMax;
    private int max;
    private int value;
    private List<NumberWatcher> listeners;

    private Context context;

    public NumberPlusMinusView(Context context) {
        super(context);
        init(context, null);
    }

    public NumberPlusMinusView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    private void init(@NonNull Context ctx, AttributeSet attributeSet) {
        this.listeners = new ArrayList<>();

        this.context = ctx;
        inflate(ctx, R.layout.number_plus_minus_view, this);

        TypedArray a = ctx.getTheme().obtainStyledAttributes(attributeSet, R.styleable.NumberPlusMinusView, 0, 0);

        int tint;
        try {
            this.setIsEnabled(a.getBoolean(R.styleable.NumberPlusMinusView_enabled, true));
            this.isUnsigned = a.getBoolean(R.styleable.NumberPlusMinusView_unsigned, false);

            this.hasMin = a.hasValue(R.styleable.NumberPlusMinusView_min);
            this.hasMax = a.hasValue(R.styleable.NumberPlusMinusView_max);

            this.min = a.getInt(R.styleable.NumberPlusMinusView_min, 0);
            this.max = a.getInt(R.styleable.NumberPlusMinusView_max, 0);

            this.value = a.getInt(R.styleable.NumberPlusMinusView_value, this.min);

            tint = a.getColor(R.styleable.NumberPlusMinusView_tint, Color.rgb(0, 0, 0));
        } finally {
            a.recycle();
        }

        setOrientation(HORIZONTAL);

        String currentCD = this.getContentDescription().toString();
        findViewById(R.id.minus_btn).setContentDescription(currentCD + " Minus");
        findViewById(R.id.plus_btn).setContentDescription(currentCD + " Plus");
        findViewById(R.id.result).setContentDescription(currentCD + " Result");

        ((ImageButton) findViewById(R.id.minus_btn)).setImageTintList(ColorStateList.valueOf(tint));
        ((ImageButton) findViewById(R.id.plus_btn)).setImageTintList(ColorStateList.valueOf(tint));

        EditText resultView = findViewById(R.id.result);

        findViewById(R.id.plus_btn).setOnClickListener(l -> {

            resultView.clearFocus();
            hideKeyborad();

            if (this.checkGreater(this.value + 1)) {
                ++this.value;
                updateUI();
            }
        });

        findViewById(R.id.minus_btn).setOnClickListener(l -> {

            resultView.clearFocus();
            hideKeyborad();

            if (this.checkSmaller(this.value - 1)) {
                --this.value;
                updateUI();
            }
        });

        var self = this;

        resultView.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                String s = ((EditText) v).getText().toString();
                try {
                    Integer.parseInt(s);
                } catch (NumberFormatException ignored) {
                    resultView.setText(String.valueOf(self.hasMin ? self.min : 0));
                    resultView.setSelection(resultView.getText().toString().length());
                }
            }
        });

        resultView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                resultView.clearFocus();
                hideKeyborad();
            }
            return false;
        });

        resultView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    self.value = Integer.parseInt(s.toString());
                    self.listeners.forEach(l -> l.onNumberChanged(self, self.value));
                } catch (NumberFormatException ignored) {
                }
            }
        });


        setUpMinMaxFilter();
        initUI();
    }

    private void hideKeyborad() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getRootView().getWindowToken(), 0);
    }

    private void setUpMinMaxFilter() {
        EditText result = findViewById(R.id.result);
        var filter = new InputFilter[1];

        if (hasMin && hasMax) {
            filter[0] = new InputFilterMinMax(min, max);
        } else if (hasMin) {
            filter[0] = new InputFilterMinMax(min, Integer.MAX_VALUE);
        } else if (hasMax) {
            filter[0] = new InputFilterMinMax(Integer.MIN_VALUE, max);
        }

        result.setFilters(filter);
    }

    private void initUI() {
        EditText t = findViewById(R.id.result);
        t.setPaintFlags(0);
        t.setText(String.valueOf(this.value));
    }

    private void updateUI() {
        EditText t = findViewById(R.id.result);
        t.setText(String.valueOf(this.value));
    }

    private boolean checkGreater(int i) {
        if (this.hasMax && i <= this.max) {
            return true;
        }

        return !this.hasMax;
    }

    private boolean checkSmaller(int i) {
        if (this.hasMin && i >= this.min) {
            if (!this.isUnsigned) {
                return true;
            }

            if (i >= 0) {
                return true;
            }
        }

        if (!this.hasMin) {
            if (!this.isUnsigned) {
                return true;
            }

            return i >= 0;
        }
        return false;
    }

    public void setValue(int value) {
        this.value = value;
        this.updateUI();
    }

    public void addNumberChangedListener(NumberWatcher watcher) {
        if (this.listeners == null) {
            this.listeners = new ArrayList<>();
        }
        this.listeners.add(watcher);
    }

    public void setIsEnabled(boolean enabled) {

        if (enabled) {
            findViewById(R.id.plus_btn).setVisibility(VISIBLE);
            findViewById(R.id.minus_btn).setVisibility(VISIBLE);
            findViewById(R.id.result).setFocusable(true);
            findViewById(R.id.result).setFocusableInTouchMode(true);
        } else {
            findViewById(R.id.plus_btn).setVisibility(INVISIBLE);
            findViewById(R.id.minus_btn).setVisibility(INVISIBLE);
            findViewById(R.id.result).setFocusable(false);
            findViewById(R.id.result).setFocusableInTouchMode(false);
        }
    }
}