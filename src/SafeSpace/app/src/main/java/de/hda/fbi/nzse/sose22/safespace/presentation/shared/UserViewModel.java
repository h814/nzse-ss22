package de.hda.fbi.nzse.sose22.safespace.presentation.shared;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.Task;

import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceFirebase;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;

@HiltViewModel
public class UserViewModel extends ViewModel implements Observer {

    private final MutableLiveData<User> user;
    private final UserService userService;

    @Inject
    UserViewModel(@UserServiceFirebase UserService userService) {
        this.user = new MutableLiveData<>();
        this.userService = userService;
        this.user.setValue(null);
        userService.addObserver(this);
    }

    public LiveData<User> getUser() {
        return user;
    }

    public Task<Boolean> login(String email, String password) {
        return this.userService.login(email, password);
    }

    public void logout() {
        this.userService.logout();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof User || arg == null) {
            this.user.postValue((User) arg);
        }
    }
}
