package de.hda.fbi.nzse.sose22.safespace.domain.model;

/**
 * Type which kind of offer someone has
 */
public enum OfferType {
    House,
    Flat,
    Room
}
