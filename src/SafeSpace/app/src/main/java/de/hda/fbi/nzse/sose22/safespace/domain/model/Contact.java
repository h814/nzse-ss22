package de.hda.fbi.nzse.sose22.safespace.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Locale;

import javax.annotation.Nonnull;

public class Contact {
    private final ContactType type;
    private String phoneNumber;
    private String wappNumber;
    private String mailAddress;
    private String otherData;

    public Contact(ContactType type, String data) {
        this.type = type;
        this.set(data);
    }

    public static ContactType typeFromString(String type) {
        switch (type.toLowerCase(Locale.ROOT)) {
            case "whatsapp":
                return ContactType.WhatsApp;
            case "mail":
                return ContactType.Mail;
            case "other":
                return ContactType.Other;
            case "phone":
                return ContactType.Phone;
            default:
                throw new IllegalArgumentException("No valid type");
        }
    }

    public void set(String data) {
        switch (this.type) {
            case Mail:
                this.mailAddress = data;
                break;
            case Other:
                this.otherData = data;
                break;
            case Phone:
                this.phoneNumber = data;
                break;
            case WhatsApp:
                this.wappNumber = data;
                break;
            default:
                throw new RuntimeException("No contact type set");
        }
    }

    public String get() {
        switch (this.type) {
            case Mail:
                return this.mailAddress;
            case Other:
                return this.otherData;
            case Phone:
                return this.phoneNumber;
            case WhatsApp:
                return this.wappNumber;
            default:
                throw new RuntimeException("No contact type set");
        }
    }

    public ContactType getType() {
        return this.type;
    }

    public String getTypeString() {
        switch (this.type) {
            case Mail:
                return "Mail";
            case Other:
                return "Other";
            case Phone:
                return "Phone";
            case WhatsApp:
                return "WhatsApp";
        }
        throw new RuntimeException("no valid type set");
    }

    public boolean is(ContactType type) {
        return this.type == type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Contact)) return false;

        Contact contact = (Contact) o;

        return new EqualsBuilder().append(type, contact.type).append(phoneNumber, contact.phoneNumber).append(wappNumber, contact.wappNumber).append(mailAddress, contact.mailAddress).append(otherData, contact.otherData).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(type).append(phoneNumber).append(wappNumber).append(mailAddress).append(otherData).toHashCode();
    }

    @Nonnull
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", type)
                .append("phoneNumber", phoneNumber)
                .append("wappNumber", wappNumber)
                .append("mailAddress", mailAddress)
                .append("otherData", otherData)
                .toString();
    }
}
