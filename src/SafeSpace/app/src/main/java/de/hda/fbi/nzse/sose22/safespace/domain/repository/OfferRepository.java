package de.hda.fbi.nzse.sose22.safespace.domain.repository;

import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.UUID;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;

public interface OfferRepository {
    Task<List<Offer>> get();

    Task<Boolean> createNew(Offer o);

    Task<Boolean> update(UUID offerId, Offer updatedOffer);

    Task<Offer> getById(UUID id);

    Task<Boolean> removeById(UUID id);
}
