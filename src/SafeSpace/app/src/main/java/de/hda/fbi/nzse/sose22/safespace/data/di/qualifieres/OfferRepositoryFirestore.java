package de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres;

import javax.inject.Qualifier;

/**
 * Qualifies OfferRepositoryFirestore as annotation to specify a concrete interface implementation when injecting
 */
@Qualifier
public @interface OfferRepositoryFirestore {
}
