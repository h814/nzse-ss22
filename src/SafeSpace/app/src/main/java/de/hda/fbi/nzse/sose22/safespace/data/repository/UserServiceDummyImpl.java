package de.hda.fbi.nzse.sose22.safespace.data.repository;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.UUID;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;

public class UserServiceDummyImpl extends UserService {

    private final static UUID USER_ID = UUID.fromString("9ff9ea23-c29d-4f96-ab7e-c93db7c94bea");

    @Inject
    public UserServiceDummyImpl() {
        super();
    }

    @Override
    public Task<Boolean> login(String email, String password) {
        return userRepository.getUserById(USER_ID).continueWith(task -> {
            var user = task.getResult();
            super.setCurrentUser(user);
            return user != null;
        });
    }

    @Override
    public Task<Boolean> changePassword(String password, String oldPassword) {
        return Tasks.forResult(true);
    }

    @Override
    public Task<Boolean> changeEmail(String email, String oldPassword) {
        return null;
    }

    @Override
    public Task<Boolean> alterUser(User user) {
        return null;
    }

    @Override
    public Task<Boolean> register(User user, String password) {
        this.userRepository.createUser(null, user);
        return Tasks.forResult(true);

    }

    @Override
    public void logout() {
        this.setCurrentUser(null);
    }
}
