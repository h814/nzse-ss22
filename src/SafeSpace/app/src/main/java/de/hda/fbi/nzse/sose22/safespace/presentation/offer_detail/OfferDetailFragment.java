package de.hda.fbi.nzse.sose22.safespace.presentation.offer_detail;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.Tasks;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.StorageException;

import org.imaginativeworld.whynotimagecarousel.ImageCarousel;
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.FavoriteRepositorySharedPrefs;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferAcceptedGroup;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.ImageRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.InsertEntryFragment;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

@AndroidEntryPoint
public class OfferDetailFragment extends Fragment {

    @Inject
    @FavoriteRepositorySharedPrefs
    FavoriteRepository favoriteRepository;
    @Inject
    @OfferRepositoryFirestore
    OfferRepository offerRepository;
    @Inject
    ImageRepository imageRepository;
    private OfferDetailViewModel offerDetailViewModel;
    private UUID offerId;
    private boolean isFavorite;

    public OfferDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_offer_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        if (getArguments() != null && getArguments().getSerializable(InsertEntryFragment.EDIT_OFFER_ID_KEY) != null) {
            this.offerId = (UUID) getArguments().getSerializable(InsertEntryFragment.EDIT_OFFER_ID_KEY);
        }

        super.onViewCreated(view, savedInstanceState);

        MaterialToolbar toolbar = requireView().findViewById(R.id.offer_detail_toolbar);
        toolbar.setNavigationOnClickListener(view1 -> Navigation.findNavController(requireView()).popBackStack());

        AtomicBoolean deletePending = new AtomicBoolean(false);
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.offer_detail_top_bar_menu_delete) {

                if (deletePending.get()) return true;

                deletePending.set(true);

                requireView().findViewById(R.id.progress_linear).setVisibility(View.VISIBLE);

                String imageUploadPath = "offer_images/" + Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid() + "/" + offerId.toString();
                var deleteTasks = List.of(offerRepository.removeById(this.offerId), imageRepository.deleteImage(imageUploadPath));

                Tasks.whenAllComplete(deleteTasks).addOnCompleteListener(tasks -> {
                    requireView().findViewById(R.id.progress_linear).setVisibility(View.GONE);

                    String msg = getString(R.string.unknown_error);

                    if (tasks.isSuccessful()) {
                        var hasError = false;
                        for (var res : tasks.getResult()) {
                            if (!res.isSuccessful()) hasError = true;
                        }

                        if (!hasError) {
                            Navigation.findNavController(requireView()).popBackStack();
                            return;
                        }
                    } else if (tasks.getException() != null) {
                        msg = tasks.getException().getMessage();
                    }

                    for (var task : tasks.getResult()) {
                        if (task.getException() != null) {

                            if (
                                    task.getException() instanceof StorageException &&
                                            task.getException().getMessage() != null &&
                                            task.getException().getMessage().contains("Object does not exist at location.")) {
                                msg = null;
                                continue;
                            }

                            msg = task.getException().getMessage();
                        }
                    }

                    if (msg == null) {
                        Navigation.findNavController(requireView()).popBackStack();
                        return;
                    }

                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
                    builder
                            .setTitle(R.string.offer_detail_delete_dialog_title)
                            .setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, (dialog, which) -> {
                                dialog.dismiss();
                                deletePending.set(false);
                            }).show();

                });

                return true;

            } else if (item.getItemId() == R.id.offer_detail_top_bar_menu_favorite) {
                this.isFavorite = !this.isFavorite;

                if (isFavorite) {
                    toolbar.getMenu().findItem(R.id.offer_detail_top_bar_menu_favorite).setIcon(R.drawable.ic_favorite_rounded_filled_48);
                    favoriteRepository.add(this.offerId);
                } else {
                    toolbar.getMenu().findItem(R.id.offer_detail_top_bar_menu_favorite).setIcon(R.drawable.ic_outline_favorite_rounded_48);
                    favoriteRepository.remove(this.offerId);
                }

                return true;
            }

            return false;
        });

        this.isFavorite = this.favoriteRepository.is(this.offerId);

        if (isFavorite) {
            toolbar.getMenu().findItem(R.id.offer_detail_top_bar_menu_favorite).setIcon(R.drawable.ic_favorite_rounded_filled_48);
        } else {
            toolbar.getMenu().findItem(R.id.offer_detail_top_bar_menu_favorite).setIcon(R.drawable.ic_outline_favorite_rounded_48);
        }

        this.offerDetailViewModel = new ViewModelProvider(this).get(OfferDetailViewModel.class);

        offerDetailViewModel.getMyUser().observe(getViewLifecycleOwner(), this::setContactInfos);

        offerDetailViewModel.getMyOffer().observe(getViewLifecycleOwner(), offer -> {
            this.setupView(offer);

            var userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);

            userViewModel.getUser().observe(getViewLifecycleOwner(), user -> {

                if (user == null) {
                    //TODO show error
                    return;
                }

                FloatingActionButton floatingActionButton = requireView().findViewById(R.id.offer_detail_edit);
                if (user.getUserId().equals(offer.getInsertedByUserId())) {
                    floatingActionButton.show();
                    toolbar.getMenu().findItem(R.id.offer_detail_top_bar_menu_delete).setVisible(true);
                } else {
                    floatingActionButton.hide();
                    toolbar.getMenu().findItem(R.id.offer_detail_top_bar_menu_delete).setVisible(false);
                }
            });
        });

        requireView().findViewById(R.id.offer_detail_edit).setOnClickListener(v -> {

            if (this.offerId == null) {
                return;
            }

            var dir = OfferDetailFragmentDirections
                    .actionMainNavigationOfferDetailToMainNavigationInsertEntry()
                    .setIsNew(false)
                    .setOFFERID(this.offerId);

            Navigation.findNavController(requireView()).navigate(dir);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        this.offerDetailViewModel.update();
    }

    private Map<String, String> getLangKeyMap() {
        var langKeys = getResources().getStringArray(R.array.insert_entry_p2_available_languages_keys);

        Map<String, String> languages = new HashMap<>();
        Arrays.stream(langKeys).forEach(langKey -> {
            switch (langKey.toLowerCase()) {
                case "de":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_de));
                    break;
                case "en":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_en));
                    break;
                case "uk":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_uk));
                    break;
                case "other":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_other));
                    break;
                default:
                    languages.put(langKeys[langKey.length() - 1], getString(R.string.insert_entry_p2_available_languages_other));
            }
        });

        return languages;
    }

    private void createChip(int chipGroupId, String name, ChipType chipType) {
        ChipGroup chipGroup = requireView().findViewById(chipGroupId);
        Chip chip = new Chip(requireContext());
        chip.setText(name);
        switch (chipType) {
            case Single:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_person_rounded_48, null));
                break;
            case Flat:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_apartment_rounded_48, null));
                break;
            case Family:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_family_rounded_48, null));
                break;
            case Bathroom:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_shower_rounded_48, null));
                break;
            case Kitchen:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_kitchen_rounded_48, null));
                break;
            case House:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_house_rounded_48, null));
                break;
            case Room:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_room_rounded_48, null));
                break;
            case Group:
                chip.setChipIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_group_rounded_48, null));
                break;
        }
        TypedValue value = new TypedValue();
        Resources.Theme theme = requireContext().getTheme();
        theme.resolveAttribute(com.google.android.material.R.attr.colorOnSurfaceVariant, value, true);
        @ColorInt int color = value.data;
        //TODO set chip color
        chip.setChipIconTint(ColorStateList.valueOf(color));
        chipGroup.addView(chip);
    }

    private void createTextView(int textViewId, String content) {
        TextView textView = requireView().findViewById(textViewId);
        textView.setText(content);
    }

    private void setupView(Offer offer) {

        ((ChipGroup) requireView().findViewById(R.id.offer_detail_chipGroup_properties)).removeAllViews();
        ((ChipGroup) requireView().findViewById(R.id.offer_detail_chipGroup_languages)).removeAllViews();
        ((ChipGroup) requireView().findViewById(R.id.offer_detail_chipGroup_tags)).removeAllViews();

        switch (offer.getType()) {
            case Flat:
                createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_apartment), ChipType.Flat);
                break;
            case Room:
                createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_room), ChipType.Room);
                break;
            case House:
                createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_house), ChipType.House);
                break;
        }

        offer.getAcceptedGroup().forEach(offerAcceptedGroup -> {
                    switch (offerAcceptedGroup) {
                        case Group:
                            createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_group), ChipType.Group);
                            break;
                        case Family:
                            createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_family), ChipType.Family);
                            break;
                        case Single:
                            createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_single), ChipType.Single);
                            break;
                    }
                }
        );

        if (offer.getAcceptedGroup().contains(OfferAcceptedGroup.Group) || offer.getAcceptedGroup().contains(OfferAcceptedGroup.Family)) {
            createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_person_cnt, offer.getMaxAcceptedPersons()), ChipType.Unset);
        }

        if (offer.isHasBath())
            createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_bathroom), ChipType.Bathroom);
        if (offer.isHasKitchen())
            createChip(R.id.offer_detail_chipGroup_properties, getString(R.string.offer_detail_kitchen), ChipType.Kitchen);

        var languages = getLangKeyMap();
        offer.getYourLanguages().forEach(language -> createChip(R.id.offer_detail_chipGroup_languages, getLangKeyMap().getOrDefault(language, new ArrayList<>(languages.values()).get(languages.size() - 1)), ChipType.Unset));

        createTextView(R.id.offer_detail_description, offer.getDescription());

        offer.getTags().forEach(tag -> createChip(R.id.offer_detail_chipGroup_tags, tag, ChipType.Unset));

        //Show contact info
        //******

        //address
        var address = offer.getAddress();
        String addressString;
        if (offer.isShowFullAddress()) {
            addressString = getString(R.string.offer_detail_full_address,
                    address.getStreet(),
                    address.getNumber(),
                    Long.toString(address.getPostalCode()),
                    address.getTown());
        } else {
            addressString = getString(R.string.offer_detail_only_town, address.getTown());
        }
        createTextView(R.id.offer_detail_address, addressString);
        createCarousel(offer);

    }

    private void setContactInfos(User user) {
        //contact types
        //contact types
        for (int i = 0; i < user.getContacts().size(); i++) {
            String contactType = user.getContacts().get(i).getTypeString();

            switch (contactType) {
                case "WhatsApp":
                    createTextView(R.id.offer_detail_whatsapp, (contactType + " " + user.getContacts().get(i).get()));
                    break;
                case "Mail":
                    createTextView(R.id.offer_detail_email, (contactType + " " + user.getContacts().get(i).get()));
                    break;
                case "Other":
                    createTextView(R.id.offer_detail_other, (contactType + " " + user.getContacts().get(i).get()));
                    break;
                case "Phone":
                    createTextView(R.id.offer_detail_phone, (contactType + " " + user.getContacts().get(i).get()));
                    break;
                default:
                    throw new IllegalArgumentException("No valid type");
            }
        }
    }

    private void createCarousel(Offer offer) {
        ImageCarousel carousel = requireView().findViewById(R.id.offer_detail_carousel);
        carousel.registerLifecycle(getViewLifecycleOwner());

        List<CarouselItem> list = new ArrayList<>();

        offer.getImageURIs().forEach(image -> list.add(
                new CarouselItem(image.toString())
        ));

        carousel.setData(list);
    }

    private enum ChipType {
        Single,
        Flat,
        Family,
        Bathroom,
        Kitchen,
        House,
        Room,
        Group,
        Unset
    }
}