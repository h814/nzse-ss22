package de.hda.fbi.nzse.sose22.safespace.presentation.search_list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.BrowseOffers;

@HiltViewModel
public class SearchListViewModel extends ViewModel {

    private final BrowseOffers browseOffers;
    private final MutableLiveData<List<Offer>> offers;
    private final SavedStateHandle savedStateHandle;

    @Inject
    public SearchListViewModel(@OfferRepositoryFirestore OfferRepository offerRepository, SavedStateHandle savedStateHandle) {
        this.browseOffers = new BrowseOffers(offerRepository);
        this.offers = new MutableLiveData<>();
        this.savedStateHandle = savedStateHandle;
        update();
    }

    public LiveData<List<Offer>> getOffers() {
        return this.offers;
    }

    //Updates browseOffers based on town
    public void update() {
        this.browseOffers.getForTown(SearchListFragmentArgs.fromSavedStateHandle(this.savedStateHandle).getTown()).addOnSuccessListener(this.offers::postValue);
    }

}
