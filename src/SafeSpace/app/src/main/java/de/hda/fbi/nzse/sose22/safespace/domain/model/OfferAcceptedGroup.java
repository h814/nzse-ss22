package de.hda.fbi.nzse.sose22.safespace.domain.model;

/**
 * Types which person groups a person can host in his offer
 */
public enum OfferAcceptedGroup {
    Single,
    Family,
    Group,
}
