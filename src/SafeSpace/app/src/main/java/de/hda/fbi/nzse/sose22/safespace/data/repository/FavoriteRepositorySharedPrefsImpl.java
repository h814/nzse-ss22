package de.hda.fbi.nzse.sose22.safespace.data.repository;

import android.content.Context;

import androidx.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Favorites;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;

public class FavoriteRepositorySharedPrefsImpl implements FavoriteRepository {

    public static final String KEY = "safespace_favorites";

    private final Context context;

    @Inject
    public FavoriteRepositorySharedPrefsImpl(@ApplicationContext Context context) {
        this.context = context;
    }

    private void save(Set<UUID> data) {
        var prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        String dataJson = new Gson().toJson(new Favorites(data));
        prefs.putString(KEY, dataJson);
        prefs.apply();
    }

    @Override
    public Set<UUID> get() {
        var prefs = PreferenceManager.getDefaultSharedPreferences(context);
        var data = prefs.getString(KEY, "");

        if (data.isEmpty()) return Stream.<UUID>of().collect(Collectors.toSet());

        Favorites ids = new Gson().fromJson(data, Favorites.class);
        return ids.getFavSet();
    }

    @Override
    public void add(UUID id) {
        var data = get();
        data.add(id);
        save(data);
    }

    @Override
    public void remove(UUID id) {
        var data = get();
        data.remove(id);
        save(data);
    }

    @Override
    public boolean is(UUID id) {
        return get().contains(id);
    }

}
