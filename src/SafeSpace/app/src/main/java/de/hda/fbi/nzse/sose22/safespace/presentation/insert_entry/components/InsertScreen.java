package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;

public interface InsertScreen {

    /**
     * Method is used to fill the View with data of an existing offer
     *
     * @param o The offer from which the data is extracted
     */
    // Is indeed used but not as "interface" as this
    // but more to show that the method is missing if not
    // implemented in implementing classes
    @SuppressWarnings("unused")
    void populateScreenData(Offer o);

    /***
     * Checks if the values of a screen are valid
     *
     * @return true if screen is valid false otherwise
     */
    boolean isValid();
}
