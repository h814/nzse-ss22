package de.hda.fbi.nzse.sose22.safespace.domain.repository;

import java.util.Set;
import java.util.UUID;

public interface FavoriteRepository {
    Set<UUID> get();

    void add(UUID id);

    void remove(UUID id);

    boolean is(UUID id);
}
