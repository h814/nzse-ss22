package de.hda.fbi.nzse.sose22.safespace.data.di;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserRepositoryDummy;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.repository.UserRepositoryDummyImpl;
import de.hda.fbi.nzse.sose22.safespace.data.repository.UserRepositoryFirestoreImpl;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserRepository;

/**
 * Binds the {@link UserRepository} to concrete implementations for dependency injection.
 * <p>
 * Specifies {@link UserRepositoryDummyImpl} as {@link UserRepositoryDummy} and
 * {@link UserRepositoryFirestoreImpl} as {@link UserRepositoryFirestore}
 */
//Suppressed because only used by Hilt for Di.
//IDE thinks it is unused
@SuppressWarnings("unused")
@Module
@InstallIn(SingletonComponent.class)
public abstract class UserRepositoryModule {

    @Binds
    @Singleton
    @UserRepositoryDummy
    public abstract UserRepository bindUserRepositoryDummyImpl(
            UserRepositoryDummyImpl userRepositoryDummy
    );

    @Binds
    @Singleton
    @UserRepositoryFirestore
    public abstract UserRepository bindUserRepositoryFirestore(
            UserRepositoryFirestoreImpl userRepositoryFirestore
    );
}