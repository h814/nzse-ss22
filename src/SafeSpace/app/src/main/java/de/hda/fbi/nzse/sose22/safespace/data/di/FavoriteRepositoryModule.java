package de.hda.fbi.nzse.sose22.safespace.data.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.FavoriteRepositoryDummy;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.FavoriteRepositorySharedPrefs;
import de.hda.fbi.nzse.sose22.safespace.data.repository.FavoriteRepositoryDummyImpl;
import de.hda.fbi.nzse.sose22.safespace.data.repository.FavoriteRepositorySharedPrefsImpl;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;

/**
 * Binds the {@link FavoriteRepository} to concrete implementations for dependency injection.
 * <p>
 * Specifies {@link FavoriteRepositoryDummyImpl} as {@link FavoriteRepositoryDummy} and
 * {@link FavoriteRepositorySharedPrefsImpl} as {@link FavoriteRepositorySharedPrefs}
 */
//Suppressed because only used by Hilt for Di.
//IDE thinks it is unused
@SuppressWarnings("unused")
@Module
@InstallIn(SingletonComponent.class)
public abstract class FavoriteRepositoryModule {

    @Provides
    @Singleton
    @FavoriteRepositorySharedPrefs
    public static FavoriteRepository bindFavoriteRepositorySharedPrefsImpl(
            @ApplicationContext Context context
    ) {
        return new FavoriteRepositorySharedPrefsImpl(context);
    }

    @Binds
    @Singleton
    @FavoriteRepositoryDummy
    public abstract FavoriteRepository bindFavoriteRepositoryDummyImpl(
            FavoriteRepositoryDummyImpl favoriteRepositoryDummy
    );
}