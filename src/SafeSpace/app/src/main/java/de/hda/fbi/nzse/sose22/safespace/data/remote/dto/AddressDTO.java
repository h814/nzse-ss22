package de.hda.fbi.nzse.sose22.safespace.data.remote.dto;

import java.util.Map;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Address;

public class AddressDTO {
    private String street;
    private String number;
    private long postalCode;
    private String town;

    public AddressDTO() {

    }

    /***
     * Creates an OfferDTO from a {@link Map}.
     * Is used for have a single point of failure for parsing the data.
     *
     * Suppressing the casts because the map contains raw types and they must be handled with type casts.
     * Exceptions will be caught and result in a {@link MalformedAddressDTOException}.
     *
     * @param data A map with the values for the DTO
     * @return A dto created from the map
     * */
    @SuppressWarnings({"ConstantConditions"})
    public static AddressDTO fromMap(Map<String, Object> data) {
        AddressDTO dto = new AddressDTO();

        try {
            dto.street = (String) data.get("street");
            dto.number = (String) data.get("number");
            dto.town = (String) data.get("town");
            dto.postalCode = (long) data.get("postalCode");

            return dto;
        } catch (Exception e) {
            throw new MalformedAddressDTOException(e.getMessage());
        }
    }

    /***
     * Transforms a DTO to an instance of the Domain section
     *
     * @return An {@link Address} Object created of the DTO data
     */
    public Address toAddress() {
        return new Address(street, number, postalCode, town);
    }
}
