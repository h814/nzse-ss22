package de.hda.fbi.nzse.sose22.safespace.presentation.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceFirebase;
import de.hda.fbi.nzse.sose22.safespace.domain.model.User;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;

@HiltViewModel
public class RegisterViewModel extends ViewModel {

    private final MutableLiveData<User> user;
    private final UserService userService;
    private final User initialUser;
    private boolean isProfileChange = false;

    @Inject
    public RegisterViewModel(@UserServiceFirebase UserService userService, SavedStateHandle savedStateHandle) {

        this.userService = userService;

        this.user = new MutableLiveData<>();

        if (savedStateHandle.<Boolean>get(RegisterFragment.IS_CHANGE_PROFILE_PARAM) != null &&
                Boolean.TRUE.equals(savedStateHandle.<Boolean>get(RegisterFragment.IS_CHANGE_PROFILE_PARAM))) {
            this.isProfileChange = true;
            this.user.setValue(userService.getUser());
            this.initialUser = new User(userService.getUser());
        } else {
            User user = new User(UUID.randomUUID(), "", "", "");
            this.user.setValue(user);
            this.initialUser = user;
        }

    }

    //Updates the Users
    public void updateUser(User u) {
        this.user.postValue(u);
    }

    //Returns the User
    public LiveData<User> getUser() {
        return this.user;
    }

    /**
     * This method registers a user with Firebase
     *
     * @param password password of the user as string
     * @return Firebase Task<Boolean>
     */
    public Task<Boolean> register(String password) {
        var user = this.user.getValue();

        if (user == null) throw new IllegalStateException("User cannot be null");

        if (isProfileChange) {
            var currentTask = this.userService.alterUser(this.user.getValue());

            if (!user.getEmail().equals(this.initialUser.getEmail())) {
                return Tasks.forException(new IllegalArgumentException("User email not matching password"));
            }

            if (!password.isEmpty()) {
                return Tasks.forException(new IllegalArgumentException("Expected also old password"));
            }

            return currentTask.continueWith(task -> {
                if (!task.isSuccessful()) {
                    if (task.getException() != null) throw task.getException();
                    throw new RuntimeException("Failed to alter user");
                }

                return task.getResult();
            });
        } else {
            return this.userService.register(user, password);
        }
    }

    /**
     * This method registers a user with Firebase
     *
     * @param password    new password of the user
     * @param oldPassword old password of the user
     * @return Firebase Task<Boolean>
     */
    public Task<Boolean> register(String password, String oldPassword) {

        var user = this.user.getValue();
        if (user == null) throw new IllegalStateException("User cannot be null");

        var errorIndex = new AtomicInteger(0);
        final String[] errorMSGes = {"Failed to alter user", "Failed to change email", "Failed to change password"};

        if (isProfileChange) {
            var currentTask = this.userService.alterUser(user);

            if (!user.getEmail().equals(this.initialUser.getEmail())) {
                currentTask = currentTask.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        if (task.getException() != null) throw task.getException();
                        throw new RuntimeException(errorMSGes[errorIndex.getAndSet(1)]);
                    }

                    return this.userService.changeEmail(user.getEmail(), oldPassword);
                });
            }

            if (!password.isEmpty()) {
                currentTask.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        if (task.getException() != null) throw task.getException();
                        throw new RuntimeException(errorMSGes[errorIndex.getAndSet(2)]);
                    }

                    return this.userService.changePassword(password, oldPassword);
                });
            }

            return currentTask.continueWith(task -> {
                if (!task.isSuccessful()) {
                    if (task.getException() != null) throw task.getException();
                    throw new RuntimeException(errorMSGes[errorIndex.get()]);
                }

                return task.getResult();
            });
        }

        throw new IllegalArgumentException("If no profile is changed call register with one argument");
    }

}
