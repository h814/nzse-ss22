package de.hda.fbi.nzse.sose22.safespace.data.remote.dto;

import com.google.firebase.firestore.DocumentSnapshot;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Address;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferAcceptedGroup;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferType;

public class OfferDTO {
    private final List<OfferAcceptedGroup> acceptedGroup;
    private final UUID id;
    private final UUID insertedByUserId;
    private final OfferType type;
    private final int roomCount;
    private final boolean hasBath;
    private final boolean hasKitchen;
    private final int maxAcceptedPersons;
    private final List<String> yourLanguages;
    private final Address address;
    private final boolean showFullAddress;
    private final String description;
    private final List<String> tags;
    private final List<URI> imageURIs;
    private final int showcaseImageIndex;

    public OfferDTO(UUID id, UUID insertedByUserId, OfferType type, int roomCount, boolean hasBath, boolean hasKitchen, List<OfferAcceptedGroup> acceptedGroup, int maxAcceptedPersons, List<String> yourLanguages, Address address, boolean showFullAddress, String description, List<String> tags, List<URI> imageURIs, int showcaseImageIndex) {
        this.acceptedGroup = acceptedGroup;
        this.id = id;
        this.insertedByUserId = insertedByUserId;
        this.type = type;
        this.roomCount = roomCount;
        this.hasBath = hasBath;
        this.hasKitchen = hasKitchen;
        this.maxAcceptedPersons = maxAcceptedPersons;
        this.yourLanguages = yourLanguages;
        this.address = address;
        this.showFullAddress = showFullAddress;
        this.description = description;
        this.tags = tags;
        this.imageURIs = imageURIs;
        this.showcaseImageIndex = showcaseImageIndex;
    }

    /***
     * Creates an OfferDTO from a Firebase {@link DocumentSnapshot}.
     * Is used for have a single point of failure for parsing the data.
     *
     * Suppressing the casts because firebase toObject not parses correctly and cannot use e.g. Sets
     * it is required to cast every single part of the returned map of firebase to its expected type
     * exceptions will be caught and result in a {@link MalformedOfferDTOException} or {@link MalformedAddressDTOException}.
     *
     * @param snapshot The fetched Firebase data
     * @return A dto created from Firebase data
     * */
    @SuppressWarnings("unchecked")
    public static OfferDTO fromFirebase(DocumentSnapshot snapshot) {
        try {
            List<OfferAcceptedGroup> acceptedGroup = ((List<String>) Objects.requireNonNull(snapshot.get("acceptedGroup"))).stream().map(OfferAcceptedGroup::valueOf).collect(Collectors.toList());
            UUID id = UUID.fromString(snapshot.getString("id"));
            UUID insertedByUserId = UUID.fromString(snapshot.getString("insertedByUserId"));
            OfferType type = OfferType.valueOf(snapshot.getString("type"));
            int roomCount = Objects.requireNonNull(snapshot.getLong("roomCount")).intValue();
            boolean hasBath = Boolean.TRUE.equals(snapshot.getBoolean("hasBath"));
            boolean hasKitchen = Boolean.TRUE.equals(snapshot.getBoolean("hasKitchen"));
            int maxAcceptedPersons = Objects.requireNonNull(snapshot.getLong("maxAcceptedPersons")).intValue();
            List<String> yourLanguages = (List<String>) snapshot.get("yourLanguages");
            boolean showFullAddress = Boolean.TRUE.equals(snapshot.getBoolean("showFullAddress"));
            String description = snapshot.getString("description");
            List<String> tags = (List<String>) snapshot.get("tags");
            List<URI> imageURIs = ((List<String>) Objects.requireNonNull(snapshot.get("imageURIs"))).stream().map(URI::create).collect(Collectors.toList());
            int showcaseImageIndex = Objects.requireNonNull(snapshot.getLong("showcaseImageIndex")).intValue();

            AddressDTO addressDTO = AddressDTO.fromMap((Map<String, Object>) snapshot.get("address"));
            Address address = addressDTO.toAddress();

            return new OfferDTO(
                    id,
                    insertedByUserId,
                    type,
                    roomCount,
                    hasBath,
                    hasKitchen,
                    acceptedGroup,
                    maxAcceptedPersons,
                    yourLanguages,
                    address,
                    showFullAddress,
                    description,
                    tags,
                    imageURIs,
                    showcaseImageIndex
            );
        } catch (MalformedAddressDTOException e) {
            throw e;
        } catch (Exception e) {
            throw new MalformedOfferDTOException(e.getMessage());
        }
    }

    /***
     * Transforms an {@link Offer}-Model into a DTO
     *
     * @param offer An {@link Offer}-Model to transform into a DTO
     * @return An {@link OfferDTO} from the {@link Offer}-Model data
     */
    public static OfferDTO fromOffer(Offer offer) {
        return new OfferDTO(offer.getId(), offer.getInsertedByUserId(), offer.getType(), offer.getRoomCount(), offer.isHasBath(), offer.isHasKitchen(), new ArrayList<>(offer.getAcceptedGroup()), offer.getMaxAcceptedPersons(), offer.getYourLanguages(), offer.getAddress(), offer.isShowFullAddress(), offer.getDescription(), offer.getTags(), offer.getImageURIs(), offer.getShowcaseImageIndex());
    }

    public Map<String, Object> toFirebaseMap() {
        Map<String, Object> offerMap = new HashMap<>();

        offerMap.put("id", this.id.toString());
        offerMap.put("insertedByUserId", this.insertedByUserId.toString());
        offerMap.put("hasBath", this.hasBath);
        offerMap.put("hasKitchen", this.hasKitchen);
        offerMap.put("maxAcceptedPersons", this.maxAcceptedPersons);
        offerMap.put("yourLanguages", this.yourLanguages);
        offerMap.put("address", this.address);
        offerMap.put("showFullAddress", this.showFullAddress);
        offerMap.put("description", this.description);
        offerMap.put("tags", this.tags);
        offerMap.put("imageURIs", this.imageURIs);
        offerMap.put("showcaseImageIndex", this.showcaseImageIndex);
        offerMap.put("roomCount", this.roomCount);
        offerMap.put("type", this.type);
        offerMap.put("acceptedGroup", new ArrayList<>(this.acceptedGroup));

        return offerMap;
    }

    /***
     * Transforms a DTO to an instance of the Domain section
     *
     * @return An {@link Offer} Object created of the DTO data
     */
    public Offer toOffer() {
        return new Offer(
                id, insertedByUserId, type, roomCount, hasBath, hasKitchen, new HashSet<>(acceptedGroup), maxAcceptedPersons, yourLanguages, address, showFullAddress, description, tags, imageURIs, showcaseImageIndex
        );
    }
}