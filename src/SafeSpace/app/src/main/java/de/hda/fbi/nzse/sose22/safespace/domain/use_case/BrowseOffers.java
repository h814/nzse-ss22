package de.hda.fbi.nzse.sose22.safespace.domain.use_case;

import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;

public class BrowseOffers {
    private final OfferRepository offerRepository;

    public BrowseOffers(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    public Task<List<Offer>> getForUser(UUID uuid) {
        return offerRepository.get().continueWith(task -> {
            if (!task.isSuccessful())
                throw new OfferNotFoundException("Failed to fetch offers for user by user id: " + uuid.toString());

            return task.getResult().stream().filter(o -> o.getInsertedByUserId().equals(uuid)).collect(Collectors.toList());
        });
    }

    public Task<List<Offer>> getForTown(String town) {
        return offerRepository.get().continueWith(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) throw task.getException();
                throw new OfferNotFoundException("Failed to fetch offers for town: " + town);
            }
            return task.getResult().stream().filter(o -> o.getAddress().getTown().toLowerCase().contains(town.trim().toLowerCase())).collect(Collectors.toList());
        });
    }

    public Task<List<Offer>> getForFavorite(List<UUID> favoriteList) {
        return offerRepository.get().continueWith(task -> {
            if (!task.isSuccessful())
                throw new OfferNotFoundException("Failed to fetch offers for favorites: " + favoriteList.stream().map(UUID::toString).collect(Collectors.joining(",")));

            return task.getResult().stream().filter(o -> favoriteList.contains(o.getId())).collect(Collectors.toList());
        });
    }

    public Task<Offer> getByOfferId(UUID id) {
        return offerRepository.getById(id).continueWith(task -> {
            if (!task.isSuccessful())
                throw new OfferNotFoundException("Failed to fetch offer by id: " + id.toString());

            return task.getResult();
        });
    }
}
