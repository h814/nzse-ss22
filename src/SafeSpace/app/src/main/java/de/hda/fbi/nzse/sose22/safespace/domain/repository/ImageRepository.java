package de.hda.fbi.nzse.sose22.safespace.domain.repository;

import com.google.android.gms.tasks.Task;

import java.io.InputStream;
import java.net.URI;

public interface ImageRepository {

    Task<URI> uploadImage(String path, InputStream imageStream);

    Task<Boolean> deleteImage(String path);

}
