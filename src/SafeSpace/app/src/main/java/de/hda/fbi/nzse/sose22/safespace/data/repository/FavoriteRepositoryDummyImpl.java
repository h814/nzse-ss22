package de.hda.fbi.nzse.sose22.safespace.data.repository;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.repository.FavoriteRepository;

public class FavoriteRepositoryDummyImpl implements FavoriteRepository {

    final Set<UUID> favs;

    @Inject
    public FavoriteRepositoryDummyImpl() {
        this.favs = Stream.of(
                UUID.fromString("4c4a1f0f-113f-44b1-a5a9-f2d2f08ab8eb"),
                UUID.fromString("0400d036-0f57-428e-9501-1d9c213bda0e")
        ).collect(Collectors.toSet());
    }

    @Override
    public Set<UUID> get() {
        return this.favs;
    }

    @Override
    public void add(UUID id) {
        this.favs.add(id);
    }

    @Override
    public void remove(UUID id) {
        this.favs.remove(id);
    }

    @Override
    public boolean is(UUID id) {
        return favs.contains(id);
    }

}
