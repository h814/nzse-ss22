package de.hda.fbi.nzse.sose22.safespace.data.remote.dto;

public class MalformedOfferDTOException extends RuntimeException {
    public MalformedOfferDTOException(String message) {
        super(message);
    }
}