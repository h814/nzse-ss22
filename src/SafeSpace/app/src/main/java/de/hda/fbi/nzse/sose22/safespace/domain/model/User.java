package de.hda.fbi.nzse.sose22.safespace.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

/**
 * This is the model for a registered user (someone who inserts offers).
 */
public class User {
    private final UUID userId;
    private List<Contact> contacts;
    private String firstName;
    private String lastName;
    private String email;

    public User(User oldUser) {
        this.userId = oldUser.userId;
        this.contacts = new ArrayList<>(oldUser.contacts);
        this.firstName = oldUser.firstName;
        this.lastName = oldUser.lastName;
        this.email = oldUser.email;

    }

    public User(UUID id, String firstName, String lastName, String email) {
        this(id, firstName, lastName, email, null);
    }

    public User(UUID id, String firstName, String lastName, String email, List<Contact> contacts) {
        this.userId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.contacts = new ArrayList<>();

        if (contacts != null) {
            this.contacts.addAll(contacts);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void changeContact(ContactType type, String data) {

        if (data.equals("")) return;

        var hasType = this.contacts.stream().anyMatch(contact -> contact.is(type));

        if (!hasType) this.contacts.add(new Contact(type, ""));

        this.contacts = this.contacts.stream().peek(contact -> {
            if (contact.is(type)) {
                contact.set(data);
            }
        }).collect(Collectors.toList());
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    public void removeContact(ContactType contact) {
        for (int i = 0; i < this.contacts.size(); ++i) {
            if (this.contacts.get(i).is(contact)) {
                this.contacts.remove(i);
                return;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof User)) return false;

        User user = (User) o;

        return new EqualsBuilder().append(userId, user.userId).append(contacts, user.contacts).append(firstName, user.firstName).append(lastName, user.lastName).append(email, user.email).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(userId).append(contacts).append(firstName).append(lastName).append(email).toHashCode();
    }

    @Nonnull
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("userId", userId)
                .append("contacts", contacts)
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("email", email)
                .toString();
    }

    public UUID getUserId() {
        return userId;
    }
}
