package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;

public class InsertEntryP2Fragment extends Fragment implements InsertScreen {

    private final AtomicBoolean isInited;
    private List<String> selectedLanguages;
    private List<String> tags;
    private InsertOfferViewModel insertOfferViewModel;

    public InsertEntryP2Fragment() {
        this.selectedLanguages = new ArrayList<>();
        this.tags = new ArrayList<>();
        isInited = new AtomicBoolean(false);
    }

    public static InsertEntryP2Fragment newInstance() {
        InsertEntryP2Fragment fragment = new InsertEntryP2Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_insert_entry_p2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.insertOfferViewModel = new ViewModelProvider(requireParentFragment()).get(InsertOfferViewModel.class);

        this.populateLanguageSpinner();
        this.setupTagsList();

        TextInputEditText descriptionField = requireView().findViewById(R.id.insert_entry_p2_s3_description);
        descriptionField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                TextInputLayout descriptionLayout = requireView().findViewById(R.id.insert_entry_p2_s3_description_layout);
                descriptionLayout.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                var o = insertOfferViewModel.getOffer().getValue();
                if (o != null) {
                    o.setDescription(s.toString());
                    insertOfferViewModel.updateOffer(o);
                }
            }
        });

        this.insertOfferViewModel.getOffer().observe(getViewLifecycleOwner(), offer -> {
            if (this.isInited.get()) return;
            if (offer != null) {
                this.populateScreenData(offer);
                this.isInited.set(true);
            }
        });
    }

    private void setupTagsList() {
        ChipGroup chipGroup = requireView().findViewById(R.id.insert_entry_p2_s2_tag_list_chips);

        EditText tagInput = requireView().findViewById(R.id.insert_entry_p2_s2_tag_input);

        tagInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                TextInputLayout tagLayout = requireView().findViewById(R.id.insert_entry_p2_s2_tag_input_layout);
                tagLayout.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tagInput.setOnEditorActionListener((view, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                var tag = view.getText().toString();
                view.setText("");

                if (!this.tags.contains(tag) && !tag.isEmpty()) {
                    this.tags.add(tag);
                    createChip(chipGroup, tag, null, getString(R.string.insert_entry_p2_s1_snackbar_undo_text, tag), this.tags);
                    insertOfferViewModel.updateTags(this.tags);
                }
                return true;
            }
            return true;
        });
    }

    private void createChip(ChipGroup chipGroup, String text, String textAlt, String snackText, List<String> dataList) {
        Chip chip = new Chip(requireContext());
        chip.setText(text);

        if (textAlt == null) textAlt = text;

        Log.d("CreateChip", "createChip: " + text + " " + textAlt + " " + snackText);

        chip.setCloseIconVisible(true);
        final String finalTextAlt = textAlt;
        chip.setOnCloseIconClickListener(v -> {
            dataList.remove(finalTextAlt);
            chipGroup.removeView(chip);
            insertOfferViewModel.updateTags(this.tags);

            Snackbar undo = Snackbar.make(chipGroup, snackText, Snackbar.LENGTH_SHORT);
            undo.setAction(R.string.insert_entry_p2_s1_snackbar_undo_action, ignore -> {
                chipGroup.addView(chip);
                dataList.add(finalTextAlt);
                insertOfferViewModel.updateTags(this.tags);
            });
            undo.show();
        });
        chipGroup.addView(chip);
    }

    private Map<String, String> getLangMap() {
        var langKeys = getResources().getStringArray(R.array.insert_entry_p2_available_languages_keys);

        Map<String, String> languages = new HashMap<>();
        Arrays.stream(langKeys).forEach(langKey -> {
            switch (langKey.toLowerCase()) {
                case "de":
                    languages.put(getString(R.string.insert_entry_p2_available_languages_de), langKey);
                    break;
                case "en":
                    languages.put(getString(R.string.insert_entry_p2_available_languages_en), langKey);
                    break;
                case "uk":
                    languages.put(getString(R.string.insert_entry_p2_available_languages_uk), langKey);
                    break;
                case "other":
                    languages.put(getString(R.string.insert_entry_p2_available_languages_other), langKey);
                    break;
            }
        });

        return languages;
    }

    private Map<String, String> getLangKeyMap() {
        var langKeys = getResources().getStringArray(R.array.insert_entry_p2_available_languages_keys);

        Map<String, String> languages = new HashMap<>();
        Arrays.stream(langKeys).forEach(langKey -> {
            switch (langKey.toLowerCase()) {
                case "de":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_de));
                    break;
                case "en":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_en));
                    break;
                case "uk":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_uk));
                    break;
                case "other":
                    languages.put(langKey, getString(R.string.insert_entry_p2_available_languages_other));
                    break;
            }
        });

        return languages;
    }

    private void populateLanguageSpinner() {

        var languages = getLangMap();

        AutoCompleteTextView s = requireView().findViewById(R.id.insert_entry_p2_s1_language_spinner);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(requireContext(), R.layout.simple_dropdown_item);
        adapter.addAll(languages.keySet());
        s.setAdapter(adapter);

        ChipGroup chipGroup = requireView().findViewById(R.id.insert_entry_p2_s2_language_list_chips);
        s.setOnItemClickListener((parent, view, position, id) -> {

            TextInputLayout spinnerLayout = requireView().findViewById(R.id.insert_entry_p2_s1_language_spinner_layout);
            spinnerLayout.setError(null);

            String language = (String) parent.getItemAtPosition(position);
            s.setText("");
            s.clearFocus();
            var langKey = languages.getOrDefault(language, new ArrayList<>(languages.values()).get(languages.size() - 1));
            if (this.selectedLanguages.contains(langKey)) return;

            this.selectedLanguages.add(langKey);
            createChip(chipGroup, language, langKey, getString(R.string.insert_entry_p2_s1_snackbar_undo_text, language), this.selectedLanguages);
            this.insertOfferViewModel.updateLanguages(this.selectedLanguages);
        });
    }

    @Override
    public void populateScreenData(Offer o) {
        ChipGroup chipGroupLang = requireView().findViewById(R.id.insert_entry_p2_s2_language_list_chips);
        this.selectedLanguages = o.getYourLanguages();
        var languages = getLangKeyMap();
        this.selectedLanguages.forEach(key -> {
            var language = languages.getOrDefault(key, new ArrayList<>(languages.values()).get(languages.size() - 1));
            createChip(chipGroupLang, language, key, getString(R.string.insert_entry_p2_s1_snackbar_undo_text, language), this.selectedLanguages);
        });

        ChipGroup chipGroupTags = requireView().findViewById(R.id.insert_entry_p2_s2_tag_list_chips);
        this.tags = o.getTags();
        this.tags.forEach(tag -> createChip(chipGroupTags, tag, null, getString(R.string.insert_entry_p2_s1_snackbar_undo_text, tag), this.tags));
        TextInputEditText descriptionField = requireView().findViewById(R.id.insert_entry_p2_s3_description);
        descriptionField.setText(o.getDescription());
    }

    @Override
    public void onResume() {
        super.onResume();
        ChipGroup chipGroupLang = requireView().findViewById(R.id.insert_entry_p2_s2_language_list_chips);
        chipGroupLang.removeAllViews();
        var languages = getLangKeyMap();
        this.selectedLanguages.forEach(key -> {
            var language = languages.getOrDefault(key, new ArrayList<>(languages.values()).get(languages.size() - 1));
            createChip(chipGroupLang, language, key, getString(R.string.insert_entry_p2_s1_snackbar_undo_text, language), this.selectedLanguages);
        });

        ChipGroup chipGroupTags = requireView().findViewById(R.id.insert_entry_p2_s2_tag_list_chips);
        chipGroupTags.removeAllViews();
        this.tags.forEach(tag -> createChip(chipGroupTags, tag, null, getString(R.string.insert_entry_p2_s1_snackbar_undo_text, tag), this.tags));
    }

    @Override
    public boolean isValid() {
        boolean hasError = false;

        if (selectedLanguages.size() == 0) {
            TextInputLayout spinnerLayout = requireView().findViewById(R.id.insert_entry_p2_s1_language_spinner_layout);
            spinnerLayout.setError("Es muss mindestens eine Sprache gewählt werden");
            hasError = true;
        }

        if (tags.size() < 2) {
            TextInputLayout tagLayout = requireView().findViewById(R.id.insert_entry_p2_s2_tag_input_layout);
            tagLayout.setError("Es müssen mindestens zwei Tags gewählt werden");
            hasError = true;
        }

        if (Objects.requireNonNull(insertOfferViewModel.getOffer().getValue()).getDescription().trim().length() <= 10) {
            TextInputLayout descriptionLayout = requireView().findViewById(R.id.insert_entry_p2_s3_description_layout);
            descriptionLayout.setError("Die Beschreibung muss mindestens 10 Zeichen lang sein");
            hasError = true;
        }

        if (Objects.requireNonNull(insertOfferViewModel.getOffer().getValue()).getDescription().isEmpty()) {
            TextInputLayout descriptionLayout = requireView().findViewById(R.id.insert_entry_p2_s3_description_layout);
            descriptionLayout.setError("Es muss eine Beschreibung gesetzt werden");
            hasError = true;
        }

        return !hasError;
    }
}