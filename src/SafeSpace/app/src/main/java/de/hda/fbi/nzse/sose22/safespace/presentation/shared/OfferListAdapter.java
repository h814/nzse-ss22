package de.hda.fbi.nzse.sose22.safespace.presentation.shared;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.divider.MaterialDivider;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferAcceptedGroup;

public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.ViewHolder> {

    private final List<Offer> offers;
    private Context context;

    public OfferListAdapter(List<Offer> initList) {
        this.offers = initList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_offer_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public long getItemId(int position) {
        Offer offer = offers.get(position);
        return offer.getId().getMostSignificantBits() & Long.MAX_VALUE;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        var offer = offers.get(position);

        holder.itemView.setOnClickListener(v -> {
            var arguments = new Bundle();
            arguments.putSerializable("OFFER_ID", offer.getId());
            Navigation.findNavController(holder.itemView).navigate(R.id.main_navigation_offer_detail, arguments);
        });


        holder.type.setText(offer.getType().toString());
        holder.town.setText(offer.getAddress().getTown());
        holder.desc.setText(generateAllowedGroupString(offer.getAcceptedGroup(), offer.getMaxAcceptedPersons()));

        if (offer.getImageURIs().size() > offer.getShowcaseImageIndex() && offer.getImageURIs().get(offer.getShowcaseImageIndex()) != null) {
            var showCaseURI = Uri.parse(offer.getImageURIs().get(offer.getShowcaseImageIndex()).toString());
            Glide.with(holder.itemView.getContext()).load(showCaseURI).centerCrop().into(holder.imageView);
        } else {
            //Show default image if no showcase image is possible
            Glide.with(holder.itemView.getContext()).load(R.drawable.ic_app_logo).centerCrop().into(holder.imageView);
        }

        if (position == 0) {
            //TODO if first removed divider wont disappear
            holder.divider.setVisibility(View.GONE);
        }
    }

    private String generateAllowedGroupString(Set<OfferAcceptedGroup> acceptedGroups, int numberCount) {
        StringBuilder s = new StringBuilder();

        List<String> result = new ArrayList<>();

        boolean single = false;

        if (acceptedGroups.contains(OfferAcceptedGroup.Group)) {
            result.add(context.getString(R.string.my_offers_accepted_group_group));
        }

        if (acceptedGroups.contains(OfferAcceptedGroup.Family)) {
            result.add(context.getString(R.string.my_offers_accepted_group_family));
        }

        if (acceptedGroups.contains(OfferAcceptedGroup.Single)) {
            result.add(context.getString(R.string.my_offers_accepted_group_single));
            single = true;
        }

        s.append(String.join("/", result));
        s.append(" ");

        if (result.size() != 1 || !single) {
            s.append(context.getString(R.string.my_offers_accepted_group_up_to));
            s.append(" ");
            s.append(numberCount);
            s.append(" ");
            s.append(numberCount == 1 ? context.getString(R.string.my_offers_accepted_group_person_sg) : context.getString(R.string.my_offers_accepted_group_person_pl));
        }


        return s.toString();
    }

    @Override
    public int getItemCount() {
        return this.offers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView imageView;
        final TextView type;
        final TextView town;
        final TextView desc;
        final MaterialDivider divider;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.adapter_offer_list_image);
            this.type = itemView.findViewById(R.id.adapter_offer_list_type);
            this.town = itemView.findViewById(R.id.adapter_offer_list_town);
            this.desc = itemView.findViewById(R.id.adapter_offer_list_desc);
            this.divider = itemView.findViewById(R.id.adapter_offer_list_divider);
        }
    }
}
