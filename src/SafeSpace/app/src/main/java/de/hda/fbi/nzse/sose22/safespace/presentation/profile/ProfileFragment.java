package de.hda.fbi.nzse.sose22.safespace.presentation.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

@AndroidEntryPoint
public class ProfileFragment extends Fragment {

    private UserViewModel userViewModel;

    public ProfileFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        this.userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);

        this.userViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            try {
                LinearLayout loggedInView = requireView().findViewById(R.id.profile_loged_in_container);
                LinearLayout notLoggedInView = requireView().findViewById(R.id.profile_not_loged_in_container);

                if (user != null) {
                    ((TextView) requireView().findViewById(R.id.profile_user_greeting)).setText(getString(R.string.profile_user_greeting, user.getFirstName()));
                    loggedInView.setVisibility(View.VISIBLE);
                    notLoggedInView.setVisibility(View.GONE);
                } else {
                    loggedInView.setVisibility(View.GONE);
                    notLoggedInView.setVisibility(View.VISIBLE);
                }
            } catch (Exception ignored) {
            }
        });


        requireView().findViewById(R.id.profile_button_register).setOnClickListener(l -> {
            var dir = ProfileFragmentDirections.actionMainNavigationProfileToMainNavigationRegister();
            Navigation.findNavController(view).navigate(dir);
        });

        requireView().findViewById(R.id.profile_button_login).setOnClickListener(l -> {
            var dir = ProfileFragmentDirections.actionGlobalProfileToMainNavigationLogin2();
            Navigation.findNavController(view).navigate(dir);
        });

        requireView().findViewById(R.id.profile_button_settings).setOnClickListener(l -> {
            var dir = ProfileFragmentDirections.actionMainNavigationProfileToMainNavigationSettings();
            Navigation.findNavController(view).navigate(dir);
        });

        requireView().findViewById(R.id.profile_button_my_entries).setOnClickListener(l -> {
            var dir = ProfileFragmentDirections.actionMainNavigationProfileToMainNavigationMyOffers2();
            Navigation.findNavController(view).navigate(dir);
        });

        requireView().findViewById(R.id.profile_button_logout).setOnClickListener(l -> this.userViewModel.logout());

        requireView().findViewById(R.id.profile_button_my_profile).setOnClickListener(l -> {
            var dir = ProfileFragmentDirections.actionMainNavigationProfileToMainNavigationMyProfile();
            Navigation.findNavController(requireView()).navigate(dir);
        });
    }
}