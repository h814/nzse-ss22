package de.hda.fbi.nzse.sose22.safespace.domain.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.annotation.Nonnull;

public class Address {
    private String street;
    private String number;
    private long postalCode;
    private String town;

    public Address(String street, String number, long postalCode, String town) {
        this.street = street;
        this.number = number;
        this.postalCode = postalCode;
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        return new EqualsBuilder().append(postalCode, address.postalCode).append(street, address.street).append(number, address.number).append(town, address.town).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(street).append(number).append(postalCode).append(town).toHashCode();
    }

    @Nonnull
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("street", street)
                .append("number", number)
                .append("postalCode", postalCode)
                .append("town", town)
                .toString();
    }
}
