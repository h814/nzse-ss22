package de.hda.fbi.nzse.sose22.safespace.presentation.my_offers;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.OfferRepositoryFirestore;
import de.hda.fbi.nzse.sose22.safespace.data.di.qualifieres.UserServiceFirebase;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.UserService;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.BrowseOffers;

@HiltViewModel
public class MyOffersViewModel extends ViewModel {

    private final BrowseOffers browseOffers;
    private final MutableLiveData<List<Offer>> offers;
    private final UserService userService;

    @Inject
    public MyOffersViewModel(@OfferRepositoryFirestore OfferRepository offerRepository, @UserServiceFirebase UserService userService) {
        this.browseOffers = new BrowseOffers(offerRepository);
        this.offers = new MutableLiveData<>();
        this.userService = userService;
        var user = userService.getUser();
        if (user != null) {
            this.browseOffers.getForUser(user.getUserId()).addOnSuccessListener(this.offers::postValue);
        } else this.offers.postValue(new ArrayList<>());
    }

    public LiveData<List<Offer>> getOffers() {
        return this.offers;
    }

    public void update() {
        var user = userService.getUser();
        if (user != null) {
            this.browseOffers.getForUser(user.getUserId()).addOnSuccessListener(this.offers::postValue);
        } else this.offers.postValue(new ArrayList<>());
    }
}
