package de.hda.fbi.nzse.sose22.safespace.domain.repository;

import com.google.android.gms.tasks.Task;

import java.util.Observable;

import de.hda.fbi.nzse.sose22.safespace.domain.model.User;

public abstract class UserService extends Observable {

    protected UserRepository userRepository;
    private User currentUser;

    public UserService() {
        this.currentUser = null;
        this.userRepository = null;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
        this.setChanged();
        this.notifyObservers(this.currentUser);
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public abstract Task<Boolean> login(String email, String password);

    public abstract Task<Boolean> changePassword(String password, String oldPassword);

    public abstract Task<Boolean> changeEmail(String email, String oldPassword);

    public abstract Task<Boolean> alterUser(User user);

    public abstract Task<Boolean> register(User user, String password);

    public abstract void logout();

    public final User getUser() {
        return this.currentUser;
    }

    @Override
    public void notifyObservers(Object user) {
        super.notifyObservers(user);
    }
}
