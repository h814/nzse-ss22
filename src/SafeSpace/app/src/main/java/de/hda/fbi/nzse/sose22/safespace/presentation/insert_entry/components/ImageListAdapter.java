package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import de.hda.fbi.nzse.sose22.safespace.R;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {
    private static final int DATA_TYPE = 0;
    private static final int BUTTON_TYPE = 1;
    private final List<Uri> data;
    private final int limit;
    private final ImageView deleteField;
    public ActivityResultLauncher<Intent> galleryLauncher = null;
    private ViewGroup parent;
    private boolean hasAddButton;

    public ImageListAdapter(List<Uri> dataList, int limit, ImageView deleteField) {
        this.data = dataList;
        this.hasAddButton = true;
        this.deleteField = deleteField;
        this.limit = limit;
        if (dataList.size() >= limit) {
            this.hasAddButton = false;
        }
    }

    public void setHasAddButton(boolean value) {
        if (value != this.hasAddButton) {
            this.hasAddButton = value;

            if (value) notifyItemInserted(this.data.size());
            else notifyItemRemoved(this.data.size());
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.parent = parent;
        View view;
        if (viewType == DATA_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_insert_image_list, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_insert_image_button, parent, false);
        }

        return new ViewHolder(view, (RecyclerView) parent, this.deleteField);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position >= getItemCount() - 1 && hasAddButton) {
            // Add btn
            holder.itemView.findViewById(R.id.adapter_insert_entry_image_list_button).setOnClickListener(l -> {
                if (this.galleryLauncher == null) {
                    return;
                }

                Intent startGallery = new Intent(Intent.ACTION_GET_CONTENT);
                startGallery.setType("image/*");
                startGallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                galleryLauncher.launch(startGallery);
            });
        } else {
            ImageView imageView = holder.itemView.findViewById(R.id.adapter_insert_entry_image_list_imageView);
            if (this.data.get(position) != null) {
                Glide.with(parent).load(this.data.get(position)).into(imageView);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!hasAddButton) return DATA_TYPE;

        if (position < getItemCount() - 1) {
            return DATA_TYPE;
        } else {
            return BUTTON_TYPE;
        }
    }

    public void swap(int pos, int pos2) {
        Collections.swap(this.data, pos, pos2);
        notifyItemMoved(pos, pos2);
    }

    @Override
    public int getItemCount() {
        int add = 0;
        if (hasAddButton) add = 1;

        return Math.min(this.data.size() + add, limit + add);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView, RecyclerView parent, ImageView deleteField) {
            super(itemView);
            this.recyclerView = parent;
            itemView.setOnTouchListener((v, event) -> {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                    v.startDragAndDrop(data, shadowBuilder, v, 0);
                    return true;
                } else {
                    v.performClick();
                    return false;
                }
            });

            itemView.setOnDragListener((v, event) -> {
                int action = event.getAction();
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        if (v.getId() == R.id.adapter_insert_entry_image_list_imageView_container && v.findViewById(R.id.adapter_insert_entry_image_list_button) != null)
                            return false;
                        if (deleteField != null) {
                            deleteField.setVisibility(View.VISIBLE);
                            deleteField.getBackground().setAlpha(128);
                        }
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        if (deleteField != null) {
                            deleteField.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case DragEvent.ACTION_DROP:
                        View view = (View) event.getLocalState();
                        if ((itemView.getId() == R.id.adapter_insert_entry_image_list_imageView_container && v.findViewById(R.id.adapter_insert_entry_image_list_button) != null) || view == v)
                            return false;
                        ViewGroup owner = (ViewGroup) view.getParent();

                        //same view = reorder
                        if (owner == this.recyclerView) {
                            int toPos = this.getAbsoluteAdapterPosition();
                            int fromPos = Objects.requireNonNull(this.recyclerView.findContainingViewHolder(view)).getAbsoluteAdapterPosition();
                            ((ImageListAdapter) Objects.requireNonNull(this.recyclerView.getAdapter())).swap(fromPos, toPos);
                        }

                        if (deleteField != null) {
                            deleteField.setVisibility(View.VISIBLE);
                            deleteField.getBackground().setAlpha(128);
                        }
                        break;
                }
                return true;
            });
        }
    }

}
