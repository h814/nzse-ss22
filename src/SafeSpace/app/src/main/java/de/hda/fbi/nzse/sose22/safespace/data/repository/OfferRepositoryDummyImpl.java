package de.hda.fbi.nzse.sose22.safespace.data.repository;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import de.hda.fbi.nzse.sose22.safespace.domain.model.Address;
import de.hda.fbi.nzse.sose22.safespace.domain.model.Offer;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferAcceptedGroup;
import de.hda.fbi.nzse.sose22.safespace.domain.model.OfferType;
import de.hda.fbi.nzse.sose22.safespace.domain.repository.OfferRepository;
import de.hda.fbi.nzse.sose22.safespace.domain.use_case.OfferNotFoundException;

/**
 * A dummy class for fake offer data
 */
@SuppressWarnings("FieldCanBeLocal")
// Supressed because just for testing of data will not be used in prod application
public class OfferRepositoryDummyImpl implements OfferRepository {

    private final List<Offer> offerDummies;

    // Declared outside constructor to not trahs up constructor, impl will be replaced with a real data fetching class in p4
    private final Offer offer1 = new Offer(OfferType.Flat, 3);
    private final Offer offer2 = new Offer(OfferType.House, 6);
    private final Offer offer3 = new Offer(OfferType.Room, 1);

    private final Address dummyAddress2 = new Address("teststreet", "17a", 63456, "Other test town");

    private final Address realTestAddress = new Address("Weserstraße", "4a", 63225, "Langen");

    @Inject
    public OfferRepositoryDummyImpl() {

        offer1.setId(UUID.fromString("0400d036-0f57-428e-9501-1d9c213bda0e"));
        offer2.setId(UUID.fromString("4c4a1f0f-113f-44b1-a5a9-f2d2f08ab8eb"));
        offer3.setId(UUID.fromString("6893bbfe-7ff7-4bcf-8f2a-97ca7c4e275e"));

        offer1.setAddress(realTestAddress);
        offer1.setShowFullAddress(true);
        offer2.setAddress(dummyAddress2);
        offer3.setAddress(realTestAddress);

        offer1.setInsertedByUserId(UUID.fromString("c2381275-c927-4bd4-8e52-535f820e5504"));
        offer2.setInsertedByUserId(UUID.randomUUID());
        offer3.setInsertedByUserId(UUID.fromString("c2381275-c927-4bd4-8e52-535f820e5504"));

        offer1.setRoomCount(3);
        offer1.setType(OfferType.Flat);
        offer1.setMaxAcceptedPersons(7);
        offer1.addAcceptedGroup(OfferAcceptedGroup.Family);
        offer1.addAcceptedGroup(OfferAcceptedGroup.Single);
        offer1.setHasKitchen(true);
        offer1.setHasBath(true);
        offer1.addImageURI(URI.create("https://media.istockphoto.com/photos/modern-house-with-garden-and-solar-panels-picture-id1255070620"));
        offer1.addImageURI(URI.create("https://media.istockphoto.com/photos/minimal-style-white-bathroom-3d-render-picture-id1071647270"));
        offer1.addImageURI(URI.create("https://image.shutterstock.com/z/stock-photo-kitchen-in-new-luxury-home-with-quartz-waterfall-island-hardwood-floors-dark-wood-cabinets-and-1818540647.jpg"));
        offer1.addImageURI(URI.create("https://image.shutterstock.com/z/stock-photo-bright-and-cozy-modern-bedroom-with-dressing-room-large-window-and-broad-window-sill-for-read-with-560973166.jpg"));
        offer1.addImageURI(URI.create("https://media.istockphoto.com/photos/luxurious-and-modern-living-room-3d-rendering-picture-id1213695541"));
        offer1.setDescription("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure");

        offer3.setHasKitchen(false);
        offer3.setHasBath(true);
        offer3.setType(OfferType.Room);
        offer3.addAcceptedGroup(OfferAcceptedGroup.Single);
        offer3.addAcceptedGroup(OfferAcceptedGroup.Group);
        offer3.setMaxAcceptedPersons(2);
        offer3.addImageURI(URI.create("https://media.istockphoto.com/photos/modern-villa-picture-id1148271183"));
        offer3.addImageURI(URI.create("https://media.istockphoto.com/photos/bathroom-in-new-luxury-home-picture-id1194313209"));
        offer3.addImageURI(URI.create("https://image.shutterstock.com/z/stock-photo-vase-on-metal-table-and-grey-lamp-in-spacious-bedroom-with-white-carpet-and-gallery-on-wall-above-783358234.jpg"));
        offer3.addImageURI(URI.create("https://media.istockphoto.com/photos/scandinavian-style-living-room-interior-picture-id1212170511"));


        offer1.addLanguages("Deutsch");
        offer2.addLanguages("Englisch");
        offer2.addLanguages("Englisch");
        offer3.addLanguages("Deutsch");
        offer3.addLanguages("Englisch");


        this.offerDummies = Stream.of(offer1, offer2, offer3).collect(Collectors.toList());
    }

    @Override
    public Task<List<Offer>> get() {
        return Tasks.forResult(this.offerDummies);
    }

    @Override
    public Task<Boolean> createNew(Offer o) {
        o.setId(UUID.randomUUID());
        this.offerDummies.add(o);
        return Tasks.forResult(true);
    }

    @Override
    public Task<Boolean> update(UUID offerId, Offer updatedOffer) {
        for (int i = 0; i < offerDummies.size(); ++i) {
            if (offerDummies.get(i).getId().equals(offerId)) {
                offerDummies.set(i, updatedOffer);
                return Tasks.forResult(true);
            }
        }

        return Tasks.forException(new OfferNotFoundException(offerId.toString()));
    }

    @Override
    public Task<Offer> getById(UUID id) throws OfferNotFoundException {
        return Tasks.forResult(this.offerDummies.stream().filter(o -> o.getId().equals(id)).findAny().orElseThrow(() -> new OfferNotFoundException("offer with id " + id.toString() + " not found")));
    }

    @Override
    public Task<Boolean> removeById(UUID id) throws OfferNotFoundException {
        var offer = this.offerDummies.stream().filter(o -> o.getId().equals(id)).findAny().orElseThrow(() -> new OfferNotFoundException("offer with id " + id.toString() + " not found"));
        this.offerDummies.remove(offer);
        return Tasks.forResult(true);
    }
}