package de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import dagger.hilt.android.AndroidEntryPoint;
import de.hda.fbi.nzse.sose22.safespace.R;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components.InsertEntryP1Fragment;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components.InsertEntryP2Fragment;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components.InsertEntryP3Fragment;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components.InsertOfferNavViewModel;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components.InsertOfferViewModel;
import de.hda.fbi.nzse.sose22.safespace.presentation.insert_entry.components.InsertScreen;
import de.hda.fbi.nzse.sose22.safespace.presentation.shared.UserViewModel;

@AndroidEntryPoint
public class InsertEntryFragment extends Fragment {

    public static final String EDIT_OFFER_ID_KEY = "OFFER_ID";

    private InsertOfferNavViewModel insertOfferNavViewModel;
    private InsertOfferViewModel insertOfferViewModel;

    private boolean isNew;

    private Fragment page1;
    private Fragment page2;
    private Fragment page3;

    private FragmentTransaction transaction;

    public InsertEntryFragment() {
        isNew = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insert_entry, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        new ViewModelProvider(requireActivity()).get(UserViewModel.class).getUser().observe(getViewLifecycleOwner(), user -> {
            if (user == null) {
                var callbackID = InsertEntryFragmentArgs.fromBundle(getArguments()).getCallbackID();
                var dir = InsertEntryFragmentDirections.actionMainNavigationInsertEntryToMainNavigationLogin().setCallbackID(callbackID);
                Navigation.findNavController(requireView()).navigate(dir);
            }
        });

        this.insertOfferViewModel = new ViewModelProvider(this).get(InsertOfferViewModel.class);
        this.insertOfferNavViewModel = new ViewModelProvider(this).get(InsertOfferNavViewModel.class);
        this.insertOfferNavViewModel.getPage().observe(getViewLifecycleOwner(), this::pageChanged);
        this.insertOfferNavViewModel.getActiveFragment().observe(getViewLifecycleOwner(), this::fragmentChanged);

        var args = InsertEntryFragmentArgs.fromBundle(getArguments());
        this.isNew = args.getIsNew();

        if (args.getOFFERID() != null) {
            insertOfferViewModel.setId(args.getOFFERID());
        }

        ((MaterialToolbar) requireView().findViewById(R.id.insert_entry_top_bar)).setTitle(isNew ? getString(R.string.insert_entry_title_new) : getString(R.string.insert_entry_title_edit));

        this.page1 = InsertEntryP1Fragment.newInstance();
        this.page2 = InsertEntryP2Fragment.newInstance();
        this.page3 = InsertEntryP3Fragment.newInstance();

        //new ViewModelProvider(requireActivity()).get(InsertOfferViewModel.class).getOffer().observe(getViewLifecycleOwner(), o -> ((TextView) view.findViewById(R.id.insert_Debug)).setText(o.toString()));

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                //Prevent default back press event and call close
                close();
            }
        });

        ((MaterialToolbar) requireView().findViewById(R.id.insert_entry_top_bar)).setNavigationOnClickListener(l -> this.close());

        requireView().findViewById(R.id.insert_entry_prev_btn).setOnClickListener(l -> this.previous());

        requireView().findViewById(R.id.insert_entry_next_btn).setOnClickListener(l -> this.next());


        requireView().findViewById(R.id.insert_entry_finish_btn).setOnClickListener(l -> this.save());

        this.transaction = null;

    }

    private void enableBtn(boolean enabled, Button... btns) {
        for (var btn : btns) {
            btn.setEnabled(enabled);
            btn.setClickable(enabled);
        }
    }

    private void save() {

        if (!validateBeforeNextScreen()) {
            return;
        }

        requireView().findViewById(R.id.progress_linear).setVisibility(View.VISIBLE);

        Button prevBtn = requireView().findViewById(R.id.insert_entry_prev_btn);
        Button saveBtn = requireView().findViewById(R.id.insert_entry_finish_btn);

        enableBtn(false, prevBtn, saveBtn);

        this.insertOfferViewModel.save(requireContext()).addOnCompleteListener(task -> {

            requireView().findViewById(R.id.progress_linear).setVisibility(View.GONE);

            if (task.isSuccessful()) {
                var offer = new ViewModelProvider(this).get(InsertOfferViewModel.class).getOffer().getValue();

                if (offer == null || !isNew)
                    Navigation.findNavController(requireView()).navigateUp();
                else {
                    var dir = InsertEntryFragmentDirections.actionMainNavigationInsertOfferToMainNavigationOfferDetail(offer.getId());
                    Navigation.findNavController(requireView()).navigate(dir);
                }
            } else {

                String msg = getString(R.string.unknown_error);

                if (task.getException() != null) {
                    msg = task.getException().getMessage();
                }

                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
                builder
                        .setTitle(R.string.insert_entry_failed_to_create_offer_dialog_title)
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                            dialog.dismiss();
                            enableBtn(true, prevBtn, saveBtn);
                        }).show();
            }
        });
    }

    private void pageChanged(Integer i) {
        switch (i) {
            case 0:
                requireView().findViewById(R.id.insert_entry_prev_btn).setVisibility(View.GONE);
                requireView().findViewById(R.id.insert_entry_next_btn).setVisibility(View.VISIBLE);
                requireView().findViewById(R.id.insert_entry_finish_btn).setVisibility(View.GONE);
                this.insertOfferNavViewModel.setActiveFragment(page1);
                break;
            case 1:
                requireView().findViewById(R.id.insert_entry_prev_btn).setVisibility(View.VISIBLE);
                requireView().findViewById(R.id.insert_entry_next_btn).setVisibility(View.VISIBLE);
                requireView().findViewById(R.id.insert_entry_finish_btn).setVisibility(View.GONE);
                this.insertOfferNavViewModel.setActiveFragment(page2);
                break;
            case 2:
                requireView().findViewById(R.id.insert_entry_prev_btn).setVisibility(View.VISIBLE);
                requireView().findViewById(R.id.insert_entry_next_btn).setVisibility(View.GONE);
                requireView().findViewById(R.id.insert_entry_finish_btn).setVisibility(View.VISIBLE);
                this.insertOfferNavViewModel.setActiveFragment(page3);
                break;
        }

    }

    private void fragmentChanged(Fragment f) {
        if (this.transaction == null) {
            this.transaction = getChildFragmentManager().beginTransaction();
        }

        this.transaction.addToBackStack(null);

        this.transaction.replace(R.id.insert_entry_fragment_view, f).commit();
        this.transaction = null;
    }

    /***
     * Validates if a screen is valid so it is possible to change it to the next one.
     * If an error occurs e.g a screen is not castable to the InsertScreen interface the method returns true to
     * not hold up the application flow should be enough for MVP.
     *
     * @return true if is valid else otherwise
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted") // Suppressed because inverting destroys semantic flow in our opinion
    private boolean validateBeforeNextScreen() {
        var currentFragment = this.insertOfferNavViewModel.getActiveFragment().getValue();

        if (currentFragment != null) {
            try {
                return ((InsertScreen) currentFragment).isValid();
            } catch (ClassCastException e) {
                e.printStackTrace();
                Log.e("InsertEntryFragment", "Failed to validate screen", e);
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Calls the next fragment
     */
    private void next() {

        if (!validateBeforeNextScreen()) {
            return;
        }

        this.transaction = getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        this.insertOfferNavViewModel.next();
    }

    /**
     * Calls the previous fragment
     */
    private void previous() {
        this.transaction = getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        this.insertOfferNavViewModel.previous();
    }

    /**
     * Will be called to exit (abort) the creation of a new entry.
     * Shows a dialog before going back
     * If so it will be go back to my offers page.
     */
    private void close() {

        InputMethodManager imm = (InputMethodManager) requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);

        var exitAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
        exitAlertDialogBuilder
                .setTitle(R.string.insert_entry_exit_dialog_title)
                .setMessage(R.string.insert_entry_exit_dialog_text)
                .setNegativeButton(R.string.insert_entry_exit_dialog_stay, (dialog, id) -> dialog.dismiss())
                .setPositiveButton(R.string.insert_entry_exit_dialog_leave, (dialog, id) -> Navigation.findNavController(requireView()).navigateUp());

        var exitAlertDialog = exitAlertDialogBuilder.create();
        exitAlertDialog.show();
    }

}