# Leitfaden für ein teilstandardisiertes Interview

## Hinweise

- Hinweis #1: Stellen Sie die richtigen Fragen: Stellen Sie nicht irgendwelche Fragen; die Fragen sollen Ihnen helfen, die NutzerInnen und deren spezifische Bedürfnisse, Lebensgewohnheiten, Wünsche und Nutzungskontexte besser zu verstehen. Die Antworten sind Basis für die Erarbeitung eines Anwendungsszenarios, sowie je einer funktionalen und qualitativen Anforderung (inkl. Quantifizierung) und einem Begeisterungsfaktor.
- Hinweis #2: Achten Sie auf eine “gute” Interviewvorbereitung und -durchführung: Je besser Sie das Interview planen und durchführen desto besser verwertbar sind die Resultate und Erkenntnisse. Beachten Sie deshalb die Prinzipien einer guten Interviewdurchführung (vgl. Videos und Links).
  - Stellen Sie offene Fragen, bei denen BenutzerInnen über ihre Erfahrungen berichten können
  - Vermeiden Sie Suggestivfragen – das ist vor allem wenn Sie das Interview das erste Mal durchführen sehr schwer umsetzbar; → bemühen Sie sich!
  - Fragen Sie nicht nach irgendwelchen technischen Details oder Features; diese sind für die Benutzung irrelevant.
  - Konzentrieren Sie sich auf die Ziele, den Anwendungskontext und die Lebenssituationen in denen ein Medium konsumiert wird.

### Ihre Hinweise auf Basis des durchgearbeiteten Materials

## Informationsbedarf für Persona

## Informationsbedarf für Szenarien

## Hypothesen

### vermutete mentale Modelle

## Leitfaden

Vorbemerkungen zur Durchführung

### Einstieg

### Fragen

### Abschluss
