Legen Sie hier Ihre Paper-Prototypen und ggf. Wireframes ab. Da mehrere Prototypen zu erstellen sind und mit diesen parallel zu arbeiten ist, gehen Sie bitte wie folgt vor:

option1 -- hier in einem pdf/Bild zunächst den ersten Wurf ablegen und diesen dann nach dem thinking-aloud-Test überschreiben
option1-history.pdf -- hier als PDF die Entwicklung von Option 1 darstellen (vgl. Beispiel in Moodle)
analog für option2 und option3
final -- hier den umzusetzenden finalen Entwurf abbilden
