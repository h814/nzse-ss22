# Persona
Hier dokumentieren Sie, wie Sie zu Ihren Persona gekommen sind. Die konkreten Persona hinterlegen Sie als PDF oder in einem bearbeitbaren Format (mit Bildern).

## Stakeholder
## Personenliste (Umfeld)
## Kriterien zur Unterscheidung von Personen
## Relevante Kriterien zur Klassifikation mit Begründung
## Ausprägung der relevanten Kriterien der Personen des Umfelds
## Personenliste (außerhalb)
für welche Ausprägung fehlt Ihnen noch jemand, d.h. mit welchen Eigenschaften müssen Sie noch Leute finden
## Erkenntnisse nach den Interviews
## Tabelle der Persona mit ihren relevanten Kriterien
inkl. Links zu den konkreten Persona im Repository

