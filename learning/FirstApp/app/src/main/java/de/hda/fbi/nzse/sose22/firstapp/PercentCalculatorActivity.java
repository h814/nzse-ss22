package de.hda.fbi.nzse.sose22.firstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.ParseException;

public class PercentCalculatorActivity extends AppCompatActivity {

    EditText grundwert;
    EditText prozentsatz;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.grundwert = findViewById(R.id.grundwert);
        this.prozentsatz = findViewById(R.id.prozentsatz);
        this.result = findViewById(R.id.result_text);

        findViewById(R.id.calculate_btn).setOnClickListener(v -> {
           try{
               double pw = this.calculate(Double.parseDouble(this.grundwert.getText().toString()), Double.parseDouble(this.prozentsatz.getText().toString()));
               result.setText(String.valueOf(pw));
           }catch (IllegalArgumentException e) {
               Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
           }
        });
    }

    /**
     * Calculates the percentage value from a base value and percentage rate
     * @param grundwert The base value
     * @param prozentwert The % value (starts by 0 and the max value is 100)
     * */
    private double calculate(double grundwert, double prozentwert) {

        if (prozentwert == 0) return 0;
        if (prozentwert > 100) throw new IllegalArgumentException("Max prozentwert is 100");

        return grundwert / 100 * prozentwert;
    }
}