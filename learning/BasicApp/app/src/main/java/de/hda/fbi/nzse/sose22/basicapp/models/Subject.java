package de.hda.fbi.nzse.sose22.basicapp.models;

import androidx.annotation.NonNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.time.LocalDate;

public class Subject implements Serializable {

    private String name;
    private String profName;
    private LocalDate year;

    public Subject(String name, String profName, LocalDate year) {
        this.name = name;
        this.profName = profName;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfName() {
        return profName;
    }

    public void setProfName(String profName) {
        this.profName = profName;
    }

    public LocalDate getYear() {
        return year;
    }

    public void setYear(LocalDate year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Subject)) {
            return false;
        }

        Subject user = (Subject) o;

        return new EqualsBuilder()
                .append(name, user.name)
                .append(profName, user.profName)
                .append(year, user.year)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(profName)
                .append(year.getYear())
                .toHashCode();
    }

    @NonNull
    @Override
    public String toString() {
        return new ToStringBuilder(this).append(name).append(year.getYear()).append(profName).toString();
    }

}
