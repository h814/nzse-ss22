package de.hda.fbi.nzse.sose22.basicapp.models;


import androidx.annotation.NonNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.time.LocalDate;

public class Grade implements Serializable {

    public static final LocalDate EMPTY_DATE = LocalDate.of(1800, 1, 1);

    private int value;
    private final Subject subject;
    private LocalDate date;

    public Grade(int value, Subject subject, LocalDate date) {
        this.subject = subject;
        this.value = value;
        this.date = date;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Subject getSubject() {
        return subject;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Grade)) {
            return false;
        }

        Grade user = (Grade) o;

        return new EqualsBuilder()
                .append(value, user.value)
                .append(date, user.date)
                .append(subject, user.subject)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(subject)
                .append(value)
                .append(date)
                .toHashCode();
    }

    @NonNull
    @Override
    public String toString() {
        return new ToStringBuilder(this).append(value).append(date).append(subject).toString();
    }
}
