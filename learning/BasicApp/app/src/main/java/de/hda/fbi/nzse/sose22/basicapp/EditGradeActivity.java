package de.hda.fbi.nzse.sose22.basicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import de.hda.fbi.nzse.sose22.basicapp.models.Grade;
import de.hda.fbi.nzse.sose22.basicapp.models.Subject;

public class EditGradeActivity extends AppCompatActivity {

    private final Calendar myCalendar= Calendar.getInstance();
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private Grade toEditGrade;
    private int gradeIndex;
    private boolean isNew = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_grade);

        setSupportActionBar(findViewById(R.id.edit_grade_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Edit grade");

        this.gradeIndex = getIntent().getExtras().getInt("index");
        this.toEditGrade = (Grade) getIntent().getExtras().get(MyGradesActivity.TO_EDIT_INTENT);

        if(this.toEditGrade.getDate().equals(Grade.EMPTY_DATE)) this.isNew = true;

        findViewById(R.id.edit_grade_save_btn).setOnClickListener(l -> {
            var grade = this.createNewGradeOfInput();
            Intent intent = new Intent();
            intent.putExtra(MyGradesActivity.NEW_GRADE_INTENT, grade);
            intent.putExtra("index", this.gradeIndex);
            setResult(RESULT_OK, intent);
            finish();
        });

        findViewById(R.id.edit_grade_abort_btn).setOnClickListener(l -> {
            this.onAbort(isNew);
        });

        var dateInput = (EditText) findViewById(R.id.edit_grade_date_input);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                dateInput.setText(myCalendar.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(dateFormat));
            }
        };

        dateInput.setOnClickListener(l -> {
            new DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        insertDataToForm();
    }

    private void insertDataToForm() {

        var gradeInput = (EditText) findViewById(R.id.edit_grade_grade_input);
        var dateInput = (EditText) findViewById(R.id.edit_grade_date_input);
        var teacherInput = (EditText) findViewById(R.id.edit_grade_teacher_input);
        var subjectInput = (EditText) findViewById(R.id.edit_grade_subject_name_input);
        var yearInput = (EditText) findViewById(R.id.edit_grade_year_input);

        gradeInput.setText(this.toEditGrade.getValue() == 0 ? "" : String.valueOf(this.toEditGrade.getValue()));

        if(!this.toEditGrade.getDate().equals(Grade.EMPTY_DATE)) {
            dateInput.setText(this.toEditGrade.getDate().format(dateFormat));
            myCalendar.set(Calendar.YEAR, this.toEditGrade.getDate().getYear());
            myCalendar.set(Calendar.MONTH, this.toEditGrade.getDate().getMonthValue());
            myCalendar.set(Calendar.DAY_OF_MONTH, this.toEditGrade.getDate().getDayOfMonth());
        }else {
            dateInput.setText("");
        }

        teacherInput.setText(this.toEditGrade.getSubject().getProfName().isEmpty() ? "" : this.toEditGrade.getSubject().getProfName());
        subjectInput.setText(this.toEditGrade.getSubject().getName().isEmpty() ? "" : this.toEditGrade.getSubject().getName());
        yearInput.setText(this.toEditGrade.getSubject().getYear().equals(Grade.EMPTY_DATE) ? "" : String.valueOf(this.toEditGrade.getSubject().getYear().getYear()));
    }

    private Grade createNewGradeOfInput() {
        var gradeInput = (EditText) findViewById(R.id.edit_grade_grade_input);
        var dateInput = (EditText) findViewById(R.id.edit_grade_date_input);
        var teacherInput = (EditText) findViewById(R.id.edit_grade_teacher_input);
        var subjectInput = (EditText) findViewById(R.id.edit_grade_subject_name_input);
        var yearInput = (EditText) findViewById(R.id.edit_grade_year_input);


        Subject s = new Subject(subjectInput.getText().toString(), teacherInput.getText().toString(), LocalDate.of(Integer.parseInt(yearInput.getText().toString()), 1, 1));
        return new Grade(Integer.parseInt(gradeInput.getText().toString()), s, LocalDate.of(myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onAbort(isNew);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onAbort(boolean isNew) {
        Intent intent = new Intent();
        intent.putExtra("index", this.gradeIndex);

        if(isNew) intent.putExtra("isNew", true);

        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
