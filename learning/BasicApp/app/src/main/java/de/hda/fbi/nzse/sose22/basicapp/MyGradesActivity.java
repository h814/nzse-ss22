package de.hda.fbi.nzse.sose22.basicapp;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import de.hda.fbi.nzse.sose22.basicapp.models.Grade;
import de.hda.fbi.nzse.sose22.basicapp.models.Subject;

public class MyGradesActivity extends AppCompatActivity {

    public static final String TO_EDIT_INTENT = "toEditGrade";
    public static final String NEW_GRADE_INTENT = "newGrade";

    private static boolean isListInit = false;
    private static List<Grade> myGrades;

    private static final Subject testSubject = new Subject("TeWi", "Kuntz", LocalDate.of(2022, 1, 1));
    private static final Grade testGrade1 = new Grade(2, testSubject, LocalDate.of(2022, 3, 2));
    private static final Grade testGrade2 = new Grade(1, testSubject, LocalDate.of(2022, 5, 12));

    ActivityResultLauncher<Intent> editGradeCallback = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        if (result.getResultCode() == RESULT_FIRST_USER || result.getData() == null || result.getData().getExtras() == null) {
            Toast.makeText(this, R.string.my_grades_abort_toast, Toast.LENGTH_SHORT).show();
            return;
        }

        var index = result.getData().getExtras().getInt("index");

        if(result.getResultCode() == RESULT_CANCELED) {
            assert result.getData() != null;

            var isNew = result.getData().getExtras().getBoolean("isNew");

            if(isNew) {
                MyGradesActivity.myGrades.remove(index);
                this.setupGradeList();
            }

            Toast.makeText(this, R.string.my_grades_abort_toast, Toast.LENGTH_SHORT).show();
            return;
        }

        var newGrade = (Grade) result.getData().getExtras().get(NEW_GRADE_INTENT);

        if(newGrade == null || newGrade.getDate().equals(Grade.EMPTY_DATE)) {
            Toast.makeText(this, R.string.my_grades_failed_toast, Toast.LENGTH_SHORT).show();
            return;
        };

        MyGradesActivity.myGrades.set(index, newGrade);
        this.setupGradeList();
    });

    public static void initList() {
        if(!MyGradesActivity.isListInit){
            MyGradesActivity.isListInit = true;
            MyGradesActivity.myGrades = Stream.of(testGrade1, testGrade2).collect(Collectors.toList());
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_grades);

        setSupportActionBar(findViewById(R.id.my_grades_toolbar));
        getSupportActionBar().setTitle(R.string.nav_main_mygrades_string);

        MyGradesActivity.initList();

        setupGradeList();

        BottomNavigationView bottomNavigation = findViewById(R.id.my_grades_main_nav);
        bottomNavigation.setOnItemSelectedListener(this::onBottomNavSelect);
        bottomNavigation.setSelectedItemId(R.id.nav_main_mygrades);

        findViewById(R.id.my_grades_add_btn).setOnClickListener(l -> {
            var g = new Grade(0, new Subject("", "", Grade.EMPTY_DATE), Grade.EMPTY_DATE);
            MyGradesActivity.myGrades.add(g);
            this.setupGradeList();
            Intent intent = new Intent(this, EditGradeActivity.class);
            intent.putExtra(MyGradesActivity.TO_EDIT_INTENT, g);
            intent.putExtra("index", MyGradesActivity.myGrades.size() - 1);
            editGradeCallback.launch(intent);
        });
    }

    private void setupGradeList() {
        LinearLayout itemListLayout = findViewById(R.id.my_grades_item_list);
        itemListLayout.removeAllViewsInLayout();

        IntStream.range(0, MyGradesActivity.myGrades.size()).forEach(i -> {
            var g = MyGradesActivity.myGrades.get(i);
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);

            TextView t = new TextView(this);
            t.setText(g.toString());
            layout.addView(t);

            LinearLayout btnGroup = new LinearLayout(this);
            btnGroup.setOrientation(LinearLayout.HORIZONTAL);
            Button editButton = new Button(this);
            editButton.setText(R.string.my_grades_edit_btn);
            editButton.setOnClickListener(bl -> {
                Intent intent = new Intent(this, EditGradeActivity.class);
                intent.putExtra(MyGradesActivity.TO_EDIT_INTENT, g);
                intent.putExtra("index", i);
                editGradeCallback.launch(intent);
            });


            Button deleteButton = new Button(this);
            deleteButton.setText(R.string.my_grades_delete_btn);
            deleteButton.setOnClickListener(bl -> {
                AlertDialog.Builder confirmDeleteDialogueBuilder = new AlertDialog.Builder(this);
                confirmDeleteDialogueBuilder
                        .setMessage(getString(R.string.my_grades_delete_dialogue, g.getValue(), g.getSubject().getName(), g.getDate().toString()))
                        .setTitle(R.string.my_grades_delete_dialogue_title);

                confirmDeleteDialogueBuilder.setPositiveButton(R.string.delete, (dialog, which) -> {
                    MyGradesActivity.myGrades.remove(g);
                    this.setupGradeList();
                });

                confirmDeleteDialogueBuilder.setNegativeButton(R.string.cancel, (dialog, which) -> {
                    dialog.cancel();
                });

                AlertDialog dialog = confirmDeleteDialogueBuilder.create();
                dialog.show();
            });

            btnGroup.addView(editButton);
            btnGroup.addView(deleteButton);

            layout.addView(btnGroup);
            itemListLayout.addView(layout);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private boolean onBottomNavSelect(MenuItem item) {
        var id = item.getItemId();

        Intent intent;
        if(id == R.id.nav_main_home) {
            finish();
            return true;
        } else if(id == R.id.nav_main_mygrades) {
            return true;
        }

        return false;
    }
}