package de.hda.fbi.nzse.sose22.basicapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_main_home_string);

        this.bottomNavigation = findViewById(R.id.nav_main_main_activity);
        this.bottomNavigation.setOnItemSelectedListener(this::onBottoNavSelect);

        setupHowToUseList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.bottomNavigation.setSelectedItemId(R.id.nav_main_home);
    }

    private boolean onBottoNavSelect(MenuItem item) {
        var id = item.getItemId();

        Intent intent;
        if(id == R.id.nav_main_home) {
            return true;
        } else if(id == R.id.nav_main_mygrades) {
            intent = new Intent(this, MyGradesActivity.class);
            startActivity(intent);
            return true;
        }

        return false;
    }

    private void setupHowToUseList() {
        String[] points = getResources().getStringArray(R.array.main_steps_to_use_list);

        LinearLayout layoutContainer = findViewById(R.id.main_how_to_use_list_container);

        for(var point : points) {
            SpannableString s = new SpannableString(point);
            s.setSpan(new BulletSpan(20, Color.BLACK, 10), 0, s.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            TextView tv = new TextView(this);
            tv.setText(s);
            tv.setPadding(30, 0, 10, 30);
            layoutContainer.addView(tv);
        }
    }

}