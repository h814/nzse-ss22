package de.hda.fbi.nzse.sose22.upanddown;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class UpAndDownActivity extends AppCompatActivity {

    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.counter = 0;
        setContentView(R.layout.activity_upanddown);

        findViewById(R.id.button_minus_5).setOnClickListener(l -> {
            this.onClickDecrementCounterByFive();
        });

        findViewById(R.id.button_plus_5).setOnClickListener(l -> {
            this.onClickIncrementCounterByFive();
        });
    }

    /**
     * Increments the counter state and calls the setter for the counter textview
     * */
    void onClickIncrementCounterByFive() {
        this.counter += 5;
        this.setCounterValueText();
    }


    /**
     * Decrements the counter state and calls the setter for the counter textview
     * */
    void onClickDecrementCounterByFive() {
        this.counter -= 5;
        this.setCounterValueText();
    }

    /**
    * Set the value of the counter text field to the current counter value
    * */
    void setCounterValueText() {
        final TextView counterTextView = (TextView)findViewById(R.id.counter_value);
        counterTextView.setText(String.format("%d", this.counter));
    }
}