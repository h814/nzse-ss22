# Wohnungsangebote für Flüchtlinge: SafeSpace

## Problem
Es gibt viele Gründe in humanitären Notsituationen sein Land zu verlassen und für sich und seine Familie einen sicheren Ort zum Verweilen zu suchen. Viele tausende Menschen flüchten aus ihrem Land und suchen nach Unterschlupf. Zwar gibt es viele öffentliche Auffanglager, doch diese sind häufig überlastet und zusätzlich gibt es viele private Bürger, die die Möglichkeiten hätten, Flüchtlinge bei sich aufzunehmen. 

## Lösung und Vision
Unsere App bildet die Verbindung zwischen diesen beiden Welten. Flüchtlinge sehen in der App auf einen Blick, welche Bürger in der gewünschten Stadt bereit wären, Geflüchtete aufzunehmen. Bürger können ihren freien Platzbedarf angeben und so unbenutzten Wohnraum sozial und effizient nutzen.  

## Zielgruppe
- Flüchtlinge
- Bürger, die unbenutzten Wohnraum zur Verfügung haben
